%%%%%%%%%% Start macros
\newcommand{\tmem}[1]{{\em #1\/}}
\newcommand{\tmop}[1]{\ensuremath{\operatorname{#1}}}
\newcommand{\tmstrong}[1]{\textbf{#1}}
\newcommand{\tmverbatim}[1]{{\ttfamily{#1}}}
\newenvironment{quoteenv}{\begin{quote} }{\end{quote}}
%%%%%%%%%% End macros

\makecapitolo
{Andrea Centomo}
{Origami Box Problem}
{Il problema di ottenere la scatola a forma di parallelepipedo di volume
massimo ritagliando dagli angoli di un rettangolo quattro quadrati 
congruenti (vedi Figura \ref{cento:bp}) \ è un problema di ottimizzazione ben 
noto ({\tmem{box problem}}). 
Con lo sviluppo di software che integrano grafica e
manipolazione algebrica è possibile trattare il problema, almeno in 
forma approssimata, ben prima che gli studenti possiedano il concetto di 
derivata \cite{dodge}. Nell'Analisi Non Standard (NSA) il problema si 
risolve in modo immediato e rigoroso ricorrendo al metodo di Fermat per 
funzioni polinomiali di terzo grado.

In un articolo relativamente recente \cite{wares} l'autore suggerisce di 
arricchire il {\tmem{box problem}} accompagnando lo studio matematico alla 
costruzione
concreta della scatola di volume massimo con tecniche di {\tmem{paper
folding}}. 
Nella breve nota che segue mi ripropongo di esplorare un percorso,
in qualche modo, inverso rispetto ai precedenti. A partire da un semplice
modello di scatola origami, di cui viene assegnato il {\tmem{crease 
pattern}},
si risolve con il metodo di Fermat un problema di minimo per una funzione
razionale che serve a ridurre il consumo di carta da utilizzare per 
realizzare il modello. 
L'approccio seguito si inquadra idealmente nel novero dei problemi
di ottimo relativi al {\tmem{paper folding}} alcuni dei quali, per quanto
sappiamo, sono ancora
aperti \footnote{\tmverbatim{\href{https://erikdemaine.org/wrapping/}{
https://erikdemaine.org/wrapping/}}}.

}

\section{Box problem classico}

\ Nella sua forma più semplice il {\tmem{box problem}} è il seguente:

\begin{quoteenv}
  {\tmstrong{Box problem.}} Da un quadrato di carta di lato $L$ (vedi Figura
  \ref{cento:bp}), ritagliando quattro quadrati congruenti agli angoli, ottenere 
la
  scatola a forma di parallelepipedo di volume massimo.
\end{quoteenv}

\begin{figure}[h]
	\centering
  \includegraphics[width=7.5cm]{box_base.eps}
  \caption{{\tmem{Box problem}}\label{cento:bp}}
\end{figure}

Se supponiamo, ad esempio, che sia $L = 3$ dm, il problema si risolve
determinando il punto di massimo assoluto della funzione polinomiale di 
terzo
grado
\[ f (x) = x (3 - 2 x) (3 - 2 x) = 4 x^3 - 12 x^2 + 9 x \]
definita nell'intervallo chiuso e limitato $[0, 3 / 2]$. Dal momento che il
grafico di $f$ è quello di Figura \ref{cento:gbp}, è facile ricavare ``a
vista'' il valore del punto di massimo $x_m = 1 / 2$.

\begin{figure}[h]
\centering 
 	\includegraphics[width=7.5cm]{geogebra.eps}
  \caption{Grafico di $f$\label{cento:gbp}}
\end{figure}

Ricorrendo al metodo di Fermat nella sua formulazione originale è
piuttosto semplice verificare in NSA che la soluzione ``a vista'' è la
soluzione esatta del problema. Preso un numero iperreale infinitesimo
$\epsilon \neq 0$ si ha che
\[ f (x + \epsilon) = 4 (x + \epsilon)^3 - 12 (x + \epsilon)^2 + 9 (x +
   \epsilon) \]
e, con qualche calcolo che volendo si può eseguire con un qualsiasi
CAS\footnote{Acronimo di Computer Algebra System.}, si ha
\[ d (x) = f (x + \epsilon) - f (x) = (12 x^2 - 24 x + 9) \epsilon + (12 x -
   12) \epsilon^2 + 4 \epsilon^3 . \]
Secondo Fermat il massimo si ha in corrispondenza dell'unico valore di $x 
\in
(0, 3 / 2)$ tale che la differenza $d$ sia minima (punto stazionario). 
Ciò
accade quando
\[ 12 x^2 - 24 x + 9 = 0 \]
da cui, risolvendo l'equazione di secondo grado, si ottiene
\[ x_m = \frac{1}{2} . \]
Il fatto che $x_m$ sia effettivamente di massimo si può giustificare
rigorosamente in NSA come segue: se $x = x_m$ la differenza infinitesima
assume il valore
\begin{equation}
  d (x_m) = - 6 \epsilon^2 + 4 \epsilon^3 = (4 \epsilon - 6) \epsilon^2 < 0
\end{equation}
e ciò per ogni infinitesimo non nullo $\epsilon$. Se consideriamo la
monade
\[ \tmop{monad} \left( \frac{1}{2} \right) = 
\left\{ x \in \IR: x \approx \frac{1}{2} \right\} \]
dove $\IR$ rappresenta l'insieme dei numeri iperreali e il
simbolo $\approx$ la relazione di infinita vicinanza, è chiaro che da (1)
possiamo concludere che $x = 1 / 2$ è il punto di massimo assoluto di $f$
ristretta a monad$\left( \frac{1}{2} \right)$. Per il {\tmstrong{principio 
di
trasferimento}} ciò implica che $x = 1 / 2$ sia un punto di massimo 
locale
(in senso standard) di $f$.

\section{Scatola origami}

In un articolo sul {\tmem{box problem}} \cite{wares} l'autore accompagnava 
la trattazione teorica con la proposta esplicita di un metodo origami per la
costruzione della scatola a forma di parallelepipedo di volume massimo. 
Senza sapere di questo articolo avevo già proposto un paio di anni fa 
un lavoro analogo ad un gruppo di studentesse di terza liceo 
linguistico\footnote{Classe 3AL dell'anno scolastico 2017/2018.} in una 
forma molto semplificata. 
Dopo aver disegnato su un foglio di carta la Figura \ref{cento:bp} nel caso in 
cui si ottiene la scatola di volume massimo, invece di ritagliare gli angoli
quadrati, avevo chiesto di trovare un incastro origami che permettesse alla
scatola di carta di mantenere la sua forma di parallelepipedo. I risultati
ottenuti potevano essere accettabili per scopi didattici, ma se riguardati
nell'orizzonte delle enormi possibilità che l'origami offre per la
costruzione di scatole, apparivano piuttosto modesti. Per questo motivo ho
pensato di rivisitare l'intera questione e di partire invece che dalla
matematica dall'origami.

\subsection{Twist box}

L'ingegnosa scatola a base quadrata di Figura \ref{cento:tfb} richiede minime
abilità origamistiche per essere realizzata.

\begin{figure}[h]
	\centering 
  	\includegraphics[width=7.5cm]{NSA_origami_centomo-1.png}
  \caption{Twist box \label{cento:tfb}}
\end{figure}

Il solo riferimento al {\tmem{crease pattern}}, rappresentato in Figura
\ref{cento:tfcp}, è sostanzialmente sufficiente per piegare il modello.

\begin{figure}[h]
	\centering 
 	\includegraphics[width=7.5cm]{crease_tomoko.eps}
  \caption{Crease pattern\label{cento:tfcp}}
\end{figure}

Se il foglio rettangolare di partenza ha lunghezza $a$ e larghezza $b$ è
facile vedere che la base quadrata della scatola ha area $a^2 / 25$ e che
l'altezza della scatola è $b - 2 a / 5$. \\
Il volume $V$ della scatola è
allora
\[ V = \frac{a^2}{25} \cdot \left( b - \frac{2}{5} a \right) . \]
La superficie totale $S$ di un parallelepipedo a base quadrata di lato 
$a / 5$
e altezza $b - 2 a / 5$ vale
\[ S = \frac{2}{25} a^2 + \frac{4}{5} \left( a b - \frac{2}{5} a^2 \right) =
   \frac{4}{5} a b - \frac{6}{25} a^2 \]
e
\[ E = a b - S = \frac{1}{5} a b + \frac{6}{25} a^2 \]
rappresenta l'eccesso $E$ di carta richiesta per poter costruire la scatola
con il processo di piegatura prescelto. Se per realizzare il modello usiamo 
un
foglio A4 $(29.7 \tmop{cm} \times 21 \tmop{cm})$, senza possibilità di
ritagliarlo, avremo due possibilità

\begin{table}[h]
	\centering 
  \begin{tabular}{|l|l|l|l|}
    \hline
    & $a$ $[\tmop{cm}]$ & $b$ $[\tmop{cm}]$ & $V$ $[\tmop{cm}^3]$\\
    \hline
    Soluzione 1 & 29.7 & $21$.0 & 321.79\\
    \hline
    Soluzione 2 & 21.0 & 29.7 & 375.73\\
    \hline
  \end{tabular}
  \caption{Volumi di scatole ottenute da un A4}
\end{table}

Per poter ridurre al massimo il consumo di carta viene spontaneo porre e
risolvere il seguente:

\begin{quoteenv}
  {\tmstrong{Problema di minimo.}} Fissato il valore $V$ del volume della
  scatola, determinare le dimensioni del foglio di carta che permettono di
  costruire la scatola origami minimizzando il consumo di carta.
\end{quoteenv}

In questo caso la funzione area da minimizzare è
\[ S (x) = 10 x^2 + \frac{5 V}{x^{}} \]
con dove $x > 0$ è una lunghezza pari a un quinto della larghezza del
foglio di carta. Osservato che
\[ S (x + \epsilon) = 10 (x + \epsilon)^2 + \frac{5 V}{x + \epsilon} = S 
(x) + 20 x \epsilon + 10 \epsilon^2 - \frac{5 V \epsilon}{x (x + \epsilon)} 
\]
e che
\[ \frac{1}{x (x - \epsilon)} = \frac{1}{x^2} + \frac{\epsilon}{x^2 (x -
   \epsilon)} \]
si ottiene per la funzione differenza la forma
\[ S (x + \epsilon) - S (x) = \left( 20 x - \frac{5 V}{x^2} \right) 
\epsilon +
\left( 10 - \frac{5 V}{x^2 (x - \epsilon)} \right) \epsilon^2 . \]
Secondo Fermat il minimo si ha quando
\[ 20 x - \frac{5 V}{x^2} = 0 \qquad x = \left( \frac{V}{4}
   \right)^{\frac{1}{3}} \]
a cui corrisponde un foglio di dimensione
\[ 5 \left( \frac{V}{4} \right)^{\frac{1}{3}} \times 3 (2 V)^{\frac{1}{3}} .
\]
Forti di questo risultato possiamo fare alcune osservazioni. Nel caso della
scatola ottenuta da un foglio A4 con la Soluzione 1 in cui
\[ V_1 \approx 321.79 \tmop{cm}^3 \]
il foglio che riduce al massimo lo spreco di carta per realizzare la scatola
dovrebbe avere dimensioni
\[ 21.6 \tmop{cm} \times 25.9 \tmop{cm} . \]
Lo spreco di carta $\delta_1$ è allora pari a
\[ \delta_1 = 29.7\tmop{cm} \cdot 21\tmop{cm} -
21.6\tmop{cm} \cdot 25.9\tmop{cm} \approx 64 \tmop{cm}^2 . \]
Nel caso della Soluzione 2 avremo
\[ V_2 = 
\tonda{4.2\tmop{cm}}^2 \cdot 21.3\tmop{cm} \approx 375.73 \tmop{cm}^3. \]
mentre il foglio ottimale per realizzare la scatola dovrebbe avere 
dimensione
\[ 22.73\tmop{cm} \times 27.27 \tmop{cm} \]
con uno spreco $\delta_2$ pari a
\[ \delta_2 = 29.7\tmop{cm} \cdot 21\tmop{cm} - 
22.73\tmop{cm} \cdot 27.27\tmop{cm} \approx 3.8 \tmop{cm}^2 . \]
Da un punto di vista del risparmio della carta la soluzione 2 è migliore
della 1.

\section*{Conclusioni}

L'integrazione tra NSA e {\tmem{paper folding}} appare molto efficace per
trattare in modo concreto alcuni problemi elementari di ottimizzazione che
intervengono in modo piuttosto naturale nella costruzione di una scatola
origami. Questo genere di problemi si possono introdurre molto presto nel
curriculum, tipicamente al terzo anno del liceo scientifico. Anche se la
soluzione approssimata di questi problemi si potrebbe condurre sfruttando
esclusivamente software per la grafica, l'uso del metodo di Fermat nella sua
versione NSA permette di rigorizzare le considerazioni di carattere 
intuitivo
giungendo, senza eccessive difficoltà, a soluzioni esatte.

\begin{thebibliography}{10}
\phantomsection
\addcontentsline{toc}{section}{Bibliografia}

\bibitem[1]{dodge} 
\textsc{W. Dodge}, 
Thinking out of the box... Problem\\
in \emph{Mathematics Teacher, Vol. 95, No. 8, pages 568-574},
2002

\bibitem[2]{wares} 
\textsc{A. Wares}, 
Geometry between the Folds\\
in \emph{Ohio Journal of School Mathematics, Fall 2014, No. 70}, 
2014

\bibitem[3]{cut} 
\url{www.cut-the-knot.org/Curriculum/Calculus/BoxVolume.shtml}

\end{thebibliography}

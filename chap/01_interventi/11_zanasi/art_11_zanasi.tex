% \documentclass[a4,12pt]{article}
% \usepackage{hyperref}
% \usepackage{mathtools}
% \usepackage[utf8]{inputenc}
% \usepackage[italian]{babel}
% \usepackage{lmodern}
% \usepackage[T1]{fontenc}
% \usepackage{pgf,tikz}
% \usepackage{graphicx}
% \usepackage{fixcmex} %serve per i simboli matematici che altrimenti non 
% vengono ingranditi
% \usepackage{pgfplots}
% 
% 
% \title{Il principio di trasferimento al contrario}
% 
% \newcommand\R{\mathbf{R}}
% \newcommand\st[1]{\mathrm{st}\left( #1 \right)}
% \DeclareMathOperator{\st}{st}

\makecapitolo
{Roberto Zanasi - ITIS Enrico Fermi, Modena}
{Il principio di trasferimento al contrario}
{Considerazioni sul funzionamento del principio del trasferimento al contrario, 
cioè come si può passare da proprietà valide per indici infiniti a proprietà 
valide per indici finiti.
}
%pdflatex\begin{document}
\section{Introduzione}
Spesso, quando presentiamo agli studenti le basi dell'analisi non standard, ci 
capita di affermare che una delle particolarità che possiede l'insieme dei 
numeri iperreali è che in esso si può fare tutto quello che si fa con i numeri 
reali. Questo è un modo non rigoroso di presentare il principio di trasferimento 
che, se enunciato rigorosamente, afferma che 
se $P(a_1,\dots,a_n)$ è una proprietà degli oggetti standard $a_1,\dots,a_n$ 
espressa in \textbf{forma elementare}, allora $P(a_1,\dots,a_n)$ vale se e solo 
se la stessa proprietà vale per le corrispondenti estensioni nonstandard, cioè: 
$P(a_1,\dots,a_n)\iff P(a^\star_1,\dots,a^\star_n)$. 

Mi pare che questo enunciato, così espresso, sia molto ``delicato'', perché un 
uso acritico della coimplicazione potrebbe portare a conclusioni sbagliate, dato 
che certe proprietà dei numeri iperreali non sono estensioni di analoghe 
proprietà di numeri standard, e il passaggio da $\R^\star$ a $\R$ potrebbe non 
essere realizzabile. Vorrei quindi analizzare le dimostrazioni di quei teoremi 
che hanno, come ipotesi, proprietà tipiche di $\R^\star$ che non sono estensioni 
di proprietà di $\R$, e vedere come sia possibile ottenere da esse proprietà 
valide in $\R$. 

Credo che sia utile, per iniziare, riassumere la definizione assiomatica di 
$\R^\star$.

\section{Definizione assiomatica di $\R^\star$}

\textbf{Assioma 1}.
$\R$ è un campo ordinato completo

Prima di definire l'insieme dei numeri iperreali, ricordiamo come si 
caratterizza l'insieme dei numeri reali. La completezza di $\R$ può essere 
definita in due modi diversi: se per \emph{completo} si intende che ogni 
sottoinsieme di $\R$ superiormente limitato ammette estremo superiore, allora 
non è necessario aggiungere il postulato di Eudosso-Archimede, che diventa un 
teorema dimostrabile. Se invece si  usa la completezza secondo Cauchy (la 
proprietà che afferma che ogni successione di Cauchy è convergente), allora 
l'assioma di Eudosso-Archimede deve essere aggiunto; direi che, in ogni caso, 
dal punto di vista didattico sia utile enunciarlo. È bene anche sottolineare 
che 
si dimostra che, a meno di isomorfismi, $\R$ è l'\emph{unico} campo ordinato 
completo.

\bigskip\noindent
\textbf{Assioma 2}. $\R^\star$ è un campo, estensione ordinata di $\R$.

L'insieme dei numeri iperreali estende l'insieme dei numeri reali, mantenendone 
le proprietà: non tutte, però, dato che $\R$ è l'unico a possederle tutte. 
L'estensione di cui si parla riguarda l'ordinamento e le proprietà di campo, 
cioè le proprietà delle operazioni; perdiamo invece la proprietà archimedea. 
Questo a causa del terzo assioma.

\bigskip\noindent
\textbf{Assioma 3}.
$\R^\star$ contiene un infinitesimo positivo.

Ai fini didattici è bene enunciare anche la versione gemella di questo 
postulato, che afferma che $\R^\star$ contiene un infinito positivo, anche se 
essa non è strettamente necessaria: il fatto che le proprietà di $\R$ siano 
valide ci permette di dimostrare che il reciproco di un infinitesimo è un 
infinito. Quello che succede in realtà, introducendo un solo elemento 
infinitesimo, è che $\R^\star$ si popola di un'infinità di infinitesimi, sia 
positivi che negativi, e di un'infinità di infiniti, anch'essi sia positivi che 
negativi.

\bigskip\noindent
\textbf{Assioma 4}.
Per ogni funzione reale $f$ in $n$ variabili esiste una funzione $f^\star$, in 
$n$ variabili, detta \emph{estensione naturale}. 

Non si estendono soltanto le operazioni di $\R$, quindi, ma tutte le funzioni. 
Per \emph{estensione} si intende la seguente proprietà: se $r$ è un numero 
reale, $f^\star(r)=f(r)$.
Ma come si comporta $f$ sui nuovi numeri? Ce lo spiega il prossimo assioma.

\bigskip\noindent
\textbf{Assioma 5 (assioma di trasferimento)}.
Se $P(a_1,\dots,a_n)$ è una proprietà degli oggetti standard $a_1,\dots,a_n$ 
espressa in \textbf{forma elementare}, allora $P(a_1,\dots,a_n)$ vale se e solo 
se la stessa proprietà vale per le corrispondenti estensioni nonstandard, cioè: 
$P(a_1,\dots,a_n)\iff P(a^\star_1,\dots,a^\star_n)$.

È il postulato più delicato, perché è valido solo per proprietà espresse in 
forma elementare. Ciò significa che ogni quantificatore è ristretto a un 
insieme: 
posso dire ``per qualunque $x$ appartenente all'insieme $A$'', ma non posso dire 
``per qualunque $X$ sottoinsieme dell'insieme $A$''. Una definizione di questo 
postulato in forma più rigorosa dovrebbe fare riferimento alla logica del primo 
ordine, ma non credo che sia il caso di affrontare questo argomento alla scuola 
superiore.

Ecco un esempio di applicazione corretta del principio di trasferimento: 
$x\in\R$ è un numero naturale se e solo se $x\ge0$ e $\lfloor x\rfloor=x$. 
Questa affermazione si estende in questo modo:
$x\in\R^\star$ è un numero ipernaturale se e solo se $x\ge0$ e $\lfloor 
x\rfloor^\star=x$. Questa può essere l'occasione per sottolineare il fatto 
che esistono sottoinsiemi dei numeri iperreali, come gli ipernaturali, che 
sono estensioni di sottoinsiemi di numeri reali.

Ed ecco un esempio di applicazione errata del principio di trasferimento: se si 
estende la proprietà che afferma che ogni sottoinsieme $A$ di $\R$ superiormente 
limitato ammette estremo superiore, si ottiene la proprietà che afferma, 
falsamente, che ogni sottoinsieme $A$ di $\R^\star$ superiormente limitato 
ammette estremo superiore. Il quantificatore opera --- erroneamente --- su un 
insieme, e un controesempio a questa affermazione è dato dall'insieme di tutti 
gli infinitesimi: esso è superiormente limitato, ma non ammette un estremo 
superiore. Vediamo perché.

Dato che per ogni $x>0$ vale la disuguaglianza $x<2x$, per il principio di 
trasferimento si può dire che per ogni infinitesimo $\delta>0$ vale la 
disuguaglianza $\delta<2\delta$, quindi non esiste un infinitesimo maggiore di 
tutti gli altri, dato che possiamo trovarne uno sempre più grande. E non è 
nemmeno possibile che l'estremo superiore sia un numero reale positivo, perché 
non esiste un numero reale positivo minore di tutti gli altri. Quindi l'insieme 
di tutti gli infinitesimi non possiede estremo superiore.


\section{Da proprietà valide per numeri infiniti a quelle per numeri finiti}

Per evitare di appesantire gli enunciati, da ora in poi indicheremo con $n$ un 
numero naturale, $N$ un numero ipernaturale infinito, $P(x)$ un predicato.

Vorremmo allora dare una risposta alla seguente domanda: se so che una proprietà 
è valida per un numero infinito, posso ``tornare indietro'' e concludere 
qualcosa riguardo i numeri finiti? Per esempio, se so che $a_N\approx 3$: posso 
dire qualcosa su $a_n$? Facciamo qualche considerazione preliminare.

Diciamo che un predicato $P(n)$ è \emph{definitivamente} vero se esiste un certo 
$\bar{n}$ tale che $P(n)$ è vero per ogni $n$ maggiore di $\bar{n}$. In modo 
meno rigoroso potremmo dire che un predicato è definitivamente vero se è vero da 
un certo punto in poi. 

Per esempio $P(n)=\text{``$n-3>0$''}$ è definitivamente vero, perché è vero per 
tutti gli $n$ maggiori di 3.

\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (10,0);
\foreach \x in {0, 1, 2, 3}
	\draw [fill=white] (\x,0) circle (3pt);
\foreach \x in {4, 5, 6, 7, 8, 9, 10}
	\draw [fill=red] (\x,0) circle (3pt);
\end{tikzpicture}
\end{center}

Se indichiamo con un puntino pieno i numeri naturali per i quali una 
proprietà è 
vera, in corrispondenza di una proprietà definitivamente vera troveremo, 
spostandoci verso destra, soltanto punti pieni.

Diciamo poi che un predicato $P(n)$ è \emph{frequentemente} vero se è vero per 
infiniti valori di $n$. 
Per esempio, $P(n)=\text{``$(-1)^n=1$''}$ è frequentemente vero, perché è vero 
per tutti gli infiniti $n$ pari.

% \begin{center}
% \begin{tikzpicture}
% \draw (0,0) -- (10,0);
% \foreach \x in {0,1,2,3,4,5,6,7,8,9,10}
% 	\filldraw (\x,0) circle (2pt);
% \foreach \x in {4,7,8,10}
% 	\filldraw[red] (\x,0) circle (3pt);
% \end{tikzpicture}
% \end{center}

\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (10,0);
\foreach \x in {0, 1, 2, 3, 5, 6, 9}
	\draw [fill=white] (\x,0) circle (3pt);
\foreach \x in {4, 7, 8, 10}
	\draw [fill=red] (\x,0) circle (3pt);
\end{tikzpicture}
\end{center}

In questo caso l'immagine ci mostra che in corrispondenza di una proprietà 
definitivamente vera non avremo, spostandoci verso destra, un ``ultimo punto'' 
pieno, dopo il quale ci saranno solo punti vuoti; i punti pieni proseguono 
all'infinito, eventualmente intervallati da punti vuoti.

In simboli, possiamo considerare le seguenti quattro categorie di affermazioni:
\begin{itemize}
\item $\forall n\; P(n)$:  ``per ogni $n$, $P(n)$ è vera''. 
\item $P(n) \text{ def}$:  ``$P(n)$ è vero definitivamente'', cioè $\exists 
\bar{n}\; \forall n>\bar{n} \;P(n)$
\item $P(n) \text{ freq}$:  ``$P(n)$ è vero frequentemente'', cioè $P(n)$ è vero 
per infiniti $n$
\item $\exists n\; P(n)$:  ``esiste (almeno) un $n$ tale che $P(n)$ è vero''
\end{itemize}

Vale quindi questa catena di affermazioni:
\[
\forall n\; P(n) \Longrightarrow P(n)\text{ def} \Longrightarrow P(n)\text{ 
freq}\Longrightarrow \exists n\; P(n).
\]

Studiamo ora le negazioni delle precedenti affermazioni. 

Qual è la negazione di ``$P(n)$ è vero definitivamente''?
% \begin{center}
% \begin{tikzpicture}
% \draw (0,0) -- (10,0);
% \foreach \x in {0,1,2,3,4,5,6,7,8,9,10}
% 	\filldraw (\x,0) circle (2pt);
% \foreach \x in {4,5,6,7,8,9,10}
% 	\filldraw[red] (\x,0) circle (3pt);
% \end{tikzpicture}
% \end{center}

\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (10,0);
\foreach \x in {0, 1, 2, 3}
	\draw [fill=white] (\x,0) circle (3pt);
\foreach \x in {4, 5, 6, 7, 8, 9, 10}
	\draw [fill=red] (\x,0) circle (3pt);
\end{tikzpicture}
\end{center}

All'aumentare di $n$ troveremo sempre qualche valore per il quale $P(n)$ è 
falsa, quindi la negazione di ``$P(n)$ è vero definitivamente'' è
``$P(n)$ è falso frequentemente''. Spostandoci verso destra troveremo sempre 
punti vuoti.

% \begin{center}
% \begin{tikzpicture}
% \draw (0,0) -- (10,0);
% \foreach \x in {0,1,2,3,4,5,6,7,8,9,10}
% 	\filldraw (\x,0) circle (2pt);
% \foreach \x in {4,7,8,10}
% 	\filldraw[red] (\x,0) circle (3pt);
% \end{tikzpicture}
% \end{center}

\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (10,0);
\foreach \x in {0, 1, 2, 3, 5, 6, 9}
	\draw [fill=white] (\x,0) circle (3pt);
\foreach \x in {4, 7, 8, 10}
	\draw [fill=red] (\x,0) circle (3pt);
\end{tikzpicture}
\end{center}

Qual è la negazione di ``$P(n)$ è vero frequentemente''?
% \begin{center}
% \begin{tikzpicture}
% \draw (0,0) -- (10,0);
% \foreach \x in {0,1,2,3,4,5,6,7,8,9,10}
% 	\filldraw (\x,0) circle (2pt);
% \foreach \x in {4,7,8,10}
% 	\filldraw[red] (\x,0) circle (3pt);
% \end{tikzpicture}
% \end{center}

\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (10,0);
\foreach \x in {0, 1, 2, 3, 5, 6, 9}
	\draw [fill=white] (\x,0) circle (3pt);
\foreach \x in {4, 7, 8, 10}
	\draw [fill=red] (\x,0) circle (3pt);
\end{tikzpicture}
\end{center}

All'aumentare di $n$ non troveremo sempre qualche valore per cui $P(n)$ è vera, 
quindi la negazione di ``$P(n)$ è vero frequentemente''
sarà ``$P(n)$ è falso definitivamente''.
% \begin{center}
% \begin{tikzpicture}
% \draw (0,0) -- (10,0);
% \foreach \x in {0,1,2,3,4,5,6,7,8,9,10}
% 	\filldraw (\x,0) circle (2pt);
% \foreach \x in {0,1,2,3}
% 	\filldraw[red] (\x,0) circle (3pt);
% \end{tikzpicture}
% \end{center}

\begin{center}
\begin{tikzpicture}
\draw (0,0) -- (10,0);
\foreach \x in {4, 5, 6, 7, 8, 9, 10}
	\draw [fill=white] (\x,0) circle (3pt);
\foreach \x in {0, 1, 2, 3}
	\draw [fill=red] (\x,0) circle (3pt);
\end{tikzpicture}
\end{center}

Ora possiamo enunciare due teoremi.

\bigskip\noindent
\textbf{Teorema 1}.
$P(N)$ è vero per un ipernaturale infinito $N$ se e solo se $P(n)$ è vero per 
infiniti $n$.

Vediamo la parte sufficiente: se $P(N)$ è vero per almeno un $N$, dimostriamo 
che $P(n)$ è vero frequentemente (cioè per infiniti $n$). Supponiamo che sia 
falsa l'affermazione
``$P(n)$ è vero per infiniti $n$'': questo significa che $P(n)$ è falso 
definitivamente. Per il principio di trasferimento si può dire che $P(N)$ è 
falso per ogni $N$, ma questa è una contraddizione.

Consideriamo ora la parte necessaria: se $P(n)$ è vero per infiniti $n$, allora 
dimostriamo che $P(N)$ è vero per almeno un $N$. In questo caso si può applicare 
il principio di trasferimento all'ipotesi ``$P(n)$ è vero per infiniti $n$'', 
per ottenere ``$P(N)$ è vero per infiniti $N$''. Quindi $P(N)$ è certamente vero 
per almeno un $N$, e il teorema è dimostrato.


Ecco un esempio di applicazione. Dato che
$P(n)=\text{``$(-1)^n=1$''}$ è vera frequentemente, per il teorema appena 
dimostrato possiamo dire che esiste almeno un $N$ infinito tale che $(-1)^N=1$. 
E, infatti, esistono ipernaturali pari e dispari.

\bigskip\noindent
\textbf{Teorema 2}
Se $P(N)$ è vero per ogni $N$ ipernaturale infinito, allora $P(n)$ è vero 
definitivamente.

Supponiamo che la tesi ``$P(n)$ vero definitivamente'' sia falsa. Questo 
significa che $P(n)$ è falso frequentemente, e per il teorema 1 si può 
concludere che 
esiste almeno un $N$ tale che $P(N)$ è falso. Ma questa è una contraddizione, 
quindi il teorema è vero.

Per esempio, $\dfrac{7}{N}<1$ per ogni $N$, quindi $\dfrac{7}{n}<1$ è una 
disuguaglianza vera definitivamente. E, in effetti, $\dfrac{7}{n}<1$ è vera per 
$n>7$.

\section{Il teorema di limitatezza delle successioni convergenti}
I due teoremi dimostrati precedentemente possono essere usati per dimostrare 
teoremi che, nelle loro ipotesi, contengono proprietà valide per indici infiniti 
e, nelle tesi, contengono invece proprietà valide per indici finiti. Per 
esempio, il teorema di limitatezza delle successioni convergenti: se una 
successione converge, allora è limitata.

Data una successione $(s_n)_n$, l'ipotesi significa che la parte standard 
$\st(s_N)=s$ per ogni $N$ ipernaturale infinito (dove $s$ è un numero reale). 
Questa affermazione, però, non è l'estensione di nessuna affermazione standard, 
e dobbiamo quindi trasformarla: se $s_N$ è infinitamente vicino a $s$, significa 
che la distanza tra i due è certamente minore di 1, quindi $s-1 < s_N < s+1$.

Per il teorema 2, possiamo concludere che $s-1 < s_n < s+1$ definitivamente, e 
quindi la successione è limitata.


\section{Scherzo finale}

Sia dato il predicato $P(n) = ``\text{$n$ è un numero primo''}$, e consideriamo 
$N!$, con $N$ ipernaturale infinito. Si ha che $N!$ è divisibile per ogni $n$ 
naturale finito, per definizione di fattoriale. Allora
$N!+1$ non è divisibile per nessun $n$ (maggiore di 1), e quindi o $N!+1$ è 
primo o è divisibile per un numero primo non standard. In ogni caso, $P(K)$ è 
vera per almeno un $K$ ipernaturale infinito. Per il teorema 1, 
$P(n)$ è vera frequentemente, e quindi esistono infiniti numeri primi. 
Ed ecco ridimostrato il teorema di Ipereuclide sull'infinità dei numeri 
primi.


%\end{document}

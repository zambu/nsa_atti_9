%\chapter{Una definizione algebrico-geometrica di derivata}
\makecapitolo
{Loredana Biacino - già Università Federico II, Napoli}
{Una definizione algebrico-geometrica di derivata}
{L’Annotazione n.32 di Peano al trattato di analisi infinitesimale 
Genocchi-Peano prende spunto da una definizione geometrica di derivata presente 
nel secondo libro del Treatise of Fluxions di Maclaurin, definizione che non 
fa 
riferimento alla cinematica o alla teoria delle prime e ultime ragioni di 
grandezze evanescenti. Peano la traduce semplificandola e precisandola in modo 
da ottenere una definizione rigorosa. Nella comunicazione a tale definizione è 
data una veste aritmetica, nel senso che la derivata è definita come elemento 
di 
separazione di un’opportuna classe di insiemi separati e contigui. Inoltre, 
sempre prendendo spunto da un’idea di Peano, si evidenziano due tipi di derivabilità 
equivalenti alla continuità della derivata.
}

\section{Introduzione}
Il presente articolo è nato leggendo un contributo minore di Giuseppe Peano 
(1858-1932), che, ancora giovane, a Torino, nel 1884 si dedica alla stesura del 
celebre trattato di analisi infinitesimale, il Genocchi-Peano~\cite{GP}.
Di questo libro egli scrive gran parte e soprattutto la \emph{Prefazione} e le 
\emph{Annotazioni}.
Proprio a una di tali Annotazioni, la N.32, mi riferisco: si tratta di un 
contributo di carattere didattico, completamente in linea con gli innumerevoli 
altri interventi nel testo, che denotano, come sottolineato ad esempio da Beppo 
Levi, la tensione del giovane Peano verso la chiarificazione dei termini, delle 
definizioni e del contenuto di ipotesi e dimostrazioni piuttosto che verso la 
ricerca di risultati originali. D’altronde, osserverà Beniamino Segre, è 
proprio questo desiderio di rigore l’elemento originale di questo suo primo 
lavoro che pervaderà tutta la sua opera approdando qualche anno dopo alla 
realizzazione del \emph{Formulario}, motivo di ispirazione diretta o indiretta 
del Bourbakismo nel suo progetto di assiomatizzazione e astrazione della 
matematica.~\footnote{I giudizi di Beppo Levi e Corrado Segre cui mi riferisco 
sono pubblicati in \cite{LP}.} \\ 
Ma veniamo alla Annotazione N.32: Peano ricorda che Maclaurin, nel secondo 
libro del suo trattato sulle flussioni del 1742~\cite{ML} ad un certo punto 
della trattazione fornisce una definizione di flussione in termini puramente 
geometrici, senza riferimento alcuno a interpretazioni cinematiche o alla 
teoria delle prime e ultime ragioni di incrementi evanescenti. Peano crede 
opportuno riprodurre il procedimento usato da Maclaurin in poche 
parole.
% ~\footnote{
% Il metodo geometrico di Maclaurin come una base della definizione di limite si 
% può trovare negli scritti di D'Alembert, L'Huilier, Lacroix, che citano tutti 
% Maclaurin.} 
Secondo Maclaurin la flussione di una quantità rispetto ad un’altra 
quantità è: \emph{any measures of their respective rates of increase or 
decrease 
while they vary, or flow, together}.\\
Maclaurin usa l’espressione:
\emph{a quantity increases by differences that are always greater than the 
successive differences by which another quantity increases}.
Quindi se $f(x)$ e $g(x$) denotano le quantità, se $f(x_0)=g(x_0)$ e $h>0$ 
allora da 
$f(x_0+h)-f(x_0)> g(x_0+h)-g(x_0)$  e da $f(x_0)-f(x_0-h)>g(x_0)- g(x_0-h)$ 
segue $f(x_0-h)-g(x_0-h)<0$ e $f(x_0+h)-g(x_0+h)>0$. 
Maclaurin chiarisce in diversi 
punti della trattazione che si riferisce a valori piccoli di $h$.\\
Peano traduce allora l’espressione precedente dicendo che $f(x)$ cresce più 
rapidamente di $g(x)$ o anche che $g(x)$ cresce meno rapidamente di $f(x)$ se 
accade che:
\begin{center}
 \(\mathbf{f(x)-g(x)}\) \textbf{è crescente (strettamente) nel punto} 
 \(\mathbf{x=x_0} \)
\end{center}
Per una funzione lineare il rapporto degli incrementi delle variabili 
dipendente e indipendente è costante e Maclaurin dà a tale costante il nome di 
\textbf{flussione della funzione lineare} in ogni punto (\textbf{derivata} 
nella terminologia di Peano).\\
Per una funzione non lineare $f(x)$ Peano traduce le parole di Maclaurin 
scrivendo che essa ha \textbf{derivata eguale a} $\mathbf{f’(x_0)}$ se:\\
\emph{ per $x=x_0$, $f(x)$ cresce meno rapidamente di ogni funzione lineare 
avente derivata maggiore di $f’(x_0)$ e più rapidamente di ogni funzione 
lineare 
avente derivata minore di} $f’(x_0)$. \\
Con questo si conclude la definizione di Peano: possiamo esplicitarla 
scrivendo l’equazione della funzione lineare avente derivata $k$ e il cui 
diagramma passa per $(x_0, f(x_0))$:\\
\indent $y=f(x_0)+k(x-x_0)$.\\
Allora $f’(x_0)$ è un numero (se esiste) tale che:  
per  ogni $k>f’(x_0)$ esiste un intorno $I$ di $x_0$ tale che:\\
$(x,y \in I,\ x\neq y,\ x\leq x_0\leq y) \Rightarrow$\\
$f(x)-f(x_0)-k(x-x_0)>0>f(y)-f(x_0)-k(y-x_0) 
\Rightarrow k > \dfrac{f(x)-f(y)}{x-y}$;\\
analogamente per ogni $k<f’(x_0)$ esiste un intorno $I$ di $x_0$ tale che:\\
$(x,y\in I,\ x\neq y,\ x\leq x_0\leq y) \Rightarrow  
k<\dfrac{f(x)-f(y)}{x-y}$.\\
Quindi $f’(x_0)$ è un numero (se esiste) tale che per ogni $\epsilon>0$ esiste 
un intorno $I$ di $x_0$ tale che:\\
 $x \in I,\ x\neq x_0,\ \Rightarrow \ f'(x_0)-\epsilon < 
 \dfrac{f(x)-f(x_0)}{x-x_0} < f'(x_0)+\epsilon$.\\

\begin{inaccessibleblock}
  [grafico 1]
  \begin{minipage}[]{.6\textwidth}
    %\begin{center} \falsodifferenziale \end{center}
    \begin{center} 
      \includegraphics[scale=0.9]{chap/01_interventi/03_biacino/grafico2.png}
    \end{center}
 \end{minipage} 
  \hfill
 \begin{minipage}[]{.25\textwidth}
In questo  modo Peano riesce a semplificare e a tradurre nella rigorosa 
formulazione di limite di Weierstrass la complessa ma interessantissima 
definizione geometrica data da Maclaurin nel suo trattato.  
 \end{minipage}
\end{inaccessibleblock}
\label{}

\begin{esempio}
\label{esempio1}
Calcolare la derivata di $f(x)= x^2-5x$ nel punto $P=(1, -4)$.\\ 
Equazione della generica retta per $P$: $y+4=k(x-1)$;\\
Differenza: $p(x)=f(x)-y= x^2-5x+4-k(x-1)=(x-1)(x-4-k)$.\\ 
Determiniamo i valori di $k$ per cui tale differenza è crescente in $x=1$: deve 
risultare $p(x)<0$ per $x<1$ e $p(x)>0$ per $x>1$, quindi in entrambi i casi 
$x-4-k>0$ da cui $k<x-4$; possiamo prendere $x$ molto vicino a $1$, e quindi se 
$k<-3$ 
sicuramente risulta anche $k<x-4$ per $x$ abbastanza vicino a $1$.\\
Analogamente determiniamo i valori di $k$ per cui $p(x)$ è decrescente in 
$x=1$: 
deve risultare $p(x)>0$ per $x<1$  e $p(x)<0$ per $x>1$; quindi in entrambi i 
casi 
$x-4-k<0$, cioè $k>x-4$ e pertanto in questo caso se $k>-3$ siamo sicuri che 
risulta  $x-4-k<0$ per $x$ abbastanza prossimo a $1$.\\ 
Quindi la derivata richiesta è $f’(1)=-3$.
\end{esempio}

\begin{inaccessibleblock}
  [grafico 2]
  \begin{minipage}[]{.4\textwidth}
    Dalla formalizzazione di Peano del procedimento di Maclaurin noi possiamo 
    anche dedurre che $f’(x_0)$ è un numero, se esiste, tale che sussiste la 
    seguente definizione equivalente:
  \end{minipage} 
  \hfill
 \begin{minipage}[]{.5\textwidth}
 \begin{center} 
      \includegraphics[scale=0.6]{chap/01_interventi/03_biacino/grafico1.png}
    \end{center}
 \end{minipage}
\end{inaccessibleblock}
\label{}
\[
   f'(x_0)=\lim_{x,y \rightarrow x_0}\frac{f(x)-f(y)}{x-y}\ 
   \text{ dove }x\leq x_0\leq y,\ x\neq y.
  \]   
Se nella precedente relazione noi non richiediamo che sia $x\leq x_0\leq y$, 
otteniamo una definizione  più restrittiva di derivata di quella classica non 
equivalente ad essa su cui torneremo nella sezione 3. 

\section{Una definizione aritmetica di derivata.}
L’osservazione di Peano ci porta a dare la seguente definizione. \\ 
Per ogni funzione lineare la derivata in ogni punto è il coefficiente 
angolare della retta diagramma: indichiamo con $y_k$  una funzione lineare la 
cui derivata è $k$.\\
È evidente che date due funzioni lineari $y_k$ e $y_h$, è $k>h$ se e solo 
se 
la differenza $y_k - y_h$ è crescente.\\
Data  una funzione reale  $f(x)$ non lineare definita nell’intervallo 
$(a,b)$, 
sia $x_0$ un punto di tale intervallo e $P=(x_0,f(x_0))$ il corrispondente 
punto del diagramma. Consideriamo i due seguenti insiemi di numeri reali:\\
$A=$\{$k:\ k$ è la derivata di una funzione lineare $y_k$ il cui diagramma 
passa per $P$ tale che $f(x)-y_k$ è crescente in $x_0$\};\\
$B=$\{$k:\ k$ è la derivata di una funzione lineare $y_k$ il cui diagramma 
passa per $P$ tale che $f(x)-y_k$ è decrescente in $x_0$\}.\\
I due insiemi $A$ e $B$ possono essere vuoti, uno o entrambi, come mostrano 
i 
seguenti esempi.\\
\begin{esempio} 
\label{esempio2} 
Sia$ f(x)=\sqrt{|x|},\ P=(0, 0)$. Se $k\neq 0$ allora $f(x)-kx>0$ per 
$x\neq 0$ e $|x|< \dfrac{1}{k^2}$, mentre se $k=0$ 
risulta $f(x)>0$ per ogni $x\neq 0$; quindi $f(x)-kx$ in ogni caso presenta 
per 
$x=0$ un punto di minimo relativo. Ne segue che $A=\emptyset$ e 
$B=\emptyset$.
\end{esempio}

\begin{center} 
      
\includegraphics[scale=0.6]{chap/01_interventi/03_biacino/grafico3.png}
\end{center}

\begin{esempio} 
\label{esempio3}
Sia $f(x)=\sqrt{-x},\ P=(0, 0)$. Se $x\leq 0$, $f(x)=\sqrt{x}$ se $x>0$.\\ 
Allora per ogni $k\in \R$  risulta $f(x)-kx<0$ per $x<0$ e $f(x)-kx>0$ per 
$x>0$, cioè $A=\R$ e $B=\emptyset$;  considerando la 
funzione $f(x)= \sqrt{-x}$ se $x\leq 0$, $f(x)=-\sqrt{x}$ se $x>0$ si ha 
$A=\emptyset$ e $B=\R$. 
\end{esempio}
Consideriamo anche altri casi che possono presentarsi.

\begin{esempio}
\label{esempio4}
Sia  $f(x)=x$ per $0\leq x<1$, $f(x)=1$  per $1\leq x\leq 2,\ P=(1, 1)$. Allora 
$A=(-\infty,0)$, $B=(1, +\infty)$.
\end{esempio}
 
\begin{esempio} 
\label{esempio5}
 Sia $f(x)=x$ per $-1\leq x\leq 0$, $f(x)=0$ per $0<x\leq 1$,\\ $P=(0, 0)$. Allora 
$A=(-\infty, -1)$, $B=(0,+\infty)$.
\end{esempio}

Sussiste la seguente:\\
\textbf{Proposizione –} Gli insiemi $A$ e $B$, se entrambi non vuoti, sono 
due 
intervalli separati e illimitati di numeri reali, inferiormente il primo, 
superiormente il secondo.\\
\underline{Dim.} Infatti sia $k\in A$ e $h<k$; si ha  $f(x)-y_h= 
(f(x)-y_k)+(y_k-y_h)$ e quindi, essendo somma di due funzioni  crescenti in 
$x_0$, $f(x)-y_h$  è una funzione crescente in $x_0$. Ne segue $h\in A$.\\ 
Analogamente si dimostra che $B$ è un intervallo illimitato superiormente.\\
Inoltre, se $k\in A$ e $h\in B$  per $x$ appartenente ad un opportuno 
intorno 
sinistro di $x_0$, risulta $f(x)-y_k<0$ e $f(x)-y_h>0$,  da cui segue  
$f(x)-y_k<f(x)-y_h$   e quindi $k<h$, cioè i due insiemi $A$ e $B$ sono 
separati.

\begin{definizione}
Data la funzione $f(x)$ definita nell’intervallo $(a,b)$ e dato il 
punto $x_0\in(a,b)$ si dice che $f(x)$ è derivabile in $x_0$ se i due 
insiemi 
$A$ e $B$ sono entrambi non vuoti e costituiscono una coppia di insiemi 
separati contigui. In tal caso l’unico elemento di separazione è detto la 
derivata di $f(x)$ in $x_0$ e denotato con $f’(x_0)$. 
\end{definizione}

La funzione considerata nell’Esempio~\ref{esempio1} è derivabile nel punto 
(1,-4) nel senso ora definito in quanto in tal caso $A=(-\infty,-3)$ e 
$B=(-3,\infty)$ e $-3$ è la derivata. Le funzioni degli 
esempi~\ref{esempio2}, 
\ref{esempio3}, \ref{esempio4}, \ref{esempio5} invece non sono derivabili 
nei 
punti considerati.

\begin{osservazione}
È lecito considerare $A$ e $B$ costituiti solo da numeri razionali, e 
quindi 
la 
derivabilità equivale al fatto che la coppia $(A,B)$ costituisce una 
sezione 
del 
campo dei numeri razionali.
\end{osservazione}

\begin{osservazione}
La definizione data da Maclaurin è assolutamente rigorosa ed è affiancata 
da una procedura per il calcolo effettivo. Maclaurin dimostra le regole per il 
calcolo, derivata di somma, prodotto, rapporto, potenza, e considera anche 
l’integrazione, come operazione inversa della derivazione; ma la 
dimostrazione è complicata e a volte tortuosa. 
Questa è stata la motivazione fondamentale per cui un simile approccio, 
nonostante il suo interesse teorico-filosofico e la diffusione (il metodo 
geometrico di Maclaurin come una base per la definizione di limite si può 
trovare negli scritti di d’Alembert, L’Huilier, Lacroix,  che citano tutti 
Maclaurin), non ha avuto presa in ambito matematico e si è preferito il 
metodo dei differenziali, anche se inizialmente non giustificato in modo 
soddisfacente nei suoi fondamenti, perché corredato da uno svelto insieme 
di regole per cui si può meccanicamente calcolare la derivata di una 
qualsiasi funzione ottenuta con le operazioni algebriche dalle funzioni 
elementari. 
Ed infatti una differenza enorme separa il ponderoso trattato del Maclaurin 
dal primo lavoro pubblicato nel 1684 sul calcolo differenziale da Leibniz, 
il cui titolo recita: \emph{Nuovo metodo per i massimi e minimi come pure 
per le tangenti che non si arresta davanti a quantità frazionarie e 
irrazionali ed un singolare genere di  calcolo per quei problemi}, dove 
l’accento cade proprio sul calcolo proposto per i differenziali e i loro 
rapporti e occupa gran parte dell’articolo. 
E la teoria dei differenziali appena proposta si diffuse ed ebbe successo 
proprio perché forniva regole precise e algoritmi veloci per la risoluzione 
effettiva dei problemi. La definizione geometrica data da Maclaurin nel 1742 
si può chiaramente collegare alla critica stringente che  Berkeley aveva 
fatto al calcolo delle flussioni di Newton ne L’Analista~\cite{BE}: 
si trattava di dare una base teorica incontestabile alle procedure ormai in 
auge nei calcoli, mettendosi al riparo dalle giuste critiche. 
Ne segue una trattazione ricca di tantissime parole (che la flussione di 
$ay$ con $a$ costante sia eguale ad $a$ per la flussione di $y$ lo si 
apprende solo a pag.162) e poi, nel libro II la prima esplicita descrizione 
della tangente come limite delle secanti prima esposta, che ha un valore 
emblematico, in quanto afferma Maclaurin:  
come la tangente di un arco è la posizione limite di tutte le secanti che 
passano per il punto di contatto pur non essendo essa  stessa una secante, 
così un rapporto può essere  limite di rapporti variabili di incrementi 
sebbene non sia esso stesso il  rapporto di effettivi 
incrementi.~\footnote{Un lavoro che considera il contributo di Maclaurin 
alla matematica del continente è~\cite{GR}.}
\end{osservazione}

\textbf{Nota –} Durante la Giornata di Analisi Non Standard, tenutasi a 
Verona il 5 Ottobre 2019, sono venuta a conoscenza del fatto che il 
procedimento di derivazione esposto nelle pagine precedenti è alla base di 
una trattazione degli anni '80 del calcolo differenziale, di carattere 
didattico, dove sono anche  dimostrate le regole algebriche della 
derivazione e tutti i teoremi relativi al calcolo differenziale e  
integrale. Il testo, reperibile in rete, è intitolato \emph{Calculus 
Unlimited}, gli autori sono due professori universitari, Jerrold Marsden e 
Alan Weinstein; la trattazione è però completamente indipendente e non fa  
riferimento alle motivazioni storiche che la produssero nell’antichità. 

\section{Diversi tipi di derivate. Diverse implicazioni della derivabilità 
in un punto.}

 “Per le funzioni reali di variabile reale, la derivata può esistere ed 
essere discontinua. Ma la derivata in questo caso è il limite del rapporto 
incrementale, ove un estremo sia fisso, e l’altro mobile tendente al primo. 
Se per derivata si intendesse il limite del rapporto incrementale ove ambo 
i valori dati alla variabile tendono a uno stesso valore, \dots allora questa 
derivata, supposta esistente, risulta funzione continua.” 
(G.Peano~\cite{PE1})\\
Il 7 ottobre 1882 il giovane assistente Peano, impegnato nelle lezioni di 
analisi del corso tenuto dal suo professore Genocchi, scrive a questi 
sottoponendogli alcune sue idee. Tra l’altro scrive:\\
``Se $f(x)$ è una funzione avente derivata finita per tutti gli $x$ di uno 
stesso 
intervallo e per ogni $\alpha$ esiste $\epsilon$ tale che per ogni $h\leq 
\epsilon$ risulti sempre:
\[
 \left \lvert\frac{f(x+h)-f(x)}{h}-f'(x)\right \rvert\leq \alpha
\]
per ogni $x$ compreso nello stesso intervallo, allora $f’(x)$ è una 
funzione continua''~\cite{BO}.\\
Peano dimostra nella lettera che l’uniforme derivabilità è condizione 
sufficiente per la continuità della derivata. Successivamente, nel trattato 
Genocchi - Peano, egli osserverà a pag.50 che la condizione è necessaria e 
sufficiente.\\
Nella stessa lettera scrive che se $f(x)$ ammette derivata in tutto 
l’intervallo e se col tendere di $x$ ad $a$, $f’(x)$ tende verso un limite, 
questo 
limite vale $f’(a)$. Ma, osserva ancora Peano, pur esistendo $f’(a)$, la 
derivata potrebbe non tendere ad un limite per $x$ tendente ad $a$, come 
avviene per la funzione:  $f(x)=0$ se $x=0$ e  $f(x)= x^2\sin\dfrac{1}{x}$  
per $x\neq 0$ che si trova nel trattato di Dini~\cite{DI}. \\
Solo qualche anno dopo, nel 1892, in una breve nota sulla rivista belga 
Mathesis~\cite{PE3} introdurrà una nuova definizione di derivata nel punto 
$x$ come limite del rapporto:
\[
 \frac{f(x_1)-f(x_2)}{x_1-x_2} \text { al tendere di }x_1
 \text{e di }x_2\text{ a }x.
\]
Prendendo spunto da tale definizione, data la funzione $f(x)$ definita 
nell’intervallo $(a,b)$ e un punto $x_0\in (a,b)$, diciamo che $\mathbf{f(x)}$ 
è \textbf{uniformemente derivabile nel punto} $\mathbf{x_0}$ se esiste 
finito il limite, detto \textbf{derivata uniforme di} $\mathbf{f(x)}$ 
\textbf{in} $\mathbf{x_0}$ e denotato al solito con $f’(x_0)$: 
\[
 \lim_{x,y\rightarrow x_0}\frac{f(x)-f(y)}{x-y}
\]
nel senso che per ogni $\epsilon>0$ esiste $\delta>0$ tale che
\begin{equation*}
 (*) \qquad\qquad\abs{f'(x_0)-\frac{f(x)-f(y)}{x-y}} <\epsilon
 \qquad\text{per ogni }x,y\in]x_0-\delta, x_0+\delta[, \text{ con }x\neq y.
\end{equation*}

È evidente che:
\begin{center}
$(f$ derivabile uniformemente in $x_0)\ \Rightarrow\ (f$ derivabile in $x_0$)
\end{center}
ma non vale l’implicazione inversa, come dimostra il seguente:

\begin{esempio}
Consideriamo nuovamente la funzione:  $f(x)=0$ se $x=0$ e 
$f(x)= x^2\sin\dfrac{1}{x}$ per $x\neq 0$ la $f(x)$ è derivabile ovunque in 
senso ordinario, con derivata data da:
$f’(0)=0$ ; $f’(x)=2x\sin \dfrac{1}{x} - \cos\dfrac{1}{x}$ per $x\neq 0$; ma 
non è derivabile in $x=0$ nel nuovo senso.\\ 
Infatti se poniamo  $y=\dfrac{x}{1+\pi x}$ otteniamo:\\
\[
x-y= \dfrac{\pi x^2}{1+\pi x}\quad \text{ e } \quad
\dfrac{f(x)-f(y)}{x-y}=\dfrac{[(1+\pi x)^2+1]}{\pi(1+\pi x)}\sin \dfrac{1}{x},
\]
da cui si trae che il 
\[\lim_{x,y\rightarrow x_0}\dfrac{f(x)-f(y)}{x-y}\]
non esiste.
\end{esempio}
 
\noindent \textbf{Proposizione -} Sia $f(x$) derivabile in un intorno di $x_0$. 
Allora $f(x)$ è derivabile uniformemente in $x_0$ se e solo se la derivata 
$f’(x)$ è continua in $x_0$.\\
\noindent \underline{Dim.}
Supponiamo che $f(x)$ sia derivabile uniformemente in $x_0$ e quindi valga la 
(*). Fissiamo $\epsilon>0$ e sia $|x-x_0|<\delta$. Sia $r<\delta-|x-x_0|$ e sia 
$y$ tale che $|y-x|<r$; quindi $|y-x_0|<|y-x|+|x-x_0|<\delta$. Allora, poiché 
$r$ può essere scelto arbitrariamente piccolo, passando al limite nella (*) vale
$|f’(x_0)-f’(x)|<\epsilon$ per ogni $x$ tale che $|x-x_0|<\delta$. Ne segue che 
per ogni $\epsilon>0$ esiste $\delta>0$ tale che se $|x-x_0|<\delta$ allora 
$|f’(x_0)-f’(x)|<\epsilon$, cioè la derivata è continua in $x_0$.~\footnote{
La dimostrazione nel caso di funzioni d'insieme additive si trova in 
\cite{PE3}.}\\
Viceversa supponiamo $f’(x)$ continua in $x_0$: per ogni $\epsilon>0$ esiste 
$\delta>0$ tale che $f’(x_0)-\epsilon<f’(x)<f’(x_0)+\epsilon$ per ogni $x$ tale 
che $|x-x_0|<\delta$. Siano $x$ e $y$ due punti distinti dell’intervallo 
$]x_0-\delta, x_0+\delta[$ e sia ad es. $x<y$: in base al teorema di 
Lagrange esiste un punto $z\in]x,y[\subseteq]x_0-\delta, x_0+\delta[$ tale che
$\dfrac{f(x)-f(y)}{x-y}= f’(z)$, ne segue che per ogni $\epsilon>0$ esiste 
$\delta>0$ tale che per ogni $x,y\in]x_0-\delta,x_0+\delta[$ con $x\neq y$ 
risulta:
\[
 f'(x_0)-\epsilon<\frac{f(x)-f(y)}{x-y}<f'(x_0)+\epsilon,
\]
cioè $f(x)$ è uniformemente derivabile in $x_0$.\\

Si osservi che nell’esempio precedente nel punto x=0, dove la funzione non è 
uniformemente derivabile, la derivata non è continua, concordemente con la 
proposizione ora dimostrata.

\newpage
\begin{thebibliography}{10}
\phantomsection
\addcontentsline{toc}{section}{Bibliografia}

\bibitem[BE]{BE}
\textsc{Berkeley, George} (1734),
\emph{The Analist; or a Discourse Addressed to an Infidel Mathematician},
London.

\bibitem[BO]{BO}
\textsc{Borgato, Maria Teresa} (1991),
\emph{Alcune lettere inedite di Peano a genocchi e a Jordan sui fondamenti 
dell'analisi},
in Angelo Genocchi e i suoi interlocutori scientifici. Contributi 
dal'epistolario, a cura di Alberto Conte e Livia Giacarti, Deputazione 
Subalpina di Storia Patria, Torino, pp.61-97.

\bibitem[DI]{DI}
\textsc{Dini, Ulisse} (1878),
\emph{Fondamenti per la teorica delle funzioni di variabili reali},
Nistri, \S78.

\bibitem[GP]{GP} 
\textsc{Genocchi, Angelo} (1884), 
\emph{Calcolo differenziale e principi di calcolo integrale pubblicati con 
aggiunte del Dr.Giuseppe Peano}, 
Bocca, Torino.

\bibitem[GR]{GR}
\textsc{Grabiner, Judith} (1997),
\emph{Was Newton Calcolus a Dead End? The Continental Influence of Maclaurin’s 
Treatise of Fluxions}, The Amer. Math. Monthly, Vol.104, N.5, pp.394-410.

\bibitem[LP]{LP}
\textsc{}
\emph{In Memoria di Giuseppe Peano}, pubblicato nel 1995 a cura del Liceo 
Scientifico ``G.Peano`` di Cuneo.

\bibitem[ML]{ML} 
\textsc{Maclaurin, Colin} (1742) , 
\emph{A Treatise of Fluxions}, Vol.II,
Ruddimans, Edinburgh.

\bibitem[PE1]{PE1}
\textsc{Peano, Giuseppe} (1914-15),
\emph{Le grandezze coesistenti di Cauchy},
Atti Acc. Sci. Torino, 1914-15.

\bibitem[PE2]{PE2}
\textsc{Peano, Giuseppe} (1892),
\emph{Sur la definition de la derivée},
Mathesis Gand 2 serie, 2, pp.12-14.

\bibitem[PE3]{PE3}
\textsc{Peano, Giuseppe} (1887),
\emph{Applicazioni geometriche del calcolo infinitesimale},
Bocca, Torino, pag.171.

\end{thebibliography}

\chapter[De l'H\^opital: gli infinitesimi e la regola]
{\LARGE    Gli infinitesimi nell'opera del marchese de
l'H\^opital e l'origine della sua famosa regola.}
\begin{center}
 Leonardo Aldegheri - Liceo Scientifico Messedaglia, Verona
\end{center}
\vspace{1cm}
{\em In questo breve intervento si propone la riscoperta del testo “Analyse des 
infiniment petits pour l'intelligence des lignes courbes”, pubblicato nel 1696 
dal marchese de l'H\^opital, riconosciuto come il primo manuale di analisi 
infinitesimale e che prende le mosse dalle ricerche di Leibniz e dei fratelli 
Jakob e Johann Bernoulli. In particolare si illustreranno i primi elementi di 
calcolo differenziale, o delle differenze, presenti nell' ``Analyse'', 
soffermandosi sul concetto di infinitesimo usato da de l'H\^opital e sui 
postulati 
da cui egli parte per la costruzione di questo “nuova” analisi matematica. 
Infine ci si soffermerà sulla famosa regola di de l'H\^opital notando come 
appare 
nel manuale e ricercandone l'origine nel carteggio che si scambiarono de 
l'H\^opital e Johann Bernoulli tra il 1693 e il 1694.}


\section{Introduzione e note storiche}

Tra i diversi teoremi sul calcolo differenziale che vengono insegnati 
all'ultimo anno del liceo, il Teorema di de l'H\^opital e la conseguente regola 
appaiono agli studenti spesso come un mero trucco per facilitare la risoluzione 
di limiti ritenuti complessi perché presentano forme indeterminate di non 
immediata risoluzione. 
In effetti, nella concezione classica dell'analisi matematica il 
teorema e la regola di de l'H\^opital sono intimamente legati al concetto di 
limite e le loro stesse definizioni, nell'analisi classica, non ne possono 
prescindere. 
Lo spunto per questo lavoro viene dalla semplice considerazione storica che il 
marchese de l'H\^opital vive nella seconda metà del XVII secolo, mentre il 
concetto di limite viene elaborato alla fine del XVIII secolo per venire 
formalizzato solo nell'Ottocento. Almeno in origine, alla loro prima 
apparizione, le considerazioni del marchese francese non potevano essere legate 
al concetto di limite. 

Soffermiamoci su una brevissima storia del concetto di limite. Un'idea non 
delineata completamente di limite l'avevano sia Newton che Leibniz, 
quest'ultimo 
lo dimostra quando scrive del metodo di esaustione, tuttavia è solo con 
D'Alembert alla fine del Settecento che sia arriva ad un concetto abbastanza 
delineato di limite. D'Alembert è così colpito dall'idea di limite che 
riferisce di questo concetto in diversi articoli dell'Encyclopedie, affermando 
che “La teoria dei limiti è alla base della vera Metafisica del calcolo 
differenziale”.\footnote{Cfr.\cite[p.~329]{Ro}.}
La definizione che d'Alembert dà di limite è abbastanza vicina 
a quella odierna, ma rimane informale; la definizione rigorosa e la 
strutturazione della teoria del limite la daranno Cauchy e Weierstrass 
nell'Ottocento. Il primo, partendo dalla nozione di infinitesimo, sviluppa e 
precisa la nozione di limite e ne struttura rigorosamente la teoria, il secondo 
completerà il lavoro con la fondamentale definizione di limite espressa con la
notazione \(\epsilon\)-\(\delta\).

Prima di inoltrarci nella disamina del testo del 1696, credo siano necessarie 
alcune brevi note biografiche, che inquadrino il personaggio e il periodo 
storico.
Guillaume François Antoine de Sainte Mesme, marchese de l'H\^opital, nacque a 
Parigi nel 1661 da nobile famiglia e, dopo aver raggiunto il grado di capitano, 
abbandonò la carriera militare per un problema alla vista, per poi dedicare il 
resto della sua vita alla matematica. De l'H\^opital si distinse fin da giovane
per le sue capacità matematiche, risolvendo a soli quindici anni il problema 
della 
cicloide di Pascal e il problema della brachistocrona, 
posto da Jacob Bernoulli. Appassionato del neonato calcolo infinitesimale, 
diventò, intorno al 1691, allievo di Johann Bernoulli, conosciuto a Parigi e di 
sei anni più giovane. 

Il matematico svizzero diede molte lezioni private 
all'interessatissimo marchese e i due si scambiarono moltissime lettere fino 
alla morte di quest'ultimo. Nel 1694 i due strinsero un patto segreto: il 
marchese avrebbe corrisposto al brillante matematico svizzero un vitalizio di 
300 lire francesi all'anno per intrattenere una fitta corrispondenza su
tutti i problemi matematici che il marchese gli avrebbe posto e soprattutto 
per tenerlo informato delle sue scoperte sul calcolo 
infinitesimale, con il divieto di diffonderle ad altri. Tale patto cessò solo
alla morte di de l'H\^opital, avvenuta nel 1704, allorché Bernoulli poté 
svelare l'accordo e legittimare alcune sue scoperte. 

Due sono le principali opere lasciate da de l'H\^opital: “\emph{L'analisi degli 
infinitamente piccoli, per la comprensione delle linee curve}”, la cui prima 
edizione, come detto, è del 1696 e la seconda del 1715, con successive 
traduzioni in inglese e latino, e il “\emph{Trattato delle sezioni coniche}”, 
pubblicato postumo nel 1707.
 
Inoltriamoci ora nel testo originale: “\emph{Analyse des infiniment petits pour 
l'intelligence des lignes courbes}”, di cui ho personalmente tradotto dal 
francese i passaggi più significativi, inerenti il calcolo infinitesimale e la 
famosa regola, che ancora oggi porta il nome di regola di de l'H\^opital.

\section{La prefazione e la tavola dei contenuti}
Leggiamo dalla prefazione:\footnote{La traduzione dei brani citati in \cite{HO}
è a cura dell'autore. Nelle citazioni i corsivi rispecchiano l'originale e
gli enti matematici si attengono alla notazione originale, pur con lo stile 
moderno dei caratteri.} 

\begin{figure}[h]
\begin{minipage}{.5\textwidth}
\leftskip=8mm
\small L'Analisi che spieghiamo in questo lavoro 
presuppone quella comune, ma ne differisce fortemente. L'Analisi ordinaria 
tratta le grandezze finite: questa penetra sin dentro l'infinito stesso. Essa 
compara le differenze infinitamente piccole di grandezze finite, scoprendo i 
rapporti fra queste differenze: e da questo rende noti quelli tra grandezze 
finite, che comparate con quelle infinitamente piccole sono come infinite.
Potremmo persino dire che quest'Analisi si estende al di là dell'infinito: 
perché essa non si limita alle differenze infinitamente piccole, ma scopre i 
rapporti tra le differenze di queste differenze e ancora quelli di differenze 
terze, quarte e così via, senza trovare mai un termine che la possa 
arrestare.
% \cite[p.~III]{HO}
\end{minipage}
\hfill
\begin{minipage}{.47\textwidth} %
 \centering
 \includegraphics[width=0.8\textwidth]{chap/01_interventi/01_aldegheri/fig01}
  \caption*{\footnotesize Frontespizio dell'\emph{Analyse}, ed.1696}
 \label{f01}
\end{minipage}
\vspace{-.7em}
\begin{quoting}
Così essa abbraccia non solo l'infinito, ma l'infinito dell'infinito
o un infinito di infiniti. Un'Analisi di questo tipo potrebbe portarci ai veri 
principi delle linee curve. Perché le curve non sono altro che poligoni con 
infiniti lati e differiscono tra loro solo nelle differenze negli angoli che 
questi lati infinitamente piccoli formano tra loro. È solo l'analisi 
dell'infinitamente piccolo che può determinare le posizioni di questi lati per 
trovare le curvature che formano, cioè a dire, le tangenti a queste curve, le 
loro perpendicolari, i loro punti di inflessione o cuspidi, i raggi che 
riflettono o rifrangono, ecc. \cite[p.~III]{HO}
\end{quoting}
\end{figure}

Appena conclusa la prefazione il matematico francese riporta con ordine gli 
argomenti che tratterà, nella tavola che segue:\cite[p.~XVII]{HO}\\

\begin{quoting}
\begin{tabular}{lp{.75\textwidth}}
 \toprule
 Sezione I:   &  dove diamo le \emph{regole del calcolo delle differenze};\\
 Sezione II:  &  uso del calcolo delle differenze per \emph{trovare le 
 tangenti} a tutti i tipi di linee curve;\\
\end{tabular}
\begin{tabular}{lp{.75\textwidth}}
 Sezione III: &  uso del calcolo delle differenze per trovare la più grande e 
 la più piccola delle ordinate, dove si risolvono le \emph{questioni sui 
 massimi e minimi};\\
 Sezione IV:  &  uso del calcolo delle differenze per \emph{trovare i flessi 
 e le cuspidi};\\
 Sezione V:   &  uso del calcolo delle differenze per \emph{trovare le 
evolute}\\
 Sezione VI:  &  uso del calcolo delle differenze per trovare \emph{le 
caustiche 
di riflessione};\\
 Sezione VII: &  uso del calcolo delle differenze per trovare \emph{le 
caustiche 
di rifrazione};\\
 Sezione VIII:&  uso del calcolo delle differenze per trovare i punti delle 
linee
 curve che toccano una infinità di linee date in posizione, dritte o curve;\\
 Sezione IX: &   soluzione di alcuni problemi che dipendono dai metodi 
precedenti;\\
 Sezione X:  &   nuova maniera di servirsi del calcolo delle differenze con le 
curve geometriche dalla quale deduciamo i metodi dei signori Descartes e 
Hudde.\\
\bottomrule
\end{tabular}
\end{quoting}

\section[Le definizioni e le regole di derivazione]
{Sezione 1: Le definizioni e le regole di\\ derivazione} 
Nella prima sezione del testo, senza ulteriori preamboli, de l'H\^opital 
introduce immediatamente gli strumenti di lavoro: due definizioni e due
postulati, che chiama supposizioni.
\begin{quoting}
 Definizione I: chiamiamo quantità \emph{variabili} quelle che aumentano o 
diminuiscono con continuità e al contrario quantità \emph{costanti} quelle che 
rimangono le medesime mentre le altre cambiano. Così nella parabola le ordinate 
e le ascisse sono quantità variabili invece il parametro è una quantità 
costante. \cite[p.~1]{HO}
\end{quoting}

% \newpage
\begin{figure}[h!]
{\noindent
\begin{minipage}{.5\textwidth}
 \leftskip=8mm
 \noindent\small Definizione II: la porzione infinitamente piccola di cui una
 quantità variabile aumenta o diminuisce in maniera continua è chiamata 
 differenza. Per esempio sia $AMB$ una linea curva qualunque, che abbia per 
asse o diametro la linea $AC$ e come una delle ordinate a destra $PM$; e sia 
un'altra ordinata $pm$ infinitamente vicina alla prima. Posto questo [\dots] 
$Pp$ sarà la differenza di $AP$, $Rm$ quella di $PM$ [\dots] il piccolo spazio 
$MPpm$ è la differenza dello spazio compreso tra le linee $AP$ e $PM$ e l'arco 
$AM$. 
 \cite[p.~2]{HO}
\end{minipage}
\hfill
\begin{minipage}{.47\textwidth} %
 \centering
 \includegraphics[width=0.8\textwidth]{chap/01_interventi/01_aldegheri/fig02}
  \caption*{\leftskip=5mm\footnotesize La curva $AMB$ e il concetto di 
  \\differenza (dalla fig.~1 originale).}
 \label{f2}
\end{minipage}
}
\end{figure}

A tali prime definizioni seguono un corollario ed un avvertimento al lettore.\\

Il Corollario:
\begin{quoting}
È evidente che la differenza di una quantità costante è nulla o zero 
\end{quoting}
L'Avvertimento:
\begin{quoting} Nel seguito verrà utilizzato il simbolo $d$ per indicare la 
differenza di una quantità variabile espressa da una singola lettera [\dots] se
per esempio nominiamo le variabili $AP$, $x$; $PM$, $y$; $AM$, $z$; l'arco 
$AM$, 
$u$; lo spazio mistilineo $APM$, $s$; e il segmento $AM$, $t$: $dx$ esprimerà 
il valore di $Pp$, $dy$ quello di $Rm$, $dz$ quello di $Sm$, $du$ quello del 
piccolo arco $Mm$, $ds$ quello del piccolo spazio $MPpm$, e $dt$ quello del 
piccolo triangolo mistilineo $MAm$.\footnote{ibidem}
\end{quoting}
Traduciamo ora i postulati o, come vengono definiti, le richieste 
o supposizioni:

\begin{quoting}
I richiesta o supposizione. Stabiliamo che si possano prendere 
l'una per l'altra due quantità che differiscono tra di loro di una quantità 
infinitamente piccola o (che è la stessa cosa) che una quantità che è aumentata 
o diminuita di un'altra quantità infinitamente minore di quella, possa essere 
considerata come rimanente la stessa. Stabiliamo per esempio che possiamo 
prendere $Ap$ per $AP$, $pm$ per $PM$, lo spazio $Apm$ per lo spazio $APM$, il
piccolo spazio $MPpm$ per il piccolo rettangolo $MPpR$ [\dots ] l'angolo $pAm$
per l'angolo $PAM$, ecc.

II richiesta o supposizione. Stabiliamo che una linea curva possa essere 
considerata come l'assemblaggio di una infinità di linee rette, ciascuna 
infinitamente piccola o (che è la stessa cosa) come un poligono con un numero 
infinito di lati, ciascuno infinitamente piccolo, i quali, attraverso gli 
angoli che formano, determinano la curvatura della linea. Stabiliamo per 
esempio che la porzione di curva $Mm$ e l'arco di cerchio $MS$ possano essere 
considerati come delle linee rette a causa della loro infinita piccolezza 
[\dots]\footnote{Ivi, p.~3}
\end{quoting}

Alla luce delle definizioni e dei postulati, in pochissime pagine vengono 
ricavate le regole di derivazione principali. Partendo da problemi specifici si 
arrivano a determinare 4 regole.
Vediamo di seguito i quattro problemi, per poi soffermarci su come de 
l'H\^opital 
risolve il secondo e il terzo, rispettivamente riguardanti la differenza di un 
prodotto e la differenza di un quoziente.

\noindent Problema 1: ``Trovare la differenza di più quantità aggiunte insieme
o sottratte le une dalle altre''.\\
Problema 2: ``Trovare la differenza di un prodotto fatto da più quantità 
moltiplicate le une con le altre''.\\
Problema 3: ``Trovare la differenza di una frazione qualunque''.\\
Problema 4: ``Trovare la differenza di una potenza qualunque perfetta o 
imperfetta di una quantità variabile''.\footnote{Ivi, pp.~3-6.}

Vediamo la soluzione che il marchese dà del secondo problema riguardante la 
differenza di un prodotto: 
\begin{quoting}
 La differenza di $xy$ è $ydx+xdy$. Perché $y$ diventa $y+dy$ mentre $x$ 
 diventa $x+dx$; e pertanto $xy$ diventa allora $xy+ydx+xdy+dxdy$ che è il
 prodotto di $x+dx$ per $y+dy$, e la differenza sarà $ydx+xdy+dxdy$, vale a 
 dire $ydx+xdy$: poiché $dxdy$ è una quantità infinitamente piccola in rapporto
 agli altri termini $ydx$ e $xdy$.\footnote{Ivi, p.~4.}
\end{quoting}
Sul problema 3, la differenza di un quoziente, leggiamo: 
\begin{quoting}
La differenza di \(\dfrac{x}{y}\) è \(\dfrac{ydx-xdy}{yy}\). Perché ammesso
che \(z=\dfrac{x}{y}\) avremo \(x=yz\) e come se fossero due quantità variabili
$x$ e $yz$ restano sempre uguali tra loro, sono quelle aumentando o diminuendo,
ne consegue che la loro differenza, vale a dire il loro aumentare o diminuire
sarà uguale tra loro e pertanto avremo:\footnote{Ivi, p.~5.}
\[dx=ydz+zdy,\qquad\text{ e }\qquad dz=\frac{dx-zdy}{y}=\frac{ydx-xdy}{yy}. 
\]
\end{quoting}

\section[Sez.II: il calcolo delle differenze e le tangenti]
{Sezione II: l'uso del calcolo delle differenze per trovare le tangenti 
a tutti i tipi di linee curve}

\begin{figure}[h!]
\begin{minipage}{.5\textwidth}
Dopo essersi soffermato sulle definizioni, sui postulati e sulle regole di 
derivazione, il matematico francese descrive immediatamente una fondamentale 
applicazione del calcolo delle differenze: la determinazione della tangente ad 
una curva in un suo punto. 

La definizione da cui parte la trattazione della sezione II è quella di 
tangente:

\noindent\small 
\leftskip=8mm Se noi prolunghiamo uno dei piccoli lati di un poligono $Mm$ 
che forma una linea curva, questo piccolo lato, così prolungato sarà chiamato 
la Tangente della curva nel punto $M$ o $m$.\cite[p.~11]{HO}
\end{minipage}
\hfill
\begin{minipage}{.47\textwidth} %
 \centering
 \includegraphics[width=0.8\textwidth]{chap/01_interventi/01_aldegheri/fig03}
  \caption*{\footnotesize \leftskip=1.2cm
  \emph{La definizione di tangente} \\(fig.2 originale).}
 \label{f3}
\end{minipage}
\end{figure}

\pagebreak

Si parte direttamente da un problema:
\begin{quoting}
{\em Sia $AM$ una 
linea curva tale che la relazione tra l'ascissa $AP$ e l'ordinata $PM$, sia 
espressa da una relazione qualsiasi, dobbiamo dal punto dato $M$ su questa 
curva tracciare la tangente $MT$.}\\

\vspace{-.8em}
\begin{figure}[h!]
\begin{minipage}{.5\textwidth}
\small \leftskip=8mm Si tracci l'ordinata $MP$, e supponendo che la retta 
$MT$ che incontra il diametro nel punto $T$, sia la tangente cercata, tracciamo
un'altra ordinata $mp$ infinitamente vicina alla precedente con una piccola 
linea $MR$ parallela ad $AP$. E nominando le quantità date $AP$, $x$; $PM$, $y$
(quindi $Pp$ o $MR=dx$, e $Rm=dy$), i triangoli simili $mRM$ e $MPT$ 
determinano 
\\
$mR(dy).RM(dx)::MP(y).PT  =\dfrac{ydx}{dy}$.
\end{minipage}
\hfill
\begin{minipage}{.47\textwidth} %
 \centering
 \includegraphics[width=0.85\textwidth]{chap/01_interventi/01_aldegheri/fig04}
 \caption*{\footnotesize \emph{I triangoli simili}\\ (dalla fig.3 originale).}
 \label{f4} 
\end{minipage}
\end{figure}

\vspace{-1em}
Ora attraverso la differenza dell'equazione data, troveremo un valore di $dx$ 
in termini che saranno tutti dipendenti da $dy$, che moltiplicato per $y$ e 
diviso per $dy$, darà il valore della sottotangente $PT$ in termini 
completamente noti e liberi dalle differenze, il che servirà a tracciare la 
tangente cercata $MT$.\footnote{Ibidem}
\end{quoting}
Per essere più esplicativo il marchese propone subito alcuni esempi, vediamo il 
primo.
\begin{quoting}
Se si vuole che $ax=yy$ esprima la relazione tra $AP$ e $PM$; la 
curva $AM$ sarà una parabola che avrà per parametro la quantità data $a$ e la 
si avrà prendendo le differenze di una parte e dell'altra, $adx=2ydy$, e 
\(dx=\dfrac{2ydy}{a}\) e \(PT\tonda{\dfrac{ydy}{a}}=\dfrac{2yy}{a}=2x\) 
sostituendo $yy$ con il suo valore $ax$. Da ciò si ha che se prendiamo $PT$ 
doppio di $AP$, e tracciamola retta $MT$, essa sarà la tangente nel punto $M$.
Questo è ciò che ci si era proposti.\footnote{Ivi, p.~12.}
\end{quoting}

\section{Sezione IX: la regola di de l'H\^opital}

\begin{wrapfigure}{R}{0pt}
\includegraphics[width=0.45\textwidth]{chap/01_interventi/01_aldegheri/fig05}
   \caption*{\footnotesize La sezione IX nel testo originale}
   \label{f5}
 \end{wrapfigure}

Al termine della completa illustrazione del nuovo calcolo e dopo averne 
evidenziato numerose applicazioni, quali lo studio delle tangenti e dei flessi 
di una curva, il matematico si occupa della ``soluzione di alcuni problemi che 
dipendono dai metodi precedenti''. È in questa nona sezione, la penultima, che 
de l'H\^opital descrive il procedimento che diverrà poi la sua famosa regola.

Questa nona sezione contiene 5 proposizioni, ben 3 delle quali riguardanti 
l'epicicloide. La prima è quella che ci interessa, essa parte dal problema di 
una curva esprimibile come un rapporto di espressioni contenenti una stessa 
variabile; ci si chiede qual è il valore dell'ordinata di questa curva quando 
per un determinato valore della variabile, entrambe le espressioni diventano 
zero.

All'inizio della sezione possiamo leggere:\footnote{
Ivi, pp.~145-146. L'asterisco * contenuto nella citazione è nel testo originale
e rimanda alla nota:``Si veda la supposizione~1''}
\begin{quoting}
{\em Sia $AMD$ una linea curva ($AP=x$, $PM=y$, e $AB=a$) tale che il valore
dell'ordinata $y$ sia espresso da una frazione dove il numeratore e il 
denominatore diventano entrambi zero quando $x=a$, cioè quando il punto $P$ 
cade sul punto dato $B$. Si chiede quale dovrebbe essere quindi il valore 
dell'ordinata $BD$.}\\
Siano intese due linee curve $ANB$, $COB$ che abbiano la linea $AB$ come asse
comune, e che siano tali che l'ordinata $PN$ esprima il numeratore, e 
l'ordinata $PO$ il denominatore della frazione generale corrispondente a tutte 
le $PM$: di modo che \(PM=\frac{AB\times PN}{PO}\). 
\end{quoting}
% \begin{figure}[h!]
\vspace{-.8em}
\begin{minipage}{.52\textwidth}
\small 
\leftskip=9mm 
È chiaro che queste due curve si 
incontrano al punto $B$; poiché dalla supposizione $PN$ e $PO$ diventano 
entrambi 
zero quando il punto $P$ cade su $B$. Posto questo, se immaginiamo una ordinata 
$bd$ infinitamente vicina a $BD$ e che rincontra le linee curve $ANB$, $COB$ ai
punti $f$, $g$;  si avrà \(bd=\dfrac{AB\times bf}{bg}\) la quale* non 
differisce da $BD$. Quindi resta solo la richiesta di trovare il rapporto tra 
$bg$ e $bf$. 
È ben visibile che l'ascissa $AP$ diventa $AB$, le ordinate $PN$,
$PO$ diventano nulle, e che divenendo $AP$ $Ab$, esse diventano $bf$, $bg$. 
Da cui ne segue che quelle ordinate, le stesse $bf$, $bg$, sono le differenze 
delle ordinate in $B$ e $b$ rispetto alle curve $ANB$ e $COB$; 
quindi se 
prendiamo la differenza del numeratore e la dividiamo per la differenza del 
denominatore, dopo aver fatto $x=a=Ab$ 
o $AB$, avremo il valore cercato dell'ordi-
\end{minipage}
\hfill
\begin{minipage}{.46\textwidth} %
 \begin{center}
 \includegraphics[width=0.8\textwidth]{chap/01_interventi/01_aldegheri/fig06}
 
 {\footnotesize La curva \(AMD\) rappresenta i rapporti \\
 fra i valori 
delle curve \(ANB\) e \(COB\) \\
(fig.130 originale).}
\end{center}
%  \caption*{\footnotesize La curva $AMD$ rappresenta i rapporti fra i valori 
% delle curve $ANB$ e $COB$ (fig.130 originale).}
 \label{f6} 
\end{minipage}
% \end{figure}
% \end{quoting}

\vspace{.2em}
\leftskip=9mm 
\small 
nata $bd$ o $BD$.
Questo è ciò che era necessario trovare.

Vediamo i primi due esempi proposti dal manuale. \\
\begin{quoting}
\begin{center}\textsc{Esempio I}.\end{center}
\noindent 164.~\textsc{Sia} 
\(y=\frac{\sqrt{2a^3x-x^4}-a\sqrt[3]{aax}}{a-\sqrt[4]{ax^3}}\). 
È chiaro che quando $x=a$, il numeratore e il denominatore della frazione 
diventano entrambi uguali a zero. Ecco perché noi prenderemo la differenza 
\(y=\frac{a^3dx-2x^3dx}{\sqrt{2a^3x-x^4}}-\frac{aadx}{3\sqrt[3]{axx}}\)  
del numeratore, e la divideremo per la differenza
\(-\frac{3adx}{4\sqrt[4]{a^3x}}\)
del denominatore, dopo aver posto $x=a$, che è come dire che divideremo 
\(-\frac{4}{3}adx\) per \(-\frac{3}{4}dx\); che dà \(\frac{16}{9}a\) per il
valore desiderato di $BD$.\footnote{Ivi, p.~146.}
\end{quoting}

Verifichiamo graficamente l'Esempio I che tratta la curva: 
\[
 y=\frac{\sqrt{2a^3x-x^4}-a\sqrt[3]{a^2x}}{a-\sqrt[4]{ax^3}}:
 \]
 \begin{figure}[h]
 \centering
\includegraphics[width=0.85\textwidth]{chap/01_interventi/01_aldegheri/fig07}
 \caption*{\footnotesize Verifica grafica dell'esempio I dell' “\em{Analyse}” 
per i valori $a=1$ e $a=2$.}
 \label{f7} 
 \end{figure}

 \begin{quoting}
\begin{center}\textsc{Esempio II}.\end{center}
\noindent 165.~\textsc{Sia} $\frac{aa-ax}{a-\sqrt{ax}}$. Troviamo $y=2a$, 
quando $x=a$.\\
Potremmo risolvere questo esempio senza avere bisogno di calcolare 
le differenze, in altro modo.\\
Avendo rimosso gli incommensurabili, si avrà 
$aaxx+2aaxy-axyy-2a^3x+a^4+aayy-2a^3y=0$,
che diviso per $x-a$, si ridurrà a 
$aax-a^3+2aay-ayy=0$; e sostituendo $a$ al posto di $x$, viene come prima 
$y=2a$. 
\end{quoting}
Tale esempio può avere una soluzione algebrica, cioè si può elaborare come 
segue: \(ay-y\sqrt{ax}=aa-ax\ \rightarrow\ aa-ax-ay=-y\sqrt{ax}\) per poi, 
elevando al quadrato i due membri e portando tutto a primo membro, ottenere:
\(aaxx+2aaxy-axyy-2a^3x+a^4+aayy-2a^3y=0\), che diviso per $x-a$ restituisce:
\(aax-a^3+2aay-ayy=0\) dove, con $a=x$, si ottiene $y=2a$. 

Tuttavia la strada del calcolo delle differenze porta a risolvere 
l'esercizio in maniera molto più veloce ed immediata:
da \(y=\frac{aa-ax}{a-\sqrt{ax}}\), sostituendo il numeratore e il denominatore 
con le rispettive differenze, otteniamo: 
\(y=\frac{-adx}{\frac{1}{2}\sqrt{a}\frac{dx}{\sqrt{x}}}=2a\).

\section[L'origine della regola di de l'H\^opital]
{Ricostruzione storica della nascita della regola di de l'H\^opital}

Torniamo ora al primo esempio della sezione nona, che per primo illustra 
l'applicazione di quella che poi verrà chiamata regola di de l'H\^opital: 
\(y=\frac{\sqrt{2a^3x-x^4}-a^3\sqrt{aax}}{a-\sqrt[4]{ax^3}}\). 

Questo esempio torna molte volte nell'opera di de l'H\^opital e la sua storia 
ci 
porta alla vera origine della famosa regola. L'esempio compare per la prima 
volta in una lettera di de l'H\^opital a Johann Bernoulli del 27 giugno 1693. In
questa lettera de l'H\^opital pone al matematico svizzero il seguente problema, 
a
sua volta propostogli dal matematico Varignon: 
\begin{quoting}
 \protect[\dots] sia l'equazione che esprime la natura di una curva dove 
l'ascissa è 
 $x$ e l'ordinata $y$; ci chiediamo il valore di $y$ quando $x$ diventa uguale 
alla 
 costante $a$. Sol. $y=2a$, perché in questo caso \\
 \(\frac{\sqrt{2a^3x-x^4}-a\sqrt[3]{aax}}{a-\sqrt[4]{ax^3}}=\frac{aa-aa}{a-a}
 =a+a\).\cite{HB}
 \end{quoting}
Questa è la prima apparizione del problema della determinazione di una 
espressione algebrica che assume la forma \(\frac{0}{0}\) per certi valori di 
una variabile;
qui de l'H\^opital ne sbaglia la soluzione. È nella lettera di Bernoulli a de 
l'H\^opital
del 22 luglio 1694, che viene fornita la risposta:
\begin{quoting}
Probl. Sia data una curva la cui natura si possa esprimere come 
una frazione uguale a $y$, che in certi casi ha il numeratore e il denominatore 
pari a zero: ci chiediamo il valore, ovvero la grandezza dell'ordinata $y$.
\end{quoting}
 \begin{figure}[h]
 \centering
\includegraphics[width=0.8\textwidth]{chap/01_interventi/01_aldegheri/fig08}
 \caption*{\footnotesize Rifacimento conforme all'originale delle due figure
 inerenti al problema.}
 \label{f8} 
 \end{figure}
\begin{quoting}
Sol. Sia $AEC$ la curva data, $AD=x$, $DE=y$, $AB= a$ una costante tale che
$BC$ diventa uguale ad una frazione il cui numeratore e il cui denominatore 
sono uguali a zero; quindi per trovare la grandezza dell'ordinata $BC$, 
costruisco sullo stesso asse $adb$ due altre curve $aeb$ e $\alpha\epsilon b$
di tale natura che avendo preso ascisse uguali a $AD$, $ad$, le ordinate $de$ 
siano in rapporto al numeratore della frazione generale che esprime le ordinate
$DE$; e $d\epsilon$ siano in rapporto al denominatore della stessa frazione: 
dato questo è chiaro che $de$ diviso per $d\epsilon$ può essere 
supposto uguale a $DE$; il problema si riduce quindi nel trovare il valore di 
$de$ diviso per $d\epsilon$ nel caso in cui $ab$ è uguale ad $AB$; ora vedo che
in questo caso $de$ e $d\epsilon$ svaniscono perché i due termini della 
frazione 
svaniscono: e così le due curve $aeb$ e $\alpha\epsilon b$ si intersecano nel
punto $b$: quindi si devono prendere gli ultimi differenziali $\beta c$, 
$\beta \gamma$, di cui l'uno diviso per l'altro mi segnerà la grandezza 
cercata $BC$: questo è ciò che mi fornisce questa regola generale.
{\em Per avere il 
valore dell'ordinata di una certa curva nel detto caso, dobbiamo dividere il 
differenziale del numeratore della frazione generale per il differenziale del 
denominatore, il quoziente, dopo aver posto $x$ uguale al supposto $AB$, sarà 
la grandezza $BC$.}\\

Esempio. La curva $AEC$ ha equazione
\(\frac{\sqrt{2a^3x-x^4}-a\sqrt[3]{aax}}{a-\sqrt[4]{ax^3}}=y\).
Pertanto se $Ab$ è $=a$, si avrà \(BC=\frac{0a}{0}\), ma ne 
vogliamo il vero valore: secondo la regola io prendo il differenziale del 
numeratore [\dots]; quindi \(\frac{-4/3adx}{-3/4dx}\) o 
$\frac{16a}{9}=BC$.\cite{BH}
\end{quoting}

Notiamo che Bernoulli usa ``{\em differentielles}'', de l'H\^opital usa 
{\em ``differences''}. 

\section{Cenni alla regola di de l'H\^opital in Analisi Non Standard}
Bernoulli, nella sua lettera prima, e de l'H\^opital, nel suo manuale poi, 
ci dicono che il rapporto fra due funzioni \(f(x)\) e 
\(g(x)\), valutato per \(x=a\), dove si annullano entrambe, equivale al 
rapporto tra le loro differenze (infinitesime) $\frac{df}{dg}$, valutate 
rispetto ad \(a\). Il teorema che ai giorni nostri si presenta agli 
studenti è però diverso, perché fa uso della nozione di limite, a quel tempo
sconosciuta, e utilizza le derivate invece delle differenze. 

L'Analisi Non Standard consente di recuperare la notazione originale della 
regola e di darne una dimostrazione comunque consistente e chiara.
Per definizione non standard di funzione continua, se 
\(x\approx x+\epsilon\) allora 
\(f(x)\approx f(x+\epsilon),\ \forall \epsilon\).
Ha senso, quindi, il ragionamento di Bernoulli che invita de l'H\^opital a 
valutare il rapporto fra le due funzioni in un punto infinitamente vicino a 
quello di crisi \(a\). Infatti, se \(f(a)=g(a)=0\) per ipotesi, 
\(df(a)=f(a+\epsilon)-f(a)\approx f(a)\) e analogamente per~\(g\).

A questo punto, allo scopo di pervenire alla formulazione attuale del teorema,
poniamo \(y=\frac{f(x)}{g(x)}\), con entrambe le 
funzioni $f(x)$ e $g(x)$ nulle nella medesima ascissa $x=a$, cioè
$f(a)=0$ e $g(a)=0$. Se esistono $f'(a)$ e $g'(a)$, con $g'(a)\ne 0$,
valutiamo il rapporto fra i differenziali \(\frac{df(a)}{dg(a)}\). 
Ricordando la definizione di differenziale alla Robinson e la definizione
di derivata \(f'(x)=\st\tonda{\frac{df(x)}{dx}}\),
abbiamo, per qualsiasi \(dx\ne 0\) e per le proprietà della funzione 
parte standard \(\st()\):


\begin{equation*}
 \begin{split}
  \frac{df(a)}{dg(a)} 
  &= \frac{f(a+dx)-f(a)}{g(a+dx)-g(a)}=
     \frac{\frac{f(a+dx)-f(a)}{dx}}{\frac{g(a+dx)-g(a)}{dx}}\approx\\
  & \approx \st\tonda{\frac{\frac{df(a)}{dx}}{\frac{dg(a)}{dx}}}=
   \frac{\st\tonda{\frac{df(a)}{dx}}}
    {\st\tonda{\frac{dg(a)}{dx}}}=\frac{f'(a)}{g'(a)}.
 \end{split}
\end{equation*}

% \vspace{-1.5em}
% \framebox{
% \begin{figure}[h!]
\begin{minipage}{.45\textwidth}
Un esempio: \(y=\frac{x^2-3x+2}{(x-1)^3-1}\) assume la forma \(\frac{0}{0}\) 
per \(x=2\). Le curve di \(f(x)=x^2-3x+2\) e di \(g(x)=(x-1)^3-1\), entrambe 
nulle per \(x=2\), in $x\approx 2$ hanno un 
andamento indistinguibile dalle loro tangenti, come mostra il campo visivo di 
un microscopio a infiniti ingrandimenti. \(f'(2)\) e \(g'(2)\) ci danno le 
pendenze delle due tangenti e si ha \(\frac{f'(2)}{g'(2)}=\frac{1}{3}\).
\end{minipage}
\hfill
\begin{minipage}{.53\textwidth} %
 \centering
 \includegraphics[width=0.8\textwidth]{chap/01_interventi/01_aldegheri/fig09}
 \label{f4} 
\end{minipage}
% \end{figure}
% }

\section{Considerazioni conclusive}
Nel suo testo, ed in particolare nella premessa, de l'H\^opital è molto preciso 
nei 
ringraziamenti da fare a chi l'ha guidato nella comprensione di questo nascente 
calcolo infinitesimale e, nella prefazione, dopo aver precisato la storia del 
calcolo infinitesimale e dei suoi protagonisti: Archimede, Cartesio, Pascal, 
Fermat e Barrow, correttamente evidenzia di dovere a Leibniz, ai Bernoulli, 
Johann in particolare, e a Newton il debito maggiore riguardante 
queste nuove idee. È da sottolineare tuttavia che mentre nei 
loro lavori i Bernoulli e Leibniz cominciano a lavorare anche su curve 
trascendenti, de l'H\^opital, praticamente lavora solo sulle algebriche, 
menzionando brevemente la curva logaritmica.

De l'H\^opital è entusiasta del nuovo calcolo, ne riconosce le enormi 
potenzialità e ha una visione molto chiara degli infinitesimi. Per lui
infinitesimi esistono veramente, sono reali, non ci sono dubbi su 
questo; essi ad esempio rappresentano gli elementi di un triangolo 
differenziale. Tale posizione, che poi verrà definita da Robinson: visione 
platonistica di de l'H\^opital, non sarà accettata da Leibniz.
Nel suo testo fondamentale ``Analisi non standard'' Robinson, dopo aver 
riportato ed analizzato il testo originale di de l'H\^opital, sostiene che il 
matematico francese fosse convinto di aver trovato l'infinito attuale nel 
calcolo degli infinitesimi.

Robinson evidenzia poi la centralità dei due assiomi iniziali contenuti 
nell'opera di Leibniz, notando che il secondo ha una storia che risale 
all'antichità, mentre il primo, che tenta di dare una base giustificativa 
all'intero nuovo calcolo delle differenze, contiene già in sé la contraddizione 
critica della teoria: la differenza tra due quantità può essere, nello stesso 
tempo, sia diversa che uguale a zero.

Tornando all'epoca in cui visse il marchese, gli infinitesimi così descritti 
avranno una prima aspra critica da Michel Rolle, poco prima autore del famoso 
teorema, e saranno difesi da uno dei primi commentatori del testo: Varignon.

Infine ricordiamo come il testo di de l'H\^opital si configura come il primo 
manuale 
di analisi infinitesimale della storia, con la prima edizione nel 1696; la 
seconda edizione del 1715 ne testimonia il successo riscosso.
L'approccio da lui usato è quello degli infinitesimi di Leibniz, distante 
nei fondamenti ma non nei risultati dall'approccio di Newton, contraddistinto 
da 
fluenti e flussioni, i cui primi manuali verranno scritti da Ditton e Hayes, 
rispettivamente nel 1704 e 1706.


\newpage

\begin{thebibliography}{10}
\phantomsection
\addcontentsline{toc}{section}{Bibliografia}

\bibitem[AV]{A.A.V.V.}
AA.VV., 
\emph{Der Briefwechsel Von Johann Bernoullihe origins of the Infinitesimal 
Calculus}, 
vol.1,Springer, Basilea, 1955

\bibitem[BH]{BH}
Lettera di Johann Bernoulli a de l'H\^opital del 22 luglio 1694, in 
traduzione.\\
Cfr. A.A. V.V. {\em Der Briefwechsel Von Johann Bernoulli}, Springer, Basilea 
1955, volume 1,
pp 235-236

\bibitem[BPS]{BPS}
R. E. Bradley, S. J. Petrilli, C. E. Sandifer,
\emph{L'H\^opital's Analyse des infiniments petits. An Annotated 
Translation with Source Material by Johann Bernoulli},
Springer International Publishing Switzerland, 2015

\bibitem[HB]{HB}
Lettera di de l'H\^opital a Johann Bernoulli, del 27 giugno 1693, in 
traduzione. 
Cfr.AA.VV. {\em Der Briefwechsel Von Johann Bernoulli}, Springer, Basilea 1955,
volume 1, p.~173

\bibitem[HO]{HO}
G. F. A. de Sainte Mesme, marchese de l'H\^opital, 
\emph{Analyse des Infiniment Petits pour l'Intelligence des Lignes Courbes},
Imprimerie Royale, Parigi, 1696.\\
Il manuale ha avuto una seconda edizione nel 1715, in cui il nome dell'autore 
compare esplicitamente


\bibitem[Gi]{Gi}
C. C. Gillispie,
\emph{Dictionary of Scientific Biography}
Scribner's, New York, 1974

\bibitem[Go]{Go}
G. Goldoni, 
\emph{Il professor Apotema insegna… Il calcolo delle differenze e il 
calcolo differenziale},
Modena, 2014

\bibitem[Ke]{Ke}
H. J. Keisler,
\emph{Elementi di analisi matematica},
Piccin Editore, Padova, 1982


\bibitem[Ka]{Ka}
V. Kats,
\emph{A History of Mathematics: an introduction},
 HarperCollins, New York, 1993

\bibitem[Ro]{Ro}
A. Robinson,
\emph{Analisi Non Standard},
Aracne, Roma 2013

\bibitem[St]{St}
D.J. Struik, 
\emph{The origin of l'H\^opital's rule, in A source book in Mathematics},
Princeton University Press, Princeton, 1986

\end{thebibliography}


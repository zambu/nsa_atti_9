
\chapter[Dalle numerosità ai numeri euclidei]
{{\LARGE Un possibile percorso didattico: }\\
{\LARGE dalle numerosità ai numeri euclidei}}
\begin{center}
Vieri Benci - Università di Pisa.
\end{center}
\vspace{1cm}
{\em La rilevanza dell'uso dei numeri infinitesimi nasce dal calcolo
infinitesimale. Dal punto di vista didattico questo significa che i numeri
infiniti ed infinitesimi dovrebbero essere introdotti durante l'ultimo(i)
anno delle scuole superiori. Forse però sarebbe auspicabile, introdurre
i numeri infinitesimi prima, senza dover necessariamente seguire lo sviluppo
culturale che abbiamo avuto noi docenti. Infatti ci sono molti aspetti dei
numeri infiniti ed infinitesimi estremamente interessanti che possono essere
sviluppati anche prima del calcolo infinitesimale. I motivi che possono
rendere questa impostazione auspicabile saranno discussi in seguito.
} 

\bigskip 
\section{Le numerosità}

\subsection{Il numero alfa}

Possiamo definire il numero $\alpha $ già al primo anno delle superiori:

\begin{definition}
\label{a}$\alpha $ è la numerosità dell'insieme dei numeri naturali 
\[
\mathbb{N=}\left\{ 1,2,3,4,.....\right\} 
\]
\end{definition}

\textbf{Spiegazione}{. }Ad ogni insieme finito si può associare un
numero naturale: il numero dei suoi elementi ovvero la sua \textbf{numerosit%
à}. Dunque possiamo generalizzare questa idea ed associare una numerosit%
à anche agli insiemi infiniti. Per esempio, possiamo associare
all'insieme $\mathbb{N}$ dei numeri naturali un numero che battezziamo $%
\alpha .$ Abbiamo bisogno di un nuovo nome e di un nuovo simbolo poiché
nessun numero naturale è così grande da poter rappresentare la 
\textit{numerosità }di $\mathbb{N}$.

Se esistono numeri infiniti, come possiamo rappresentare tutti questi
numeri? Una importante classe di numeri può essere rappresentata da
espressioni contenenti il simbolo $\alpha ;$ ad esempio%
\[
\alpha +1,\ \ \alpha ^{2},\ \ 2\alpha ,\ \ 3\alpha ^{2}+2\alpha 
\]%
sono tutti numeri infiniti. E come possiamo operare con questi numeri?
Semplice: con questi numeri valgono le stesse regole algebriche che abbiamo
imparato studiando il calcolo letterale.

Ad esempio%
\[
\left( \alpha +1\right) -6=\alpha -5 
\]%
\[
\left( \alpha +1\right) ^{2}=\alpha ^{2}+2\alpha +1 
\]%
\[
\alpha -\left( \alpha +1\right) ^{2}=-\alpha ^{2}-\alpha -1 
\]

\begin{remark}
Una espressione del tipo $\alpha ^{2}+\alpha $ non può essere
"semplificata", proprio come non si può semplificare la scrittura del
numero $\pi ^{2}+\pi $.
\end{remark}

\subsection{Definizione assiomatica della teoria delle numerosità}

Alcuni numeri infiniti, al pari dei numeri naturali, rappresentano la
numerosità di alcuni insiemi. Un minimo di conoscenza della \textit{%
teoria degli insiemi} permette di dare una buona assiomatica della teoria
della numerosità e di interpretare il loro significato.

\begin{axiom}
La numerosità è una funzione $\mathfrak{num}$ che ad ogni insieme $E$
fa corrispondere un numero $\mathfrak{num}\left( E\right) .$
\end{axiom}

\begin{axiom}
Se $E$ è un insieme finito, allora $\mathfrak{num}\left( E\right) $ è
il numero naturale che corrisponde al numero dei suoi elementi.
\end{axiom}

\begin{axiom}
Se $E\subset F,$ allora $\mathfrak{num}\left( E\right) <\mathfrak{num}\left(
F\right) .$
\end{axiom}

\begin{axiom}
Se $E$ ed $F$ sono insiemi disgiunti, allora 
\[
\mathfrak{num}\left( E\cup F\right) =\mathfrak{num}\left( E\right) +%
\mathfrak{num}\left( F\right) 
\]
\end{axiom}

\begin{axiom}
Se $E$ ed $F$ sono insiemi qualunque, allora 
\[
\mathfrak{num}\left( E\times F\right) =\mathfrak{num}\left( E\right) \cdot 
\mathfrak{num}\left( F\right) 
\]
\end{axiom}

A questo punto la definizione \ref{a} assume un senso preciso.

\bigskip 

\textbf{Esempi:}

\[
3=\mathfrak{num}\left( \left\{ 1,2,3\right\} \right) =\mathfrak{num}\left(
\left\{ 1,3,4\right\} \right) =\mathfrak{num}\left( \left\{ 6,33,47\right\}
\right) 
\]%
\[
\alpha +1=\mathfrak{num}\left( \mathbb{N}_{0}\right) =\mathfrak{num}\left( 
\mathbb{N}\cup \left\{ 0\right\} \right) 
\]%
\[
\alpha ^{2}=\mathfrak{num}\left( \mathbb{N\times N}\right) 
\]%
\[
2\alpha =\mathfrak{num}\left( \left\{ 1,-1,2,-2,3,-3,...\right\} \right) 
\]%
\[
2\alpha +1=\mathfrak{num}\left( \mathbb{Z}\right) =\mathfrak{num}\left(
\left\{ ...,-2,-1,0,1,2,...\right\} \right) 
\]

\begin{remark}
A rigore il fatto che 
\[
\mathfrak{num}\left( \mathbb{N}\right) =\mathfrak{num}\left( \mathbb{N}%
^{-}\right) ,\mathbb{N}^{-}=\left\{ -1,-2,-3,...\right\} 
\]
è un assioma del tutto naturale, ma indipendente dagli altri.
\end{remark}

\subsection{Assioma speciale}

Per fare alcuni esercizi "simpatici" conviene aggiungere un altro assioma
del tutto naturale.

\bigskip

Se considero la tabella%
\[
\begin{tabular}{lllllll}
1 & 3 & 5 & 7 & 9 & 11 & .... \\ 
2 & 4 & 6 & 8 & 10 & 12 & ....
\end{tabular}%
\]%
è intuitivo pensare che le due righe contengano lo stesso numero di
elementi.

Similmente se considero le tabelle

\[
\begin{tabular}{lllllll}
1 & 4 & 7 & 10 & 13 & 16 & .... \\ 
2 & 5 & 8 & 11 & 14 & .... & .... \\ 
3 & 6 & 9 & 12 & 15 & .... & ....%
\end{tabular}%
\]%
\[
\begin{tabular}{lllllll}
1 & 5 & 9 & 13 & 17 & .... & .... \\ 
2 & 6 & 10 & 14 & .... & .... & .... \\ 
3 & 7 & 11 & 15 & .... & .... & .... \\ 
4 & 8 & 12 & 16 & .... & .... & ....%
\end{tabular}%
\]%
\noindent è ragionevole pensare che ogni riga contenga lo stesso numero
di elementi.

In generale, dato $n\in \mathbb{N}$ e $k=0,....,n-1,$ poniamo%
\[
\mathbb{N}_{n,k}=\left\{ nm+k\ |\ m\in \mathbb{N}\right\} 
\]

\begin{axiom}
Si ha che%
\[
\mathfrak{num}\left( \mathbb{N}_{n,k}\right) =\frac{\alpha }{k}
\]
\end{axiom}

Da questo assioma, per esempio si può dedurre che

\[
\frac{\alpha }{2}=\mathfrak{num}\left( \left\{ \text{numeri pari}\right\}
\right) =\mathfrak{num}\left( \left\{ \text{numeri dispari}\right\} \right) 
\]%
Però va fatto notare che la corrispondenza biunivoca, in generale, non
implica la equinumerosità. Volendo, in una buona classe, si potrebbe
introdurre anche il concetto di cardinalità e mettere in evidenza la
differenze tra la cardinalità e la numerosità che sono due diverse
maniere di "contare" gli elementi di un insieme infinito.\footnote{%
Ulteriori informazioni sulla teoria della numerosità si possono trovare
in \cite{benci95,Bmathesis,BF2019,BDN2003}}

\subsection{La lotteria di De Finetti}

Uno degli aspetti importanti della matematica è quello di non essere
solo \textbf{bella}, ma anche \textbf{utile}. Ma i numeri infiniti a cosa
servono?

Assumiamo che lo studente conosca i rudimenti del calcolo delle probabilit%
à: basta che conosca i casi in cui lo spazio degli eventi sia finito ed
ogni evento elementare abbia la stessa probabilità (\textit{probabilit%
à equa}). Tecnicamente basta conoscere un po' di calcolo combinatorio.
Allora possiamo introdurre la lotteria di De Finetti che è una lotteria
avente infiniti biglietti numerati con i numeri naturali.

In questo caso abbiamo 
\[
\Omega \cong \mathbb{N=}\left\{ 1,2,3,...\right\} 
\]%
Dunque 
\[
P(A)=\frac{\mathfrak{num}(A)}{\mathfrak{num}(\mathbb{N})}=\frac{\mathfrak{num%
}(A)}{\alpha } 
\]%
Per esempio se $A=\left\{ 1,2,3\right\} $%
\[
P(A)=\frac{\mathfrak{num}(A)}{\mathfrak{num}(\mathbb{N})}=\frac{3}{\alpha }%
\sim 0. 
\]%
\bigskip

\begin{problem}
\textit{Qual'è la probabilità che alla lotteria di De Finetti esca
un numero pari?}
\end{problem}

La probabilità $P(\mathbb{P})$ sarà%
\[
P(\mathbb{P})=\frac{\mathfrak{num}(\mathbb{P})}{\alpha }=\frac{\frac{\alpha 
}{2}}{\alpha }=\frac{1}{2}. 
\]

\begin{remark}
Questo è proprio l'esempio che De Finetti usava per criticare la
probabilità Kolmogoroviana incapace di modellizzare questo semplice
problema.
\end{remark}

\begin{problem}
\textit{Qual'è la probabilità che escano due numeri pari?}
\end{problem}

La probabilità che il primo numero sia pari è data da $\frac{1}{2}.$
La probabilità che anche il secondo numero sia pari è minore in
quanto rimangono solo $\alpha -1$ numeri pari. Dunque la probabilità che
anche il secondo numero sia pari è data da 
\[
\frac{\frac{\alpha }{2}-1}{\alpha }=\frac{\alpha -2}{2\alpha }
\]%
Allora la probabiltà che escano due numeri pari è data da 
\[
\frac{1}{2}\cdot \frac{\alpha -2}{2\alpha }=\frac{\alpha -2}{4\alpha }=\frac{%
1}{4}-\frac{1}{2\alpha }
\]%
Ovvero questa probabilità è minore di $\frac{1}{4}$ per una quantit%
à infinitesima.\footnote{%
Ulteriori dettagli sulla Probabilità Non Archimedea si possono trovare
in \cite{Enriques1} e \cite{benciProb}. Per una approfondita analisi
filosofica riguardo a questo argomento si rimanda a \cite{BHW16}. }

\begin{remark}
\label{w}A questo punto si possono introdurre i numeri della forma%
\begin{equation}
x=\frac{P\left( \alpha \right) }{Q\left( \alpha \right) };\ Q\left( \alpha
\right) \neq 0  \label{volo}
\end{equation}%
ove $P$ e $Q$ sono polinomi a coefficienti interi. Si osservi che con questi
numeri si possono eseguire le quattro operazioni. Tutto ciò rappresenta
anche una "applicazione pratica" che motiva lo studio delle proprietà
dei polinomi e pertanto tutto ciò potrebbe essere fatto anche il primo
anno delle superiori.
\end{remark}

\subsection{Discussione sul significato "fisico" della lotteria di De Finetti%
}

Probabilmente uno studente saputello osserverebbe che i numeri infiniti non
servono nella descrizione del modo reale in quanto lotterie con infiniti
biglietti non esistono. Dopo avere mostrato qualche modello concreto in cui
lo spazio degli eventi è infinito, questa potrebbe essere una buona
occasione per spiegare la natura della Scienza occidentale nata dalle idee
di Pitagora, Platone e Galileo. Gli oggetti della scienza sono cose che non
esistono nel mondo reale; nozioni come

\begin{center}
\textit{punto materiale, forza gravitazionale, energia, metabolismo, specie
etc.}
\end{center}

\noindent non sono oggetti che si vedono, ma cose astratte che vivono in un
mondo delle idee dominato dalla razionalità. Sfruttando queste nozioni
si riescono a costruire \textit{modelli} della realtà che ci permettono
di dominare il mondo della natura. Anche i numeri appartengono a questo tipo
di cose, sia che essi siano finiti o infiniti. Ma la cosa più
stupefacente è che, almeno in parte, questi concetti astratti ci
permettono di penetrare alcuni misteri dell'universo. Questo concetto è
espresso in modo esemplare da un celebre aforisma di Einstein:

\begin{center}
\textit{The most incomprehensible thing about the universe is that it is
comprehensible.}
\end{center}

\section{Il continuo}

L'idea di continuo è descritta o modellizzata dalla retta euclidea che
denoteremo con $\mathbb{E}$. Il continuo non è importante solo per la
geometria, ma per tutte le scienze "quantitative". Infatti le grandezze
possono essere "rappresentate" da punti sulla retta euclidea (una volta
scelta l'origine $O$ ed un punto $U$).

Pertanto possiamo enunciare il seguente enunciato "metamatematico":

\begin{center}
\textit{alla misura di ogni grandezza possibile possiamo far corrispondere
un punto della retta Euclidea}
\end{center}

Questo concetto può essere espresso in modo intuitivo dal seguente
aforisma:%
\begin{equation}
\text{\textit{il continuo non ha buchi}}  \label{C}
\end{equation}

\subsection{La retta empirica}

La misura di ogni grandezza fisica dà sempre un numero decimale finito.
D'altra parte, dato un righello, ad ogni decimale finito, possiamo far
corrispondere un punto della retta euclidea. Pertanto, se denotiamo con $%
\mathbb{D}$ l'insieme dei decimali finiti possiamo chiederci se possiamo
identificare la retta euclidea con la \textbf{retta empirica} $\mathbb{D}$:

\[
\mathbb{E}=\mathbb{D}
\]%
È legittimo supporre che 
\[
\text{retta euclidea }=\ \text{retta empirica\ \ ?}
\]%
Ovviamente no! Basta dividere un segmento unitario in tre parti uguali ed
abbiamo che 
\[
1:3=0,3333333333.....
\]

Ma un segmento di lunghezza $\frac{1}{3},$ \textit{che può essere
costruito con riga e compasso}, determina un punto sulla retta euclidea, ma
non sulla retta empirica.

\begin{remark}
Ovviamente anche questa è una verità del Mondo delle Idee!!!! Un
segmento lungo $\frac{1}{3}$ è un'entità astratta che, al pari della
lotteria di De Finetti, non esiste nel mondo reale e non avrebbe neppure
senso parlarne.
\end{remark}

\subsection{La retta razionale}

Allora possiamo chiederci se la retta razionale $\mathbb{Q}$ è adeguata
a rappresentare il continuo euclideo. Tra l'altro la retta razionale ha una
bellissima proprietà:

\begin{center}
\textit{con i numeri razionali risulta possibile eseguire le quattro
operazioni }

\textit{sia mediante algorimi finiti, sia usando riga e compasso.}
\end{center}

\noindent ovvero i numeri razionali formano un \textbf{campo ordinato}.

È legittimo supporre che%
\[
\text{retta euclidea }=\ \text{retta razionale?} 
\]

Ovviamente no! Ed i motivi sono (almeno) due:

\begin{itemize}
\item La retta razionale non permette di descrivere le grandezze che sono
incommensurabili rispetto all'unita di misura: per esempio $\sqrt{2}.$

\item La retta razionale non permette di descrivere le grandezze non
archimedee: per esempio 
\[
\frac{1}{\alpha }=\text{probabilità che esca un numero dato nella
lotteria di De Finetti} 
\]%
\[
\alpha =\text{numerosità di }\mathbb{N} 
\]%
(ovviamente, le numerosità possono essere considerate particolari
grandezze).
\end{itemize}

Per sottolineare l'inadeguatezza dei numeri razionali nel descrivere tutte le
grandezze è utile ricordare che $\sqrt{2}$, essendo la diagonale di un
quadrato di lato unitario, può essere costruita con riga e compasso.

Un infinitesimo non si può "costruire direttamente" con riga e
compasso,\ ma si possono costruire grandezze non archimedee, per esempio gli
angoli di contingenza.

\begin{center} 
 \disegno{
    \pianocartesiano{0}{9}{0}{6}{gray!50, very thin, step=1}
    \tkzInit[xmin=0,xmax=+9.3,ymin=0,ymax=+6.3]
    \tkzFct[domain=0:9.3, thick, color=Maroon!50!black]{.05*x**2}
    \tkzFct[domain=0:9.3, thick, color=Blue!50!black]{.25*x}
    }
\end{center}

A questo punto è arrivato il momento di introdurre i \textbf{numeri reali%
} ed i \textbf{numeri euclidei} (che costituiscono un particolare campo
iperreale).

L'approccio classico è il seguente:

\begin{itemize}
\item si costruiscono i reali mediante le sezioni di Dedekind;

\item si costruiscono i numeri iperreali mediante un ultrafiltro.
\end{itemize}

Adesso, esporrò un approccio diverso più vicino allo sviluppo
storico. Ricordiamo che gli infinitesimi sono stati usati sistematicamente
fin dal XVII secolo (Leibnitz) mentre i numeri reali hanno fatto la loro
comparsa verso la fine del XIX secolo (Cantor e Dedekind). Da qui si può
dedurre che il concetto di numero reale è più sofisticato e sottile
di quello di numero infinitesimo. Pertanto mi sembra che, anche
didatticamente, sia più naturale introdurre prima i numeri infinitesimi
ed utilizzare questi ultimi per introdurre i numeri reali. L'approccio che
presenterò adesso, si basa sull'intuizione geometrica ed in particolare
sulla geometria Euclidea.

\subsection{La retta euclidea}

Descriveremo la retta euclidea in modo rigoroso mediante alcuni assiomi. Il
primo assioma discende dal'osservazione \ref{w}:

\begin{axiom}
\label{n}(\textbf{Assioma delle numerosità}) I numeri euclidei formano
un campo ordinato contenente le numerosità.
\end{axiom}

In particolare, la retta euclidea contiene ogni numero razionale; pertanto $%
3,\;7,\;5/4,\;$sono particolari numeri euclidei. A partire da $\alpha $ e
dai numeri razionali, usando le operazioni di campo, si possono definire
nuovi numeri come ad esempio l'inverso di $\alpha $ 
\[
\eta :=\frac{1}{\alpha }
\]%
ed i numeri ricavabili da questi 

\[
\alpha +1;\quad 7\alpha ;\quad 3\eta ;\quad \frac{\alpha }{\alpha ^{2}+2};\ 
\frac{4\pi \alpha ^{3}-\alpha +2\eta }{\alpha ^{2}+2\eta };\quad etc.
\]

Adesso vediamo come possiamo classificare i numeri euclidei.

\begin{definition}
\label{t231}Un numero euclideo $x$ si dice \textbf{infinito} se comunque si
scelga un numero naturale $k$ 
\[
\left\vert x\right\vert >k.
\]%
Un numero euclideo $x\in \mathbb{E}$ si dice \textbf{finito} se non è
infinito, ovvero se esiste un numero naturale $k$ tale che 
\[
\left\vert x\right\vert <k.
\]%
Un numero euclideo $x\in \mathbb{E}$ si dice \textbf{infinitesimo} se per
ogni numero naturale $k$ 
\[
\left\vert x\right\vert <\frac{1}{k}.
\]
\end{definition}

Per esempio è facile verificare che i numeri 
\[
\alpha +1;\quad 7\alpha ;\quad \alpha ^{2};\quad \frac{\alpha ^{3}}{2};\quad 
\frac{\alpha +2}{5};
\]%
sono infiniti. Similmente possiamo verificare che i numeri 
\[
5\eta ;\quad \frac{7}{\alpha -8};\quad 2\eta ^{2}+\eta ^{3};\quad etc.
\]%
sono infinitesimi. Si osservi che l'unico infinitesimo reale è 0. Due
numeri, $x$ ed $y,$ si dicono infinitamente vicini se la loro differenza 
è un infinitesimo; in tal caso scriveremo%
\[
x\sim y.
\]

L'assioma \ref{n} però non è sufficiente a caratterizzare la retta euclidea in 
quanto da questo non si può ricavare l'esistenza di $\sqrt{2},$ ovvero di un 
numero positivo tale che%
\[
x^{2}=2.
\]%
Dunque, per continuare la nostra indagine sul continuo, ricorreremo ancora
all'intuizione geometrica.

\subsection{I segmenti euclidei come insiemi di atomi}

Nella geometria euclidea classica, le \emph{linee} ed i \emph{segmenti} non
sono considerati \emph{insiemi di punti}; al contrario, negli ultimi due
secoli l'attitudine riduzionistica della matematica moderna ha descritto la
geometria euclidea mediante una \textit{interpretazione insiemistica}.

Se vogliamo considerare un segmento come un insieme di punti possiamo
figurarcelo come un insieme di atomi "$\circledcirc $" (di spessore nullo)
intramezzati da spazi vuoti "$~|~$".

\begin{equation}
|\circledcirc \circledcirc \circledcirc \circledcirc |\circledcirc
\circledcirc \circledcirc \circledcirc |\circledcirc \circledcirc
\circledcirc \circledcirc |\circledcirc \circledcirc \circledcirc
\circledcirc |\circledcirc \circledcirc \circledcirc \circledcirc
|\circledcirc \circledcirc \circledcirc \circledcirc |  \label{d}
\end{equation}

Infatti, se un segmento $S$ non contenesse spazi vuoti, non potrebbe essere
diviso in due segmenti congruenti $S_{1}$ ed $S_{2}$ in quanto il punto di
mezzo $M$ dovrebbe appartenere ad uno solo dei due segmenti e dunque questi
segmenti non possono essere congruenti. Ma un teorema di Euclide ci dice che
un segmento può essere diviso in due segmenti congruenti.

\bigskip

\textbf{Conclusione}: I segmenti euclidei contengono spazi vuoti.

\bigskip 

Naturalmente il disegno (\ref{d}) va preso \textit{cum grano salis}. Infatti
poiché i nostri atomi ideali hanno spessore nullo, tra due spazi vuoti
esistono infiniti atomi ed infiniti spazi vuoti. 

\begin{remark}
Si osservi che in genere si modellizza un segmento euclideo con un intervallo
di numeri reali. Questa identificazione non è corretta in quanto un
intervallo chiuso (o aperto) di numeri reali non può essere diviso in
due intervalli congruenti.\footnote{%
Vedi \cite{BFreg} o \cite{BF2019} per una discussione su questo argomento.} 
\end{remark}

\subsection{I numeri reali}

Al solito, identifichiamo alcuni punti della retta euclidea con l'estremo di
un segmento euclideo avente l'altro estremo nel punto $O=0.$ A questo punto
possiamo dare la seguente definizione metamatematica:

\begin{definition}
\label{b}\textbf{(definizione euristica)} I punti che possono essere estremi
di \textit{segmenti euclidei} sono chiamati \textbf{numeri reali}.
\end{definition}

Con questa definizione di numero reale, ne consegue che i numeri razionali
sono particolari numeri reali, ma non sono i soli, in quanto anche $\sqrt{2}$
è un numero reale. I numeri/punti che non corrispondono all'estremo di
un qualche segmento euclideo, sono chiamati numeri\textbf{\ non standard} o 
\textbf{ideali}. Per esempio un infinitesimo è un numero ideale, in
quanto non esistono segmenti euclidei di lunghezza infinitesima.

A questo punto si deve distinguere tra \textbf{lunghezze} e \textbf{distanze}%
. La distanza tra due punti che sono gli estremi di un segmento è data
da un numero reale ed è uguale alla lunghezza del segmento. Però non
esiste alcun segmento che può misurare la distanza di un spazio vuoto da
un atomo. Dunque ci sono più distanze che lunghezze. Le lunghezze
vengono descritte dai numeri reali; le distanze dai numeri euclidei. 

Naturalmente i numeri costruibili con riga e compasso non sono i soli numeri
reali; per esempio $\pi $ è un numero reale che non è costruibile
con riga e compasso. Dunque abbiamo bisogno di maggiori informazioni. Si pu%
ò provvedere a ciò enunciando la seguente proprietà della retta
euclidea che è del tutto naturale se pensiamo al nostro modello:

\begin{center}
\textit{infinitamente vicino ad ogni atomo (numero euclideo) esiste uno
spazio vuoto (numero reale).}
\end{center}

Pertanto possiamo identificare i numeri reali con gli spazi vuoti che hanno
distanza finita dall'origine. 

\subsection{Gli assiomi dei numeri euclidei}

Adesso possiamo riassumere i nostri discorsi più o meno euristici
mediante un assioma che può essere enunciato con precisione:

\begin{axiom}
\label{ps}(\textbf{Assioma della parte standard}) Esiste una funzione%
\[
st:\mathbb{E}_{fin}\rightarrow \mathbb{E}
\]%
definita sui numeri euclidei finiti $\mathbb{E}_{fin}$ che soddisfa le
seguenti proprietà:

\begin{itemize}
\item $st(x) \sim x$

\item $st(st(x))=x$

\item $st(x+y)=st(x)+st(y)$

\item $st(x)\cdot (y)=st(x) \cdot st(y)$
\end{itemize}
\end{axiom}

L'assioma della parte standard ci permette di definire con precisione
l'insieme dei numeri reali:

\begin{definition}
\textbf{(definizione formale di numero reale) }Un numero euclideo $x$ si
dice reale se e solo se è finito e%
\[
x=st(x)
\]
\end{definition}

Dunque nel nostro modello descritto nel paragrafo precedente, la parte
standard di un punto/numero definisce l'unico spazio vuoto infinitamente
vicino.

Gli assiomi \ref{n} e \ref{ps} non sono sufficienti né a caratterizzare
i numeri euclidei né a soddisfare in modo rigoroso le esigenze del
calcolo differenziale ed integrale. Abbiamo bisogno anche del seguente
assioma.

\begin{axiom}
\label{c}(\textbf{Assioma delle funzioni standard}) Data una funzione $%
\varphi $ reale, esiste un unica funzione $\varphi ^{\ast }$ definita sui
numeri euclidei che soddisfa le seguenti proprietà:

\begin{itemize}
\item per ogni numero reale $x$ si ha che $\varphi ^{\ast }(x)=\varphi (x).$

\item $x^{\ast }=x$ ($Id_{\mathbb{R}^{N}}^{\ast }=Id_{\mathbb{E}^{N}}$).

\item se $\varphi $ e $\psi $ sono funzioni reali, allora%
\begin{eqnarray*}
\left( \varphi +\psi \right) ^{\ast } &=&\varphi ^{\ast }+\psi ^{\ast } \\
\left( \varphi \cdot \psi \right) ^{\ast } &=&\varphi ^{\ast }\cdot \psi
^{\ast } \\
\left( \varphi \circ \psi \right) ^{\ast } &=&\varphi ^{\ast }\circ \psi
^{\ast }
\end{eqnarray*}%
\bigskip 
\end{itemize}
\end{axiom}

\begin{definition}
La funzione $\varphi ^{\ast }$ si dice estensione naturale della $\varphi $.
Una funzione euclidea $f$ si dice \textbf{standard, }se esiste una funzione
reale $\varphi $ tale che%
\[
f=\varphi ^{\ast }.
\]
\end{definition}

\begin{remark}
L'assioma \ref{c} implica che il campo dei numeri euclidei è un campo
iperreale. Ma l'assioma \ref{n} delle numerosità aggiunge una struttura
in più rispetto ad un campo iperreale qualunque.
\end{remark}

In un percorso didattico, probabilmente, l'assioma \ref{c} si rende
necessario per la prima volta quando si vuole introdurre la \textit{funzione
derivata} come si vede dalla seguente definizione:

\begin{definition}
La derivata in un punto $x$ della funzione $f$ è data da%
\[
Df(x)=st\left( \frac{f(x+\varepsilon )-f(x)}{\varepsilon }\right) 
\]%
qualora questo valore non dipenda da $\varepsilon .$ La funzione derivata 
è l'unica funzione standard $f^{\prime }$ tale che per ogni $x$ reale%
\[
f^{\prime }(x)=Df(x).
\]
\end{definition}

In altre parole, la derivata in un punto reale si può definire per ogni
funzione, ma per definire la funzione derivata, abbiamo bisogno dell'assioma %
\ref{c} per estendere ai numeri ideali la derivata definita sui punti reali.%
\footnote{%
Si rimanda ad \cite{bencilibro} per un testo completo di analisi basato sui
numeri euclidei. Vedi anche \cite{Bmathesis}.}

\subsection{Il continuo numerico assoluto}

Gli assiomi assiomi \ref{n}, \ref{ps} e \ref{c} sono sufficienti a
descrivere un campo iperreale, ma non a caratterizzarlo univocamente. Dunque
la descrizione della retta euclidea è ancora incompleta. Pertanto
aggiungiamo questo paragrafo per completezza, anche se, da un punto di vista
didattico, non ha rilevanza pratica e, forse, non vale la pena di considerarlo.

Nella nostra intuizione, si pensa ad un continuo come ad un insieme ordinato
senza interruzioni, ovvero senza buchi. L'usuale nozione di continuità,
dovuta a Dedekind ed a Cantor, viene definita nel modo seguente:

\begin{definition}
Si dice che un insieme linearmente ordinato $X$ è un \textbf{continuo
alla Dedekind} se per ogni coppia $\left( A,B\right) $ di sottoinsiemi di $X$
tali che%
\[
\forall a\in A,\ \forall b\in B,\ a<b,
\]%
esiste $c\in X$ tale che%
\[
\forall a\in A,\ \forall b\in B,\ a\leq c\leq b.
\]
\end{definition}

È ben noto che il campo dei numeri reali forma un continuo alla Dedeking,
ma non è coerente con (\ref{C}) perch\'{e}, per esempio, tra lo zero ed
i reali positivi esiste un buco ove vivono gli infinitesimi. Pertanto se
vogliamo avere una buona nozione di continuo che descriva i numeri euclidei
dobbiamo enunciare un altro assioma. Per fare ciò, abbiamo bisogno della
nozione di insieme \textbf{ordinario.} Praticamente un insieme è
ordinario se si può \textquotedblleft costruire\textquotedblright\
mediante le operazioni tra insiemi a partire da $\mathbb{N}$. La definizione
formale è la seguente:

\begin{definition}
\label{io}Un insieme $E$ si dice \textbf{ordinario} se soddisfa una delle
seguenti richieste:

\begin{itemize}
\item $E=\mathbb{N}$;

\item $E=\mathcal{P}(F)$ ove $F$ è un insieme ordinario;

\item  $E=\displaystyle\bigcup\limits_{i\in I}F_{i}$ 
ove $I$ e gli $F_{i},\ \forall i\in
I,$ sono insiemi ordinari; ovvero $E$ è l'unione di insiemi ordinari i
cui indici appartengono ad un insieme ordinario;

\item $E=f(F)$ ove $F$ è un insieme ordinario ed $f$ è una qualsiasi
funzione.
\end{itemize}
\end{definition}

Dunque, gli insiemi che non sono ordinari sono insiemi molto grandi detti
insiemi inaccessibili. La loro esistenza viene stabilita da un assioma della
teoria degli insiemi detto assioma di inaccessibilità. 

\begin{definition}
Si dice che un insieme linearmente ordinato $X$ è un \textbf{continuo
assoluto} se soddisfa le seguenti richieste:

\begin{itemize}
\item per ogni coppia $\left( A,B\right) $ di sottoinsiemi ordinari di $X$
tali che%
\[
\forall a\in A,\ \forall b\in B,\ a<b,
\]%
esiste $c\in X$ tale che%
\[
\forall a\in A,\ \forall b\in B,\ a<c<b.
\]
\end{itemize}
\end{definition}

L'assioma di continuità assoluta descrive assai bene l'idea intuitiva di
continuità; infatti questo assioma ci dice che tra due insiemi ordinari
di numeri $A$ e $B,$ non importa quanto siano grandi ci sta sempre qualcosa.

Adesso possiamo enuciare il nostro ultimo assioma che completa la
descrizione della retta euclidea:

\begin{axiom}
\textbf{(Assioma di continuità assoluta) }I numeri euclidei sono il pi%
ù piccolo campo iperreale che forma un continuo assoluto.
\end{axiom}

Se nella teoria degli insiemi accettiamo l'assioma di inaccessibilità
(vedi per esempio \cite{BF2019} p. 178), si può dimostrare che il campo
dei numeri euclidei esiste ed è unico (vedi \cite{keisler76}). Poiché
i numeri cardinali ed i numeri ordinali (corrispondenti ad insiemi ordinari)
possono essere identificati con particolari numerosità, la retta
euclidea rappresenta bene tutti i numeri.\footnote{%
Ovviamente si intende gli insiemi di numeri \textit{ordinati}. La retta
euclidea non contiene i numeri complessi, ma è immediato capire che
quest'ultimi possono trovare posto nel "piano euclideo"%
\[
\mathbb{E}+i\mathbb{E}.
\]%
}

\section{Conclusioni}

Tra le motivazioni che suggeriscono l'introduzione dei numeri iperreali
nelle scuole superiori possiamo elencare le seguenti:

\begin{enumerate}
\item L'argomento è affascinante e potrebbe stimolare l'interesse verso
la matematica;

\item esistono alcune applicazioni della matematica non archimedea al di
fuori del calcolo infinitesimale che possono essere fatte e capite con
strumenti relativamente elementari nei primi anni delle superiori [per
esempio nel calcolo delle probabilità];

\item la capacità di manipolare gli infinitesimi renderà quasi
banale la comprensione e le tecniche del calcolo dei limiti;

\item l'uso dei numeri infiniti e infinitesimi, facilita lo sviluppo delle
capacità di astrazione dello studente.
\end{enumerate}

I prerequisiti che consentono di portare avanti questo programma non sono
molti: 

\begin{itemize}
\item un po' di teoria degli insiemi: basta introdurre le operazioni $\cap
,\cup $ e $\times $.

\item per avere applicazioni interessanti nei primi anni bisogna introdurre
qualche nozione di calcolo delle probabilità: basta considerare i casi
in cui lo spazio degli eventi sia finito ed ogni evento elementare abbia la
stessa probabilità. In pratica un po' di calcolo combinatorio.
\end{itemize}

\bigskip

Il progetto Matematicadolce \cite{MD} (del quale siamo venuti a conoscenza
recentemente) prevede l'uso degli infinitesimi fino dal terzo anno delle
superiori. Il percorso di introduzione degli infinitesimi è quello
usuale:%
\[
naturali\rightarrow interi\rightarrow razionali\rightarrow reali\rightarrow
iperreali
\]%
Questo articolo suggerisce un percorso lievemente alternativo, più
vicino allo sviluppo storico:%
\[
naturali\rightarrow interi\rightarrow razionali\rightarrow
euclidei\rightarrow reali
\]%
Dal punto di vista dei fondamenti della matematica si identifica la retta
euclidea con un (particolare) campo iperreale. Il futuro ci dirà se
questa impostazione presenta qualche vantaggio.

\begin{thebibliography}{10}
\phantomsection
\addcontentsline{toc}{section}{Bibliografia}

\bibitem{bencilibro} 
Benci V. 
\textit{Alla scoperta dei numeri infinitesimi,
Lezioni di analisi matematica esposte in un campo non-archimedeo}, 
Aracne editrice, Roma (2018).

\bibitem{benci95} 
Benci V. 
\textit{I numeri e gli insiemi etichettati},
Conferenze del seminario di matematica dell' Università di Bari, vol. 261, 
Laterza, Bari (1995), p. 29.

\bibitem{Bmathesis} 
Benci V. 
\textit{L'analisi infinitesimale mediante i
numeri euclidei. Una introduzione elementare}, 
MatematicaMente, n.218,....,222; 
http://mathesisverona.it/Numeri/Nume218.pdf

\bibitem{benciProb} 
Benci V. 
\textit{Infinitesimi in natura,} 
Atti del convegno: VII giornata nazionale di Analisi Nonstandard, 
(P. Bonavoglia ed.), 
(2017), p. 21-33, DOI: 10.4399/97888255116663.

\bibitem{BDN2003} 
Benci V.,~Di Nasso M. 
\textit{Numerosities of labelled sets: a new way of counting}, 
Adv. Math. 21 (2003), pp.505--567.

\bibitem{BFreg} 
Benci V., Freguglia P. 
\textit{Alcune osservazioni sulla matematica non archimedea}, 
Matem. Cultura e Soc., RUMI, 1 (2016),
pp.105--122.

\bibitem{BF2019} 
Benci V., Freguglia P. 
\textit{La matematica e l'infinito},
Carocci, (2019), ISBN: 8843095250.

\bibitem{BHW16} 
Benci, V., Horsten L., Wenmackers S. 
\textit{Infinitesimal
Probabilities}, Brit. J. Phil. Sci. (2018).

\bibitem{Enriques1} 
Enriques F. 
\textit{Questioni riguardanti le Matematiche Elementari}, 
\textit{raccolte e coordinate da Federigo Enriques}, Parte
prima, vol. I, terza edizione, Nicola Zanichelli editore, Bologna, (1923).

\bibitem{keisler76} 
Keisler H.J. 
\textit{Foundations of Infinitesimal Calculus,} 
Prindle, Weber \& Schmidt, Boston, (1976).

\bibitem{MD} 
Zambelli D. et al. 
\textit{Matematica C3, Matematica dolce 1,...,5},  
Matematicamente.it. (2019). ISBN: 9788899988005.
\end{thebibliography}

% \end{document}

% !TEX encoding = UTF-8

\makecapitolo
{Daniele Zambelli - Liceo Fracastoro, Verona}
{Limiti e analisi non standard}
{L'approccio non standard permette una trattazione più intuitiva dei limiti 
e della continuità. 
Partendo dal confronto delle definizioni saranno proposti alcuni 
limiti risolti con i metodi dell'analisi non standard.
Dati i limiti di tempo, i due concetti importanti:
\begin{itemize} [nosep]
\item la funzione Parte Standard,
\item la relazione di Indistinguibilità,

saranno solo richiamati velocemente.
\end{itemize}
Questo argomento è stato proposto nelle classi terze di un liceo 
economico sociale come applicazione del calcolo con i numeri iperreali.
La teoria e gli esercizi sono contenuti nel testo:

"Matematica Dolce volume 5"\\
(vedi: www.matematicadolce.eu)\cite{zambelli}.
}

\section{Introduzione}

% \noindent
L'analisi standard è stata fondata sul concetto di limite e, partendo da 
questo, ha dimostrato in modo rigoroso e coerente tutte le acquisizioni del 
calcolo infinitesimale. 
Ora, grazie ai numeri iperreali si possono ottenere in modo coerente e 
rigoroso tutti i risultati dell'analisi classica senza utilizzare i limiti.

Ma il concetto di limite, oltre ad avere un buon peso nella programmazione 
della scuola superiore, è anche utile in certe situazioni, in particolare 
nello studio di funzioni.

Il calcolo dei limiti nell'analisi non standard risulta molto più 
semplice di quanto viene proposto nell'analisi ``standard'' tanto che lo si 
può usare come applicazione delle regole di calcolo algebrico negli 
iperreali.

\subsection{Divagazione}
Un uomo stava guardando un ritratto. Qualcuno gli chiese: 

<<Di chi è il ritratto che stai guardando?>>

Egli rispose:

<<Fratelli e sorelle io non ne ho, ma il padre di quest'uomo 
è figlio di mio padre>>.

Di chi era il ritratto che stava guardando?
\cite{smullian}

La difficoltà dell'affermazione deriva dall'alternanza tra i due termini 
``padre'' e ``figlio''.

Una difficoltà analoga si ha quando in una definizione si alternano: ``per 
ogni'' e ``esiste''...

Passiamo ora alle definizioni.

\subsection{Definizione ``standard''}

Si dice che 
\(l\) è il limite della funzione \(f\) per \(x\) che tende a \(c\) se:
\\ \\
% \vspace{1em}
\textbf{per ogni} \(\epsilon\) positivo \\
\textbf{esiste} un corrispondente \(\delta\) positivo tale che \\
\textbf{per ogni} \(x\)  che dista da \(c\) meno di quel \(\delta\): \\
\(f(x)\) dista da \(l\) meno di \(\epsilon\).
% \textbf{per ogni} \(\epsilon > 0\) \textbf{esiste} un 
% \(\delta_\epsilon > 0\) 
% tale che \textbf{per ogni} \(x\) con \(\abs{x-c} < \delta_\epsilon\), 
% \(\abs{f(x)-l} < \epsilon\). 

\vspace{1em}
E si scrive:
\[\lim_{x \rightarrow c} f(x) = l ~\Leftrightarrow~
\forall \epsilon > 0 ~\exists \delta_\epsilon > 0 \text{ tale che }
\forall x \text{ con } \abs{x-c}<\delta_\epsilon ~\Rightarrow~
\abs{f(x)-l}<\epsilon\]

\subsection{Defnizione ``non standard''}

% \begin{definizione}
Si dice che 
\(l\) è il limite della funzione \(f\) per \(x\) che tende a \(c\) se:

\vspace{1em}
\noindent
\textbf{per ogni} \(x\) infinitamente vicino a \(c\) ma diverso da \(c\):\\ 
\(f(x)\) è infinitamente vicino a \(l\). 

\vspace{1em}
E si scrive:
\[\lim_{x \rightarrow c} f(x) = l \Leftrightarrow 
\forall x \tonda{\tonda{x \approx c \wedge x \neq c} \Rightarrow 
\tonda{f(x) \approx l}}\]
% \end{definizione}

% \textbf{osservazione}
% 
% \textbf{Esiste il limite} solo se, \(f(x)\) è infinitamente vicino ad 
% \textbf{un solo} valore reale \(l\) \textbf{qualunque} sia il numero 
% infinitamente vicino a \(c\).

\subsection{Calcolare limiti}
\label{sec:cont_limiti_calcolo}

La definizione \emph{Standard} non dà alcuna informazione su come calcolare 
un limite, dice solo come verificare che un particolare valore sia 
effettivamente il limite. 
Mentre la definizione \emph{Non Standard} fornisce un semplice strumento 
per calcolare il limite.

% \begin{procedura}
Per calcolare il limite di una funzione per \(x\) che tende a un certo 
valore, \(c\) finito, basta calcolare la parte standard del valore della 
funzione per un qualunque numero infinitamente vicino a \(c\):
\[l=\lim_{x \rightarrow c} f(x) = \pst{f(c+\epsilon)}\]
Se \(c\) è infinito basta calcolare la parte standard della funzione 
calcolata per un qualunque valore infinito:
\[l=\lim_{x \rightarrow \infty} f(x) = \pst{f(M)}\]
% \end{procedura}

% \newpage
\subsection{Osservazioni}
\label{subsec:cont_limiti_osservazioni}

% \noindent Le due espressioni \emph{Standard} e \emph{Non Standard} non sono 
% equivalenti, indicano due concetti molto diversi.
\noindent Le due espressioni, quella che contiene il limite e quella che 
contiene l'infinitesimo (o l'infinito), 
non sono equivalenti, indicano due concetti molto diversi.
\setlength{\columnsep}{1cm}
\begin{multicols}{2}
 \[\lim_{x \rightarrow c} f(x) = l\]
Cosa succede ai valori di \(f(x)\) quando \(x\) si 
avvicina a \(c\).\\
Più \(x\) si avvicina a \(c\) più \(f(x)\) si avvicina a \(l\).

 \[\pst{f(c+\epsilon)} = l\]
Cosa succede ai valori di \(f(x)\) quando \(x\) è 
infinitamente vicino a \(c\).\\
Se \(x\) è infinitamente vicino a \(c\) allora \(f(x)\) è 
infinitamente vicino a \(l\).
\end{multicols}
\setlength{\columnsep}{10pt}

\noindent Pur essendo concetti molto diversi, si può dimostrare che il 
valore calcolato nei due modi è lo stesso.
\vspace{1em}

\noindent Seguendo uno schema tipico:
\nopagebreak[3]      % Perché non va????????????????????????
\begin{enumerate} [nosep]
 \item parto da un problema espresso con i reali: \(c\) è un numero reale e 
\(f(x)\) è una funzione reale;
 \item traduco tutto in numeri e funzioni iperreali;
 \item una volta risolto il problema con gli iperreali, che è più semplice 
che risolverlo con i reali, traduco la soluzione in numeri reali.
\end{enumerate}
\vspace{1em}

\noindent A questo punto il calcolo del limite si trasforma in un semplice
calcolo algebrico e quindi può essere proposto come applicazione
dei numeri iperreali ad esempio in una terza superiore.
Calcolo che, nei casi meno banali, richiede qualche attenzione.

\section{Due importanti concetti}
\label{sec:due_importanti_concetti}

Di seguito richiamo due concetti chiave dei numeri iperreali che vengono 
utilizzati nel calcolo dei limiti:

\begin{enumerate} [nosep]
 \item parte standard;
 \item relazione di indistinguibilità.
\end{enumerate}

\subsection{Parte standard}

La \emph{parte standard} di un numero finito iperreale \(x\) è l'unico numero 
reale infinitamente vicino a \(x\).

\[\text{Se} \quad x \in \IR \quad \text{e} \quad a \in \R 
\quad \text{e} \quad x = a + \epsilon 
\quad \text{allora} \quad \pst{x}=a\]

\subsection{Relazione di indistinguibilità}

Due numeri iperreali \(x, y\) sono \emph{indistinguibili} se il rapporto tra la 
loro differenza e ciascuno di essi è un infinitesimo.
\[x \sim y \quad \text{se} \quad \frac{y-x}{x}=\alpha \quad 
\text{e} \quad \frac{y-x}{y}=\beta\]
Osservazione importante:

La relazione di indistinguibilità non è mai vera se uno dei due numeri è zero.

\section{Esempi}
\label{sec:cont_limiti_esempi}

I prossimi esempi mostrano come è possibile applicare la procedura per il 
calcolo del limite a diversi casi tipici.

\subsection{Esempi semplici}
\label{subsec:cont_limiti_esempi_semplici}

\begin{esempio}
\textbf{Limite per \(x\) che tende a un numero infinito}.
\normalfont

\begin{align*}
\lim_{x \rightarrow \infty} \frac{3x^2-3x+7}{5x^2-6} & \stackrel{1}{=} 
  \pst{\frac{3M^2-3M+7}{5M^2-6}} \stackrel{2}{=}  
  \pst{\frac{3\cancel{M^2}}{5\cancel{M^2}}} = \frac{3}{5}
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con \(M\), un generico iperreale infinito;
 \item sostituiamo l'espressione ottenuta con una espressione 
   indistinguibile, eseguiamo i calcoli e calcoliamo la parte standard.
\end{enumerate}
\end{esempio}

\begin{esempio}
\textbf{Limite per \(x\) che tende ad un valore finito \(c\)}.
\normalfont
\begin{align*}
\lim_{x \rightarrow 3} \tonda{x^2-4x+2} & \stackrel{1}{=} 
  \pst{\tonda{3+\epsilon}^2-4\tonda{3+\epsilon}+2} \stackrel{2}{=}\\  
  & \stackrel{2}{=} \pst{9 + 6 \epsilon + \epsilon^2 - 12 - 4\epsilon + 2} 
  \stackrel{3}{=} \pst{9 - 12 + 2} \stackrel{4}{=} -1
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con \(\tonda{3+\epsilon}\);
 \item eseguiamo i calcoli algebrici;
 \item sostituiamo l'espressione con una espressione indistinguibile;
 \item eseguiamo i calcoli e calcoliamo la parte standard.
\end{enumerate}
\end{esempio}

\begin{esempio}
\textbf{Metodo rapido}, ma che non sempre funziona:
\normalfont

\begin{align*}
\lim_{x \rightarrow 3} \tonda{x^2-4x+2} & \stackrel{1}{=} 
  \pst{\tonda{3+\epsilon}^2-4\tonda{3+\epsilon}+2} \stackrel{2}{=}\\ 
  & \stackrel{2}{=}\pst{\tonda{3}^2-4\tonda{3}+2} \stackrel{3}{=}
  \pst{9 - 12 + 2} \stackrel{4}{=} -1
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con \(\tonda{3+\epsilon}\);
 \item sostituiamo l'espressione con una espressione indistinguibile;
 \item eseguiamo i calcoli algebrici;
 \item e calcoliamo la parte standard.
\end{enumerate}
\end{esempio}

\begin{esempio}
\textbf{Limite per \(x\) che tende a 
\mathversion{bold}\(0\)\mathversion{normal}}
\normalfont

\begin{align*}
\lim_{x \rightarrow 0} \frac{x^2-4x+2}{x^2-4} & \stackrel{1}{=} 
  \pst{\frac{\epsilon^2-4\epsilon+2}{\epsilon^2-4}} \stackrel{2}{=}  
  \pst{\frac{2}{-4}} = -\frac{1}{2}
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con \(\tonda{0+\epsilon}=\epsilon\);
 \item sostituiamo l'espressione ottenuta con una espressione 
   indistinguibile, eseguiamo i calcoli e calcoliamo la parte standard.
\end{enumerate}
\end{esempio}

\subsection{Esempi un po' meno banali}
\label{subsec:cont_limiti_esempi_meno_banali}

Ora vediamo alcuni casi un po' più delicati: dove incontriamo il rapporto 
tra infinitesimi e non infinitesimi.

\begin{esempio}
\textbf{Infinitesimo fratto finito non infinitesimo.}
\normalfont

\begin{align*}
\lim_{x \rightarrow -5} \frac{x+5}{x-3} & \stackrel{1}{=} 
  \pst{\frac{-5+\epsilon+5}{-5+\epsilon-3}} \stackrel{2}{=}  
  \pst{\frac{\epsilon}{\epsilon-8}} \stackrel{3}{=} 
  \pst{\frac{\epsilon}{-8}} \stackrel{4}{=} \pst{\delta} = 0
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con \(-5+\epsilon\);
 \item eseguiamo i calcoli;
 \item sostituiamo l'espressione ottenuta con una espressione 
   indistinguibile;
 \item un infinitesimo diviso un finito non infinitesimo dà come risultato 
un infinitesimo e la sua parte standard è zero.
\end{enumerate}
\end{esempio}

\begin{esempio}
\textbf{Finito non infinitesimo fratto infinitesimo.}
\normalfont

\begin{align*}
\lim_{x \rightarrow 4} \frac{2x+6}{x-4} & \stackrel{1}{=} 
  \pst{\frac{2\tonda{4+\epsilon}+6}{4+\epsilon-4}} \stackrel{2}{=}  
  \pst{\frac{14 + 2 \epsilon}{\epsilon}} \stackrel{3}{=} 
  \pst{\frac{14}{\epsilon}} \stackrel{4}{=} 
  \pst{M} \stackrel{5}{~\longrightarrow~} \infty
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con \(4+\epsilon\);
 \item eseguiamo i calcoli;
 \item sostituiamo l'espressione ottenuta con una espressione 
   indistinguibile;
 \item un finito fratto un infinitesimo dà come risultato un infinito; 
 \item gli infiniti non hanno parte standard quindi qui non possiamo 
calcolare \(\pst{M}\).
 
%  usare 
% l'``\(=\)''. Ma i matematici per indicare un numero più grande di qualunque 
% altro numero usano il simbolo ``\(\infty\)'', quindi invece dell'uguale 
% tracceremo una freccia.
\end{enumerate}
\end{esempio}

A questo punto il calcolo si ferma e alcuni autori, ad esempio Keisler 
\cite{keisler} si fermano qui:
\[\text{Il }\lim_{x \rightarrow 4} \frac{2x+6}{x-4} \text{ non esiste}\]
Ma se guardiamo i nostri libri di testo vediamo che esercizi di questo tipo 
sono seguiti da un risultato.

\textbf{Procediamo con l'esempio}\\
Cosa succede alla funzione \(\frac{2x+6}{x-4}\) quando \(x\) si avvicina a 
\(4\) ma è diverso da \(4\)?
Il risultato della funzione diventa sempre più grande in valore assoluto 
infatti: il numeratore si avvicina al numero \(14\) mentre il denominatore 
si avvicina a \(0\).

I matematici per indicare un valore più grande di un qualunque numero 
prefissato usano il simbolo: \(+\infty\). Ovviamente, possiamo indicare 
anche un numero più piccolo di qualunque numero prefissato usando: 
\(-\infty\).
Possiamo quindi interpretare l'espressione \(\pst{M}\) come limite usando i 
simboli di infinito.
Osserviamo anche che non possiamo dire se \(M\) è maggiore o minore di zero;
\(M\) dipende da \(\epsilon\) e precisamente ha lo stesso segno di 
\(\epsilon\) possiamo concludere:
\[\lim_{x \rightarrow 4} \frac{2x+6}{x-4} = \dots
  = \pst{\frac{14}{\epsilon}} = 
  \pst{M} ~\longrightarrow~ 
  \sistema{ -\infty & \text{se } \epsilon < 0\\ 
            +\infty & \text{se } \epsilon > 0}\]
Si può osservare che, usando gli infinitesimi, il limite destro e sinistro 
non hanno bisogno di trattamenti separati, ma parleremo di limite sinistro 
se \(\epsilon\) è minore di zero e di limite destro se \(\epsilon\) è 
maggiore di zero.
\subsection{Esempi più interessanti}
\label{subsec:cont_limiti_esempi_piu_interessanti}

Ora vediamo alcuni casi un po' più interessanti dove dobbiamo usare 
qualche trucco (o è conveniente usarlo).

\begin{esempio}
\textbf{Funzione irrazionale, con radici quadrate}.
\normalfont

\begin{align*}
\lim_{x \rightarrow \infty} \tonda{2x-\sqrt{4x^2-8x+3}} & \stackrel{1}{=} 
  \pst{2M-\sqrt{4M^2-8M+3}} \stackrel{2}{=} \\
  &=\pst{2M-\sqrt{4M^2}} \stackrel{3}{=} 
  \pst{2M-2M} = \pst{0} = 0
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con \(M\);
 \item sostituiamo l'espressione ottenuta con una espressione 
   indistinguibile;
 \item eseguiamo i calcoli \dots
\end{enumerate}
\vspace{1em}
\begin{minipage}{.69\textwidth}
Ma questa volta nei ragionamenti fatti c'è un errore. Proviamo a calcolare 
la funzione per alcuni valori abbastanza grandi di \(x\).\\
I risultati dovrebbero avvicinarsi a zero, ma non è così, sembra si 
avvicinino, invece, a \(2\). Dove abbiamo sbagliato?
\end{minipage}
\begin{minipage}{.39\textwidth}
\begin{center}
\begin{tabular}{r|r}
x & y\\\hline
100 & 2.00252 \\
1000 & 2.00025 \\
10000 & 2.00002 \\
\end{tabular}
\end{center}
\end{minipage}\\

Abbiamo usato in modo improprio la relazione \emph{indistinguibile}: 
abbiamo ottenuto un'espressione indistinguibile da zero, ma ciò non è 
possibile (vedi la definizione di indistinguibile) \dots \\
Dobbiamo seguire un'altra strada.

Consideriamo l'espressione data come una frazione e razionalizziamo il 
numeratore:
\begin{align*}
l &=\lim_{x \rightarrow \infty} 2x-\sqrt{4x^2-8x+3}=
\pst{\frac{\tonda{2M}-\sqrt{4M^2-8M+3}}{1} \cdot 1} = \\
&=\pst{\frac{\tonda{2M}-\sqrt{4M^2-8M+3}}{1} \cdot 
\frac{\tonda{2M}+\sqrt{4M^2-8M+3}}{\tonda{2M}+\sqrt{4M^2-8M+3}}}=\\
&=\pst{\frac{\cancel{4M^2}-\cancel{4M^2}+8M-3}{2M+\sqrt{4M^2-8M+3}}}=
\end{align*}
Questa volta possiamo sostituire l'espressione sotto radice con 
un'espressione indistinguibile, senza ottenere zero:
\[=\pst{\frac{8M-3}{2M+\sqrt{4M^2-8M+3}}} =
   \pst{\frac{8M}{2M+\sqrt{4M^2}}}=\]
e svolgendo i calcoli:
\(=\pst{\dfrac{8M}{2M+2M}}=
   \pst{\dfrac{8\cancel{M}}{4\cancel{M}}} = 2\)\\
otteniamo un risultato in accordo con l'andamento della funzione.
\end{esempio}

\begin{esempio}
\textbf{Funzione razionale}: metodo rapido non sempre funzionante.
\normalfont

\begin{align*}
\lim_{x \rightarrow 2} \frac{x^3+3x^2+2x}{x^2-x-6} & \stackrel{1}{=} 
\pst{\frac
  {\tonda{2+\epsilon}^3+3\tonda{2+\epsilon}^2+2\tonda{2+\epsilon}}
  {\tonda{2+\epsilon}^2-\tonda{2+\epsilon}-6}} \stackrel{2}{=}\\ 
  &=\frac{2^3+3\cdot 2^2+2\cdot 2}{2^2-2-6} =
  \pst{\frac{8+12+4}{4-2-6}} \stackrel{3}{=} \pst{\frac{24}{-4}} =-6
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con un numero infinitamente vicino a \(+2\);
 \item sostituiamo le espressioni tra parentesi con espressioni 
indistinguibili;
 \item poiché i risultati ottenuti sono diversi da zero, il risultato è 
effettivamente indistinguibile. Quindi il risultato ottenuto è valido.
\end{enumerate}
\end{esempio}

Ora vediamo un caso in cui il metodo precedente non funziona. 
% Prima di 
% affrontare l'esempio ricordiamoci che se non abbiamo maggiori informazioni 
% sugli infinitesimi \(\alpha\) e \(\beta\), non possiamo calcolare 
% \(\frac{\alpha}{\beta}\).

\begin{esempio}
\textbf{Funzione razionale}: metodo rapido ma inconcludente.
\normalfont

\begin{align*}
\lim_{x \rightarrow -2} \frac{x^3+3x^2+2x}{x^2-x-6} & = 
\pst{\frac
  {\tonda{-2}^3+3\tonda{-2}^2+2\tonda{-2}}
  {\tonda{-2}^2-\tonda{-2}-6}} =\\ 
  &=\pst{\frac{-8+12-4}
              {4+2-6}} = \pst{\frac{0}{0}} = \dots
\end{align*}
Ma qui ci scontriamo con 2 problemi: abbiamo usato la relazione di 
indistinguibile con lo zero, e abbiamo ottenuto una divisione per zero che 
non è definita. Potremmo essere un po' più pignoli, ma ancora troppo 
grossolani, osservando che quando \(x\) si avvicina a \(-2\) il numeratore e 
il denominatore si \emph{avvicinano} a zero, sono cioè degli infinitesimi e quindi 
otteniamo: \(\frac{\alpha}{\beta}\) ma anche questa maggior precisione non 
è sufficiente per calcolare un risultato reale.
\vspace{1em}

Metodo lungo ma sicuro:
dobbiamo rimboccarci le maniche e affrontare il calcolo algebrico 
ricordandoci che: \\
\(\tonda{x+a}^3= x^3+3x^2a+3xa^2+a^3\)

\begin{align*}
\lim_{x \rightarrow -2} \frac{x^3+3x^2+2x}{x^2-x-6} & \stackrel{1}{=} 
\pst{\frac
  {\tonda{-2+\epsilon}^3+3\tonda{-2+\epsilon}^2+2\tonda{-2+\epsilon}}
  {\tonda{-2+\epsilon}^2-\tonda{-2+\epsilon}-6}} \stackrel{2}{=}\\ 
  &\stackrel{2}{=}\pst{\frac{-8+12\epsilon-6\epsilon^2+\epsilon^3+
             3\tonda{4-4\epsilon+\epsilon^2}-4+2\epsilon}
             {4-4\epsilon+\epsilon^2-\tonda{-2+\epsilon}-6}} =\\ 
  &=\pst{\frac{\cancel{-8}+\cancel{12\epsilon}-6\epsilon^2+\epsilon^3+
          \cancel{12}\cancel{-12\epsilon}+3\epsilon^2\cancel{-4}+2\epsilon}
             {\cancel{4}-4\epsilon+
              \epsilon^2\cancel{+2}-\epsilon\cancel{-6}}}=\\ 
  &=\pst{\frac{\epsilon^3+2\epsilon}{\epsilon^2-5\epsilon}} = 
    \pst{\frac{\cancel{\epsilon} \tonda{\epsilon^2+2}}
             {\cancel{\epsilon} \tonda{\epsilon-5}}}  \stackrel{3}{=} 
    \pst{\frac{2}{-5}} = -\frac{2}{5}
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con \(-2+\epsilon\);
 \item eseguiamo tutti i calcoli e semplifichiamo;
 \item sostituiamo l'espressione con una indistinguibile.
\end{enumerate}
\end{esempio}

\textbf{Osservazione}\\
Pensate alla complicazione dei calcoli se ci fosse un qualche \(x^4\) o
\(x^5\)\dots
Vedremo ora un altro modo di calcolare il limite che risulta meno 
complicato.
\vspace{1em}

Prima di affrontare il prossimo metodo ricordiamoci che possiamo 
rappresentare tutti gli infinitesimi di ordine superiore ad un certo 
infinitesimo \(\epsilon\) con il simbolo: \(o\tonda{\epsilon}\).

\begin{esempio}
\textbf{Funzione razionale}: altro metodo.
\normalfont

\begin{align*}
\lim_{x \rightarrow -2} \frac{x^3+3x^2+2x}{x^2-x-6} & \stackrel{1}{=} 
\pst{\frac
  {\tonda{-2+\epsilon}^3+3\tonda{-2+\epsilon}^2+2\tonda{-2+\epsilon}}
  {\tonda{-2+\epsilon}^2-\tonda{-2+\epsilon}-6}} \stackrel{2}{=}\\ 
  &\stackrel{2}{=}\pst{\frac{-8+12\epsilon+12-12\epsilon-4+2\epsilon+
                             o\tonda{\epsilon}}
                           {4-4\epsilon+2-\epsilon-6+o\tonda{\epsilon}
                           }}=\\ 
  &=\pst{\frac{\cancel{-8}+\cancel{12\epsilon}+
               \cancel{12}\cancel{-12\epsilon}\cancel{-4}+2\epsilon+
               o\tonda{\epsilon}}
              {\cancel{4}-4\epsilon\cancel{+2}-\epsilon\cancel{-6}+
               o\tonda{\epsilon}}}\stackrel{3}{=}\\
  &\stackrel{3}{=}
    \pst{\frac{2\cancel{\epsilon}}{-5\cancel{\epsilon}}} = -\frac{2}{5}
\end{align*}

\begin{enumerate} [nosep]
 \item sostituiamo \(x\) con \(-2+\epsilon\);
 \item eseguiamo i calcoli riunendo in un unico simbolo tutti gli 
infinitesimi di ordine superiore a \(\epsilon\) e perciò trascurabili 
(si spera);
 \item sostituiamo l'espressione con una indistinguibile poi calcoliamo la 
parte standard.
\end{enumerate}
\end{esempio}

% \begin{esempio}
% \textbf{Funzione razionale}: altro metodo.
% \begin{align*}
% \lim_{x \rightarrow 2} \frac{x^3+3x^2+2x}{x^2-x-6} & \stackrel{1}{=} 
% \tonda{\pst{\frac
%   {\tonda{-2+\epsilon}^3+3\tonda{-2+\epsilon}^2+2\tonda{-2+\epsilon}}
%   {\tonda{-2+\epsilon}^2-\tonda{-2+\epsilon}-6}} \stackrel{2}{=}}\\ 
%   &=\pst{\frac{-8+12-4}{4+2-6}} = \pst{\frac{0}{0}} 
%   \text{ Indistinguibile non è applicabile.}
% \end{align*}
% 
% È evidente che \(-2\) è uno zero sia del numeratore sia del denominatore 
% quindi, per il teorema di Ruffini, entrambi questi polinomi sono 
% divisibili 
% per \(x+2\).
% Possiamo scomporre i due polinomi, semplificarli procedendo nel seguente 
% modo:
% 
% \begin{align*}
% \lim_{x \rightarrow -2} \frac{x^3+3x^2+2x}{x^2-x-6} &=
% \lim_{x \rightarrow -2} \frac{x \tonda{x+1} \cancel{\tonda{x+2}}}
%           {\tonda{x-3} \cancel{\tonda{x+2}}} \stackrel{1}{=}
% \lim_{x \rightarrow -2} \frac{x^2+x}{x-3}=\\
% &=\tonda{\pst{\frac{\tonda{-2+\epsilon}^2-\tonda{2+\epsilon}}
%                    {\tonda{-2+\epsilon}-3}}}\stackrel{2}{=}
% \pst{\frac{\tonda{-2}^2-2}{-2-3}}=
% \pst{\frac{2}{-5}} = -\frac{2}{5}
% \end{align*}
% Dove le uguaglianze hanno i seguenti motivi:
% \begin{enumerate} [nosep]
%  \item possiamo semplificare perché \(x\) è infinitamente vicino a \(-2\) 
% ma 
% è 
% diverso da \(-2\);
%  \item sostituiamo l'espressione con una indistinguibile operazione, 
% questa 
% volta, applicabile perché le due espressioni sono diverse da zero.
% \end{enumerate}
% \end{esempio}

\textbf{Osservazione}\\
Abbiamo ottenuto lo stesso valore ricavato facendo tutti i 
calcoli algebrici, ma i passaggi sono più semplici.

\section{Conclusione}
In Analisi Non Standard (NSA) i \emph{limiti} non sono essenziali per le 
dimostrazioni e la coerenza della teoria, ma anche nell'NSA possiamo 
parlare di \emph{limiti} (vedi \cite{henle}).

A volte è comodo, a volte è addirittura necessario come quando nel 
tracciare il grafico di una funzione non siamo interessati a quanto vale la 
funzione nell'infinitamente vicino ad un punto, ma come si comporta 
avvicinandosi a quel punto.

In NSA il limite si calcola ricavando la parte standard di un'espressione.
Le competenze necessarie si posseggono già dopo aver studiato i numeri 
iperreali. 
Questo permette di trattare i limiti in una terza superiore.

L'uso degli iperreali permette anche di calcolare la derivata ad una 
funzione in un punto (magari invece che derivata la chiameremo pendenza del 
grafico). In questo modo, già dalla terza gli alunni hanno la possibilità 
di famigliarizzarsi e di utilizzare concetti che poi acquisiranno un peso 
importante nello studio dell'analisi matematica \dots

\vspace{1em}
\dots analisi matematica che forse potremo chiamare di nuovo:
\begin{center}
{\huge Calcolo infinitesimale}
\end{center}


\begin{thebibliography}{10}
\phantomsection
\addcontentsline{toc}{section}{Bibliografia}

\bibitem[AAVV 2019]{zambelli} 
\textsc{Daniele Zambelli} (a cura di), 
\emph{Matematica Dolce 5}, 
\url{www.matematicadolce.eu}

\bibitem[Goldoni, 2016]{goldoni1} 
\textsc{Giorgio Goldoni}, 
\emph{I numeri Iperreali}, 
\url{ilmiolibro.kataweb.it/libro/scienza-e-tecnica/106196/i-numeri-iperreali
}

\bibitem[Henle, Kleinberg, 1979]{henle} 
\textsc{James M. Henle, Eugene Kleinberg}, 
\emph{Infinitesimal calculus}, 
New York, 1979

\bibitem[Keisler, 2015]{keisler} 
\textsc{Howard Jerome Keisler},
\emph{Elementary Calculus: An Infinitesimal Approach}, 
\url{www.math.wisc.edu/~keisler/calc.html}

\bibitem[SMUL 1984]{smullian} 
\textsc{Raimond Smullyan}, 
\emph{Qual è il titolo di questo libro?},
Bologna, 1984

\bibitem[Stecca, 2016]{stecca} 
\textsc{Bruno Stecca, Daniele Zambelli}, 
\emph{Analisi Non standard}, 
\url{nsa.readthedocs.io}

\end{thebibliography}

% \end{document}

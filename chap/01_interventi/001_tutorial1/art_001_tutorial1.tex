
\makecapitolo
{Daniele Zambelli}
{Una prima introduzione ai numeri iperreali}
{Presentiamo una sintesi dell'introduzione ai numeri iperreali. 
L'intervento e stato svolto seguendo il capitolo sugli insiemi numerici del 
testo ``Matematica Dolce 3''\cite{zambelli1}. 
\footnote{Esempi ed esercizi si trovano nel testo citato. 
Questo e gli altri testi di Matematica Dolce si posso scaricare 
liberamente da: 
\href{https://bitbucket.org/zambu/matematicadolce/downloads/}
     {bitbucket.org/zambu/matematicadolce/downloads/}}.
}\\ \\
%\newline
\begin{comment}
 
\end{comment}

\section{Perché gli Iperreali}
\label{sec:iperr_velocita}

Un gran numero di problemi può venire affrontato e risolto più facilmente 
se nei calcoli consideriamo delle quantità assolutamente trascurabili.
Dalla metà del 1600 alla metà del 1800 la matematica e la fisica hanno 
beneficiato moltissimo dall'uso di trascurabili.
I risultati sono stati grandiosi, pensiamo all'analisi e alla fisica 
classica, ma da un punto di vista teorico queste nuove grandezze hanno 
delle incongruenze che i matematici allora non sono stati in grado di 
capire profondamente e superare.

L'analisi matematica è stata riformulata mettendo al bando le quantità 
assolutamente trascurabili e passando quindi dall'``infinitamente vicino'' 
al ``vicino quanto si vuole''.
A metà del secolo scorso Abraham Robinson, matematico tedesco, nella sua 
opera: ``Non-standard analysis'' ha dato piena legittimità all'uso delle 
quantità assolutamente trascurabili creando un nuovo insieme numerico: gli 
Iperreali.

Vedremo ora come si possono introdurre questi nuovi numeri in una classe 
terza di una scuola secondaria di secondo grado.

\section{Infinitesimi... e infiniti}
\label{sec:iperr_nonarchimedei}

Chiamiamo ``infinitesimo'' un numero assolutamente trascurabile cioè un 
nuovo numero (non reale) maggiore di zero ma più piccolo di qualunque 
numero 
reale (o razionale) positivo:
\[\epsilon > 0 \quad \text{ tale che } \quad 
\epsilon < \frac{1}{n} \quad \text{ per qualunque } n \in \N\]
tradotto in simboli:
\[\exists \epsilon > 0 \quad | \quad \epsilon < \frac{1}{n} \quad \forall n 
\in \N\]
e aggiungiamo questo numero all'insieme dei reali.
\begin{osservazione}
 In un insieme che contenga numeri infinitesimi non vale il postulato di 
Eudosso-Archimede infatti se
\(\epsilon < \dfrac{1}{n} \quad \forall n \in \N\) 
moltiplicando entrambi i membri per \(n\) 
si ottiene: \(n \epsilon < 1\).
\end{osservazione}

\begin{osservazione}
Se esiste un numero infinitesimo allora:
\begin{enumerate}
 \item esistono infiniti infinitesimi, infatti anche la sua metà, la sua 
quarta parte, ..., il suo doppio, ..., sono tutti infinitesimi;
 \item esistono numeri infiniti infatti il reciproco di ogni infinitesimo è 
più grande di qualunque numero:
\[\text{se} \quad \epsilon < \frac{1}{n} \quad \forall n \in \N 
\quad \text{allora} \quad \frac{1}{\epsilon} > n \quad \forall n 
\in \N\]
\end{enumerate}
\end{osservazione}

Quindi se aggiungiamo all'insieme dei reali un numero infinitesimo, e 
possiamo usarlo nelle usuali 4 operazioni, allora in questo nuovo insieme 
avremo un numero infinito di infinitesimi e un numero infinito di infiniti.

\section{Tipi di iperreali}
\label{sec:iperr_iperreali}

Possiamo classificare i numeri presenti in questo nuovo insieme nel 
seguente modo:

\begin{description} [noitemsep]
 \item \textbf{Infinitesimi}:
numeri che, in valore assoluto, sono minori di qualunque numero reale 
positivo.
 \item \textbf{Infiniti}:
numeri che, in valore assoluto, sono maggiori di qualunque numero reale.
 \item \textbf{Zero}:
l'unico numero reale infinitesimo.
 \item \textbf{Infinitesimi non nulli}:
numeri infinitesimi, escluso lo zero.
 \item \textbf{Finiti}:
numeri che non sono infiniti.
 \item \textbf{Finiti non infinitesimi}:
numeri che non sono né infiniti né infinitesimi.
\end{description}

Per semplificare la scrittura possiamo adottare delle sigle e delle 
convenzioni:

\begin{center}
\begin{tabular}{ccc}\toprule
tipo & sigla & simboli \\\midrule

zero &  & 0 \\

infinitesimi & \emph{i} & \\

infinitesimo non nullo & \emph{inn} & 
\(\alpha, \beta, \gamma, \delta, \dots\) \\

finito non infinitesimo & \emph{fni} & \(a, b, c, d, \dots\)\\

finito & \emph{f} & \(a, b, c, d, \dots\) \\

infinito & \emph{I} & \(A, B, C, \dots\)\\

qualsiasi &  & \(x, y, z, \dots\) \\\bottomrule
\end{tabular}
\label{tab:iperr_tipi}
\end{center}

\section{Numeri infinitamente vicini}
\label{sec:iperr_infinitamentevicini}

Nei numeri reali, due numeri o sono uguali o sono diversi (ovviamente).
Nel primo caso, la differenza tra i due numeri è zero, 
nel secondo, la differenza è un numero reale diverso da zero.

Negli iperreali, se due numeri sono diversi, la loro differenza può 
essere un numero infinito, un numero finito non infinitesimo o un numero 
infinitesimo.

Riassumendo, possiamo distinguere i seguenti casi:

\begin{itemize} [noitemsep] %, label=$\bullet$]
 \item \(x\) e \(y\) sono uguali: \(y-x=0\);
 \item \(x\) e \(y\) sono diversi e in questo caso:
\begin{itemize} [nosep, label=$\bullet$] %, label=$\blacktriangle$]
 \item \(x-y\) è un infinito;
 \item \(x-y\) è un finito non infinitesimo;
 \item \(x-y\) è un infinitesimo non nullo.
\end{itemize}
\end{itemize}

Quando la differenza di due numeri è un infinitesimo, diciamo 
che i due numeri sono \emph{infinitamente vicini}.

\begin{definizione}    % [Infinitamente vicini]
Due numeri si dicono \textbf{infinitamente vicini} (simbolo:~\(\approx\)) 
se la loro differenza è un infinitesimo:
\[x \approx y \Leftrightarrow x - y = \epsilon\]
\end{definizione}

Tutti gli infinitesimi sono infinitamente vicini tra di loro e sono 
infinitamente vicini a zero.

Dato un qualunque numero iperreale l'insieme di tutti i numeri 
infinitamente vicini a questo si dice ``monade'': 

\begin{definizione}
 Si chiama \emph{monade} del numero \(x\) l'insieme formato da tutti i 
numeri iperreali infinitamente vicini a \(x\).
\end{definizione}

Due monadi diverse non hanno elementi in comune.

Una monade può contenere al massimo un numero reale, due numeri reali 
diversi appartengono a monadi diverse.

\section{Iperreali finiti e parte standard}
\label{sec:iperr_partestandard}

Tra i vari tipi di iperreali, hanno un ruolo particolare gli iperreali 
finiti perché sono quelli che assomigliano di più ai numeri che già 
conosciamo e possono essere facilmente tradotti in numeri reali e 
approssimati con numeri razionali. 

\begin{definizione}
 Un numero iperreale si dice \textbf{finito} se è un numero compreso tra 
due numeri Reali:
\[\text{Se } x \in \IR \sand a, b \in \R \sand 
  a < x < b \quad \text{ allora } x \text{ è un Iperreale finito.}\]
\end{definizione}

\noindent Ogni numero finito può essere visto come un numero \emph{reale} 
più un \emph{infinitesimo}.

Se \(x\) è finito allora \(x = a + \epsilon\) dove:
\begin{itemize} [noitemsep]
 \item \(x\) è un numero iperreale finito;
 \item \(a\) è un numero reale;
 \item \(\epsilon\) è un infinitesimo (anche zero).
\end{itemize}

Se \(x=a+\epsilon\) allora potremmo dire che \(x\) è infinitamente vicino 
ad \(a\) infatti la differenza tra i due dà un infinitesimo: 
\[x=a+\epsilon \sLRarrow
x-a = a+\epsilon-a \sLRarrow x-a = \epsilon \sLRarrow x \approx a\]
Come visto parlando delle monadi, esiste un solo numero Reale 
infinitamente vicino ad un dato numero Iperreale finito. 
Questo numero reale si chiama \emph{parte standard} del numero Iperreale.

\begin{definizione}
 Si dice che \(a\) è la \textbf{parte standard} di \(x\), e si scrive: 
 \(\st(x) = a\), se \(a\) è un numero reale e \(x\) è un numero iperreale
infinitamente vicino ad \(a\):
\[\st(x) = a \sLRarrow a \in \R \sand x \approx a\mbox{, con }x\in \IR\]
\end{definizione}
\begin{osservazione}
~

\begin{itemize} [nosep]
 \item 
La parte standard di un infinitesimo è zero infatti:
\(\epsilon = 0+\epsilon\).
 \item 
Un numero iperreale infinito non ha parte standard poiché non esiste nessun 
numero reale infinitamente vicino a un infinito.
\end{itemize}
\end{osservazione}
\vspace{1em}
La Parte Standard presenta alcune proprietà che possono essere facilmente 
dimostrate. Nel seguito \(x,\ y\) rappresentano iperreali finiti 
(poiché gli infiniti non hanno parte standard):

\begin{enumerate} [nosep]
 \item \(\pst{-x} = -\pst{x}\)
%  \item \(\pst{x \pm y} = \pst{x} \pm \pst{y}\)
 \item \(\pst{xy} = \pst{x} \cdot \pst{y}\)
 \item \(se ~ \pst{y} \neq 0 ~ allora ~
 \pst{\frac{x}{y}} = \frac{\pst{x}}{\pst{y}}\)
 \item \(se ~  x \leqslant y ~ allora ~ 
 \pst{x} \leqslant \pst{y}\).
\end{enumerate}
 
\section{Retta Iperreale e strumenti ottici}
\label{sec:iperr_retta}

I numeri reali hanno un potente modello che permette di semplificare la 
loro interpretazione: la \emph{retta reale}.

Ad ogni numero reale corrisponde un punto della retta reale e ad ogni 
punto della retta reale corrisponde un numero reale. 
Come è stata inventata la retta reale possiamo inventare la \emph{retta 
iperreale}. 

Osservandole ``a occhio nudo'', le due rette sembrano identiche.
Per vedere la differenza dobbiamo dotarci di ``strumenti ottici'' \emph{non 
standard}. 
Questi non solo forniscono rapporti di visualizzazione finiti, ma anche 
infiniti. È possibile in questo modo visualizzare un qualunque numero 
iperreale finito o infinito. Questi strumenti possono anche essere usati 
in combinazione tra di loro, di seguito riporto alcuni esempi.

%\subsection{Alcuni esempi}
%\label{subsec:iperr_microscopio}

\newcommand{\asse}[4]{% Asse orizzontale.
  % Esempio di chiamata:
  %% \disegno{\asse{-7}{+7}{0}{\(t\)}
  \def \xmi{#1}
  \def \xma{#2}
  \def \posy{#3}
  \def \var{#4}
  \draw [-{Stealth[length=2mm, open, round]}] (\xmi, \posy) -- (\xma, 
\posy);
  \node [below] at (\xma, \posy) {\var};
}

\newcommand{\assecontrattini}[4]{% Asse.
  % Esempio di chiamata:
  %% \disegno{\assecontrattini{-7}{+7}{0}{\(t\)}
  \def \xmi{#1}
  \def \xma{#2}
  \def \posy{#3}
  \def \var{#4}
  \draw[gray!50, very thin, step=1] 
       (\xmi, \posy-0.1) grid (\xma-1, \posy+0.1);
  \asse{#1}{#2}{#3}{#4};
}

\newcommand{\segmentocontrattini}[3]{% Segmento con trattini.
  % Esempio di chiamata:
  %% \disegno{\segmentocontrattini{-7}{+7}{0}
  \def \xmi{#1}
  \def \xma{#2}
  \def \posy{#3}
  \pgfmathsetmacro \endx{int(\xma-\xmi)}
  \foreach \x in {0, 1, ..., \endx}
    \draw[very thin] (\xmi+\x, \posy-0.1) -- (\xmi+\x, \posy+0.1);
  \draw [-] (\xmi-.3, \posy) -- (\xma+.3, \posy);
}

\newcommand{\microscopiod}{% Microscopio per vedere 2 - 3 \epsilon.
    \disegno{
    \assecontrattini{-1.3}{+6.3}{0}{x}
    \draw (0, 0) [below] node{0} (1, 0) [below] node{1}
          (2, 0) [below] node{2};
    \microscopio{(2, 0)}{2.5}{60}{-100}{3.5}{(7.5, 8.5)}
                {\(\times \frac{1}{\epsilon}\)}
    \segmentocontrattini{.7}{7}{6}{1}
    \draw (.8, 6) [left] node [rotate=90] {\(2 - 3 \epsilon\)} 
          (2.8, 6) [left] node [rotate=90] {\(2 - \epsilon\)}
          (3.8, 6) [left] node [rotate=90] {2};
    }
}

\newcommand{\telescopiob}{% Telescopio per visualizzare 127034.
    \disegno{
    \assecontrattini{-1.3}{+6.3}{0}{x}
    \draw (0, 0) [below] node{0} (1, 0) [below] node{1};
    \draw (-1, 1) pic [rotate=0, scale=.5] {telescopio=\(A\)};
    \microscopio{(-1, 1)}{2.5}{40}{-130}{3.5}{(7.5, 9.5)}{}
    \segmentocontrattini{0}{+6.2}{6}{1}
    \draw (0, 6) [left] node [rotate=90] {\dots} 
          (1, 6) [left] node [rotate=90] {\(A - 1\)} 
          (2, 6) [left] node [rotate=90] {\(A ~~\quad\)}
          (3, 6) [left] node [rotate=90] {\(A + 1\)}
          (5, 6) [left] node [rotate=90] {\(A + 3\)}
          (6, 6) [left] node [rotate=90] {\dots};
    }
}

\newcommand{\grandangolob}{% Grandangolo per vedenodere 300.
    \disegno{
    \assecontrattini{-1.3}{+6.3}{0}{x}
    \draw (0, 0) [below] node{0} (1, 0) [below] node{1};
    \grandangolo{(0, 0)}{2.5}{90}{-126}{3.5}{(5.5, 8.5)}{\(\div A\)}
    \segmentocontrattini{-1.1}{+5.2}{6}{1}
    \draw (1.9, 6) [left] node [rotate=90] {0} 
          (2.9, 6) [left] node [rotate=90] {\(A\)}
          (-.1, 6) [left] node [rotate=90] {\(-2 A\)};
    }
}

\newcommand{\combinazione}{% Combinazione per: \(1741,998+2\epsilon\).
    \disegno{
    \assecontrattini{-1.3}{+6.3}{0}{x}
    \draw (0, 0) [below] node{0} (1, 0) [below] node{1};
    \draw (-1, 1) pic [rotate=0, scale=.5] {telescopio=1742};
    \microscopio{(-1, 1)}{2.5}{40}{-130}{3.5}{(7.5, 9.5)}{}
    \segmentocontrattini{0}{+6.2}{6}{1}
    \draw (1., 6) [left] node [rotate=90] {\dots}
          (2., 6) [left] node [rotate=90] {1741}
          (3., 6) [left] node [rotate=90] {1742}
          (4., 6) [left] node [rotate=90] {1743}
          (5., 6) [left] node [rotate=90] {\dots};
    \microscopio{(3., 6)}{2.5}{130}{-40}{3.5}{(-5.2, 12.7)}{\(\times 1000\)}
    \segmentocontrattini{-4.5}{+1.9}{10.5}{1}
    \draw (-3.5, 10.5) [left] node [rotate=90] {\dots}
          (-2.5, 10.5) [left] node [rotate=90] {1741,998}
          (-1.5, 10.5) [left] node [rotate=90] {1741,999}
          (-0.5, 10.5) [left] node [rotate=90] {1742}
          (0.5, 10.5) [left] node [rotate=90] {\dots};
    \microscopio{(-2.5, 10.5)}{2.5}{30}{-120}{3.5}{(5, 17)}
                {\(\times \frac{1}{\epsilon}\)}
    \segmentocontrattini{-1.77}{+4.6}{15}{1}
    \draw (-0.7, 15) [right] node [rotate=90] {\dots}
          (0.3, 15) [right] node [rotate=90] {1742,998}
          (1.3, 15) [right] node [rotate=90] {\(1741,998+\epsilon\)}
          (2.3, 15) [right] node [rotate=90] {\(1741,998+2\epsilon\)}
          (3.3, 15) [right] node [rotate=90] {\dots};
    }
}

\noindent{
\begin{inaccessibleblock}[Microscopio telescopio e grandangolo non 
standard.]
\begin{minipage}{.48\linewidth}
 \begin{center}
\scalebox{0.6}{\microscopiod}
\footnotesize{\\Microscopio (\(2-3\epsilon\))\\}
 \end{center}
\end{minipage}
\hfill
\begin{minipage}{.48\linewidth}
 \begin{center}
\scalebox{0.6}{\telescopiob}
\footnotesize{\\Telescopio (\(A+3\))\\}
 \end{center}
\end{minipage}

\begin{minipage}{.48\linewidth}
 \begin{center}
\scalebox{0.6}{\grandangolob}
\footnotesize{\\Grandangolo o Zoom (\(-2A\))\\}
 \end{center}
\end{minipage}
\hfill
\begin{minipage}{.48\linewidth}
 \begin{center}
\scalebox{0.45}{\combinazione}
\footnotesize{\\Combinazione (\(1742,998+2 \epsilon\))\\}
 \end{center}
\end{minipage}
\end{inaccessibleblock}
}

\section{Operazioni}
\label{sec:iperr_operazioni}

Le regole relative alle operazioni sono, in generale abbastanza intuitive.
In questo intervento presento solo qualche esempio:

\begin{enumerate} [noitemsep]
 \item Infinito + infinito = dipende;
 \item Infinito positivo + infinito positivo = Infinito positvo;
 \item infinitesimo per infinitesimo = infinitesimo;
 \item infinitesimo fratto infinitesimo = dipende;
 \item infinitesimo fratto non infinitesimo = infinitesimo;
 \item non infinitesimo fratto infinitesimo = Infinito;
 \item infinitesimo per Infinito = dipende;
 \item finito più finito = finito;
 \item ...
\end{enumerate}

Dove ho indicato il risultato con il termine ``dipende'' non intendo che 
l'operazione non sia definita o sia indeterminata, ma che per trovare il 
risultato ho bisogno di maggiori informazioni.

Non ci sono regole immediate per le seguenti operazioni:
\begin{multicols}{4}
\begin{itemize} [nosep]
 \item \(\dfrac{\epsilon}{\delta}\)
 \item \(\dfrac{A}{B}\)
 \item \(A \cdot \epsilon\)
 \item \(A + B\)
\end{itemize}
\end{multicols}
In questi casi il tipo di risultato dipende dall'effettivo valore degli 
operandi. Ad esempio, nel caso del quoziente tra due infinitesimi possiamo 
trovarci nelle seguenti situazioni:
\begin{multicols}{3}
\begin{itemize} [nosep]
 \item \(\dfrac{\epsilon^2}{\epsilon} = \epsilon\) \quad (i)
 \item \(\dfrac{2\epsilon}{\epsilon} = 2\) \quad (fni)
 \item \(\dfrac{\epsilon}{\epsilon^2} = \frac{1}{\epsilon}\) \quad (I)
\end{itemize}
\end{multicols}

\section{Confronto}
\label{sec:iperr_confronto}

L'insieme dei numeri Reali ha un ordinamento completo, se \(a\) e \(b\) 
sono due numeri reali qualunque è sempre valida una e una sola delle 
seguenti affermazioni:
\[a<b \quad a=b \quad b<a\]
Per confrontare due numeri Reali possiamo utilizzare le seguenti regole:

\begin{enumerate} [nosep]
 \item qualunque numero negativo è minore di qualunque numero positivo;
 \item se due numeri sono negativi, è minore quello che ha il modulo 
maggiore;
 \item se \(a\) e \(b\) sono due numeri positivi, 
 \[a<b \sLRarrow a-b<0 \quad \text{ (o } \quad b-a>0 \text{ )}\]
oppure
 \[a<b \sLRarrow \frac{a}{b}<1 \quad \text{ (o } \quad \frac{b}{a}>1 
   \text{ )}\]
\end{enumerate}

\begin{osservazione}
Le prime due regole ci permettono di restringere le nostre riflessioni al 
solo caso del confronto tra numeri positivi.
Per semplificarci la vita restringeremo il confronto ai numeri positivi, senza perdere in generalità.
\end{osservazione}

\begin{osservazione}
Nella terza regola abbiamo presentato due criteri. Quello usato di solito
è il primo, ma useremo anche il secondo perché il rapporto tra due 
grandezze permette di ottenere informazioni interessanti.
\end{osservazione}

\vspace{1em}

Anche negli Iperreali valgono le proprietà dei Reali richiamate sopra.

Il confronto tra tipi diversi di iperreali è banale (ricordo che ci stiamo 
limitando ai numeri non negativi):
\[0  \quad \leqslant  \quad i  %\quad \leqslant  \quad inn 
\quad  <  \quad fni  \quad <  \quad I\]
Possiamo osservare che un \emph{finito non infinitesimo} non solo è 
maggiore di un \emph{infinitesimo}, ma è \textbf{infinitamente} più grande 
e anche un \emph{Infinito} è infinitamente più grande di un \emph{finito}.

Risulta interessante esplorare il confronto usando il rapporto tra numeri 
nel caso di:

\begin{description} [noitemsep]
 \item {\bf infinitesimi}: 
Come abbiamo visto negli ultimi esempi del paragrafo sulle operazioni, 
non solo il rapporto può essere un numero minore o maggiore di~1, ma il 
rapporto può anche essere un infinitesimo o un infinito; 
nel primo caso diremo che il primo infinitesimo è un infinitesimo di 
ordine superiore al secondo: è infinitamente più piccolo.
 \item {\bf finiti non infinitesimi}:
Il rapporto è sempre un \emph{fni}.
 \item {\bf infiniti}:
Valgono i discorsi fatti per gli infinitesimi, e anche in questo caso, il rapporto ci permette di individuare gli infiniti di ordine superiore: infinitamente più grande.
\end{description}

\section{Indistinguibili}
\label{sec:iperr_indistinguibili}

Nella soluzione di problemi, normalmente si segue un percorso composto da 
questi tre passi:
\begin{enumerate}
 \item trasformo il problema reale in un problema iperreale;
 \item risolvo il problema iperreale;
 \item trasformo il risultato iperreale in risultato reale, tipicamente 
prendendo la parte standard del risultato.
\end{enumerate}

Quando ci muoviamo in questo modo, possiamo spesso semplificare 
notevolmente i calcoli con gli iperreali usando la relazione di 
indistinguibilità, cioè sostituendo un'espressione con una diversa, ma 
indistinguibile.

\begin{definizione}
Due numeri si dicono \textbf{indistinguibili} (simbolo:~\(\sim\)) se il 
rapporto tra la loro differenza e ciascuno di essi è un infinitesimo:
\[x \sim y \sLRarrow 
\tonda{\frac{y-x}{x} = \epsilon \quad \wedge \quad \frac{y-x}{y} = \delta}
\]
\end{definizione}

\begin{osservazione}
 È importante osservare che per poter applicare la definizione entrambi i 
numeri che vogliamo confrontare devono essere diversi da \emph{zero}.
Cioè nessun numero diverso da zero può essere considerato indistinguibile 
da zero:
\[\nexists~x \neq 0 ~|~x \sim 0\]
\end{osservazione}

Applicazioni della funzione parte satndard e della relazione di 
indistinguibiltà si possono vedere nell'intervento sul calcolo dei limiti 
presentato in questo stesso convegno \cite{zambelli2}.

\section{Quantità assolutamente trascurabili}
\label{sec:iperr_ferro}

\emph{Dopo aver cercato di dare un'idea di come possono essere proposti i 
numeri iperreali, di come funzionano e di quali sono i concetti chiave, 
presento la premessa proposta dal 
prof. Ruggero Ferro}\footnote{Ruggero Ferro, Università di Verona}

\subsection*{Quantità trascurabili}
   
Quand’è che una quantità o un numero possono essere considerati 
trascurabili?
In qualsiasi campo scientifico o tecnico, le quantità che sono tanto piccole 
da non poter essere apprezzate neppure con i più sofisticati strumenti di 
misura vengono generalmente trascurate.
Si accetta così di commettere un errore (trascurabile) che è la differenza 
tra il valore effettivo di una quantità e quello che le viene attribuito 
trascurando ciò che non è rilevabile.
Però, sommando quantità apprezzate a meno di un errore trascurabile, anche 
l’errore complessivo si somma e può succedere che dopo alcune aggiunte 
l’errore non sia più trascurabile.
Questa difficoltà mostra come ciò che può essere ritenuto trascurabile in un 
certo contesto, non lo è più allargando l’orizzonte: cioè la nozione 
proposta di trascurabile è relativa.

\subsection*{Quantità assolutamente trascurabili}
  
Si può pensare a qualcosa di assolutamente trascurabile, trascurabile in 
ogni situazione? Che caratteristiche deve avere?
Se una quantità fosse assolutamente trascurabile sommata con se stessa un 
numero arbitrario di volte dovrebbe rimanere assolutamente trascurabile. 
Cioè se \(\Delta\) è assolutamente trascurabile, allora anche 
\(n \cdot \Delta\) deve essere assolutamente trascurabile qualsiasi sia il 
numero naturale non nullo \(n\).
Inoltre, poiché la quantità unitaria non è trascurabile, \(\Delta\) sarà 
minore di \(1\) e anche \(n \cdot \Delta\) sarà minore di \(1\), da cui 
segue che \(\Delta\) dovrà essere minore di \(1 / n\) qualsiasi sia \(n\) 
naturale non nullo. 
Una quantità minore di \(1 / n\) qualsiasi sia \(n\) naturale non nullo 
viene detta un infinitesimo, sicché a una quantità assolutamente 
trascurabile corrisponderà un infinitesimo. 

\subsection*{Esistenza}
    
Ci sono numeri infinitesimi?
Questa è una domanda che ha senso solo in una filosofia platonico 
aristotelico scolastica in cui ci sono solo oggetti, e si pretende che anche 
i numeri siano particolari oggetti o nel mondo delle idee o enti astratti 
(ma cosa vuol dire astratti?).
Questa domanda non ha senso se si pensa che molti concetti possano essere 
costruiti dagli umani.
Il camminare mio o di altri sono azioni ben concrete e sperimentabili, 
sicché è chiaro cosa si vuole dire con la parola passeggiata (passeggiata 
faticosa, passeggiata breve), ma questa parola non indica nessun oggetto 
anche se linguisticamente è trattata come i nomi di oggetti.
Analogamente, un numero naturale è un modo standardizzato per confrontare 
quantità di elementi in insiemi diversi, e non certo un oggetto.

\subsection*{Costruzioni mentali}
    
Così anche un numero infinitesimo non è un oggetto, ma è una costruzione 
mentale per indicare quantità minori di \(1 / n\) per ogni naturale non 
nullo n.
Come tutte le costruzioni mentali, non ha senso chiedersi se gli 
infinitesimi sono particolari oggetti: sono costruzioni mentali che possono 
essere accettabili o non accettabili se non portano o portano a 
contraddizioni. 
E’ stato dimostrato che il sistema numerico che include gli infinitesimi non 
porta a contraddizioni se e solo se non portano a contraddizione gli altri 
sistemi numerici a partire da quello dei numeri naturali.
Piuttosto possono essere più o meno opportune, se servono o meno a trattare 
problemi facilitando il conseguimento di risultati rilevanti.

\subsection*{Servono le quantità trascurabili?}
    
Non solo le quantità assolutamente trascurabili vanno indicate da 
infinitesimi, ma anche gli infinitesimi sono quantità assolutamente 
trascurabili: somme di infinitesimi danno un infinitesimo, come si dimostra 
facilmente.
Se le quantità assolutamente trascurabili, inclusi gli infinitesimi, vanno 
trascurate, a che serve considerarle?
Certo, somme di assolutamente trascurabili sono trascurabili, ma non è detto 
che lo siano i loro rapporti: un infinitesimo diviso se stesso dà 1 che non 
è un trascurabile. 
Inoltre i rapporti tra assolutamente trascurabili si presentano naturalmente 
nello studio di molti fenomeni sperimentati quotidianamente come, ad 
esempio, la velocità istantanea e la pendenza di una salita in un suo 
punto.

\subsection*{Velocità istantanea}
   
Esaminiamo cosa si intende per velocità e per velocità istantanea.
Certamente è più veloce chi percorre una maggiore distanza in minore tempo, 
sicché la velocità è la distanza percorsa diviso il tempo per percorrerla. 
Questa è la velocità, che può essere detta media, perché lungo il percorso 
la velocità, chiamiamola istantanea, può essere variata da momento a 
momento.
La velocità istantanea è una grandezza fisica di cui si ha percezione (ci si 
accorge momento per momento di correre più o meno velocemente), e può essere 
rilevata con appositi strumenti che misurano la forza centrifuga che dipende 
dalla velocità istantanea. 
Si pensa che la velocità istantanea possa essere sempre meglio approssimata 
considerando la velocità media in intervalli di tempo sempre più piccoli, ma 
la velocità media è ben altro rispetto alla velocità istantanea.  
Per cogliere la velocità istantanea di un corpo in movimento non è 
sufficiente sapere in quale posizione esso si trova in un certo momento o in 
ciascun momento del suo moto: l’essere in (il passare per) un punto non ci 
dice quale sia la velocità istantanea in quel momento. 
Guardando al movimento come successione delle posizioni occupate non si può 
cogliere la velocità.
Eppure in ogni momento quel corpo deve avere una velocità istantanea 
altrimenti starebbe là per sempre.
Se pensiamo che un istante non abbia durata zero, ma si decide di entrarvi 
dentro attribuendogli una durata che includa solo infinitesimi di tempo, ha 
senso considerare lo spostamento del corpo in un infinitesimo di tempo e il 
rapporto (che non è detto sia trascurabile) tra questo spostamento e 
l’infinitesimo di tempo impiegato per percorrerlo.
A partire dal punto scelto, è ancora una velocità media (seppure in un 
infinitesimo di tempo) velocità media che potrà differire al cambiare 
dell’infinitesimo considerato (una volta un infinitesimo, un’altra volta il 
suo doppio ad esempio). Però può succedere che tutte queste velocità medie, 
qualsiasi sia l’infinitesimo di tempo considerato, differiscano tra loro e 
da un numero reale di quantità trascurabili (infinitesime). 
In tal caso, ha senso trascurare il trascurabile e considerare la quantità 
che rimane come la velocità istantanea di quel corpo in quel punto (diversa 
dalla velocità media in un intervallo anche infinitesimo di tempo).
Non sempre si è in questo caso: può succedere che la velocità media, a 
partire da un punto, durante un infinitesimo di tempo non sia infinitamente 
vicina a quella durante un altro infinitesimo. 
Questa diversa situazione occorre in moti che saltano da un punto all’altro 
o che cambiano bruscamente modo di procedere come quando c’è un urto. Ma 
allora è corretto affermare che in quel momento non c’è velocità istantanea.

Considerazioni del tutto analoghe si possono fare per la pendenza in un 
punto, che non è la pendenza media, o nel corrispondente problema di trovare 
la tangente ad una curva in un punto distinguendola accuratamente dalla 
secante, e in altri casi in cui si considerano tassi di variazione. 

\subsection*{Conclusione}
      
Ripensando a quanto fatto, si può osservare che l’introduzione di quantità 
trascurabili ha permesso di
\begin{enumerate}
 \item Analizzare fenomeni in cui ci sono grandezze su scale completamente 
diverse: tempi percepiti e intervalli di tempo all’interno di un istante.
 \item Considerare rapporti non trascurabili tra le grandezze: velocità 
media relativa a un certo incremento infinitesimo all’interno di un istante.
 \item Trascurare le differenze trascurabili tra i vari risultati dipendenti 
da tutti i possibili infinitesimi considerabili nella situazione in esame 
per giungere a caratterizzare la grandezza che si voleva determinare: la 
velocità istantanea nel caso visto.
\end{enumerate}

Perciò le quantità trascurabili possono essere molto utili, e vale la pena 
considerarle, se vengono ben precisate in modo che con esse si possa operare 
con semplicità ed efficacia. 
Ciò è esattamente quanto viene proposto dall’analisi non standard.


\begin{thebibliography}{10}
\phantomsection
\addcontentsline{toc}{section}{Bibliografia}

\bibitem[AAVV 2019]{zambelli1} 
\textsc{Daniele Zambelli} (a cura di), 
\emph{Matematica Dolce 3}, \\
\url{www.matematicadolce.eu}

\bibitem[Zambelli, 2019]{zambelli2}
\textsc{Daniele Zambelli}, 
Limiti e analisi non standard, 
in \emph{questi atti},

% \bibitem[Ferro, 2015]{ferro1}
% \textsc{Ruggero Ferro}, 
% Ritorno all'analisi infinitesimale, 
% in \emph{V Giornata di studio ``Analisi non standard per le scuole 
% superiori''}, atti,
% 2015, Matematicamente.it

\end{thebibliography}


\chapter[Il concetto di continuità nel curricolo liceale]
{\LARGE Il concetto di continuità nel curricolo liceale}
\begin{center}
 Pietro Cacciatore - Liceo Tito Livio, Padova
\end{center}
\vspace{1cm}
{\em La formazione matematica degli studenti, e più in generale di una persona 
colta, dovrebbe fondarsi oltre che su specifiche nozioni e singoli argomenti, 
anche sui temi che hanno attraversato la storia della disciplina. 
È questo il 
caso del concetto di continuità che, a ben vedere, appare in modo più o meno 
esplicito, a seconda degli argomenti, lungo l'intero curricolo liceale. 
È possibile allora usarlo come una sorta di filo conduttore, richiamandolo alla 
mente degli studenti ogni volta si presenti l'occasione.
}

\section{Introduzione}
Uno dei temi del convegno è la continuità, ho pensato allora di proporre agli 
organizzatori un percorso, sull'intero quinquennio del liceo, fondato su 
argomenti che fossero legati a questo concetto, nella convinzione che sia utile 
per la formazione degli studenti una visione il più possibile unitaria della 
disciplina. 

Lasciatemi aprire con una similitudine alla quale sono affezionato. 
Possiamo paragonare l'insegnamento della Matematica a un albero: le radici sono 
quello che un insegnante conosce della sua disciplina ma che non dirà mai ai 
suoi studenti, come ad esempio aspetti troppo tecnici per trovare posto in un 
curricolo; il tronco e le branche principali rappresentano gli aspetti fondanti 
della disciplina come la storia, le teorie, l'epistemologia; le foglie e i 
rametti sono le applicazioni e gli esercizi; resta da considerare l'apparato 
linfatico quello che trasporta la vita e fa crescere l'albero, e che credo si 
possa paragonare alle idee e ai concetti che attraversano la disciplina e la 
rendono in perpetua trasformazione. 

\section{Continuità e incommensurabilità}
È opportuno far risalire il concetto di continuità a quello di 
incommensurabilità. Come noto la scoperta provoca una crisi nella filosofia 
pitagorica. Prima ancora di mostrarne le conseguenze, vale la pena far capire 
agli studenti quello che questa crisi rimetteva in discussione. 

Per i primi 
pitagorici Matematica e Fisica avevano lo stesso statuto epistemologico: 
entrambe spiegavano come era fatto il Mondo. Aristotele nella 
{\em Metafisica}\footnote{Aristotele, {\em Metafisica}, citato in 
\cite[p. 38]{kline}.} afferma:
``{\em I Pitagorici pensano [\dots] che da numeri sono composte le sostanze 
percepibili}''. E uno dei maggiori storici della matematica del Novecento, 
Kline, 
si esprime in un modo che non potrebbe essere più esplicito: {\em I numeri erano
per loro} [i pitagorici] {\em quello che per noi sono gli atomi}
\footnote{Ibidem.}.
Come conseguenza necessaria di questo punto di vista, i pitagorici assumevano
a fondamento della loro dottrina il {\em Principio primo: tutto è numero e 
rapporto fra numeri} (naturali). 
E da qui operavano alla solita maniera (o almeno quella che dopo la 
formalizzazione di Aristotele negli {\em Analitici Secondi}, è diventata 
``la solita maniera''; formalizzazione, è da ricordare, fatta a posteriori, 
come spesso 
succede agli epistemologi): spiegare un fatto o un fenomeno significa far 
diventare quel fatto o fenomeno una conseguenza logica dei principi primi 
assunti a fondamento della teoria scientifica. Nella loro ricerca della 
conoscenza il principio era diventato fonte di molte conquiste. Aveva permesso 
di spiegare, ad esempio, come era fatta la musica; di comprendere cioè perché 
certi suoni se percepiti assieme, o uno di seguito all'altro, risultassero 
gradevoli. 
Ebbene la ragione profonda per cui, ad esempio, due corde di lunghezza una 
doppia dell'altra emettono suoni consonanti risiede, per i pitagorici, nel fatto 
che il rapporto fra le loro lunghezze, o le frequenze delle vibrazioni sonore, è 
un {\em rapporto fra numeri interi}. 
Ad esempio la frequenza del Do1 sta a quella del Do2 come 1 sta a 2. 
Lo stesso succede per due suoni le cui frequenze stanno come 3 sta a 2, 
intervallo di terza, e come 4 sta a 3, intervallo di quarta. 
Partendo poi da una certa nota, ad esempio il Do, e procedendo di terza in 
terza, si ottiene l'intera gamma dei sette toni ed anche dei cinque 
semitoni.\cite[p. 131]{odifreddi05}
\begin{figure}[h]
 \centering
\includegraphics[width=0.8\textwidth]{chap/01_interventi/04_cacciatore/fig01}
 \caption{\footnotesize La somma di due pari è un pari, e la somma di un pari 
con un dispari è un dispari.}
 \label{continuita:f1} 
 \end{figure}

Una delle prime conquiste dei pitagorici fu lo studio dei numeri pari e 
dispari, essi ne davano una interpretazione geometrica\footnote{
Ma più che una interpretazione si trattava del loro modo di intendere i numeri
alla luce del loro Principio primo.}: 
due catene affiancate di punti uguali per i pari, e un punto in più, collocato
a cavallo delle righe, per i dispari. Si ottengono interessanti dimostrazioni 
grafiche che potremmo definire ``senza parole'' (figura \ref{continuita:f1}).
L'invenzione dei numeri poligonali è interamente dovuta alla Scuola pitagorica.
Anche in questo caso le dimostrazioni sono di tipo grafico 
(figura \ref{continuita:f2})
\cite[p. 130]{ghersi} \cite[p. 64]{boyer}.
\begin{figure}[h] %(figura 2)
 \centering
\includegraphics[width=0.6\textwidth]{chap/01_interventi/04_cacciatore/fig02}
 \caption{\footnotesize La somma di due numeri triangolari consecutivi è un
  numero quadrato; la differenza di due quadrati è un dispari; la somma dei 
  primi n pari è il doppio di un numero triangolare.}
 \label{continuita:f2} 
 \end{figure}
 
Tornando all'incommensurabilità è importante attirare l'attenzione degli 
studenti sul fatto che questo concetto è contro intuitivo: è naturale, infatti, 
pensare che date due grandezze, sarà sempre possibile trovarne una terza 
sufficientemente piccola da stare un numero di volte intero in entrambe. Che 
non si possa riuscire sembra impossibile. 
È uno di quei casi in cui c'è uno scarto fra la nostra intuizione e la forza di 
una dimostrazione. 

Ogni volta che si incontrano momenti del genere, vale la pena enfatizzarli: ne 
sono esempi la possibilità di geometrie non euclidee, la corrispondenza 
biunivoca tra i naturali e i loro quadrati,\dots La cosa accade anche per la 
Fisica, sia pure in un contesto epistemologico alquanto diverso: la sfericità e 
i moti della Terra, il legame fra elettricità e magnetismo, lo spazio inteso 
come oggetto fisico e non come contenitore,\dots 

Per evitare malintesi, sarà bene non limitarsi alla incommensurabilità fra la 
diagonale del quadrato e il suo lato, ovvero, passando alla loro misura, 
all'irrazionalità della sola \(\sqrt{2}\), ma estendere la dimostrazione ad 
altri numeri che non siano quadrati perfetti. 
Ho trovato prezioso a questo fine un piccolo ma illuminante libro di Niven, 
\emph{Numeri razionali e irrazionali} \cite{niven}. 
In esso si trovano anche le indicazioni per dimostrare l'irrazionalità di ampie 
classi di numeri; o per risultati più generali, come ad esempio il fatto che 
l'insieme degli irrazionali non è chiuso rispetto alle quattro operazioni 
aritmetiche, affrontando in un contesto più ricco il concetto di chiusura di un 
insieme rispetto a certe operazioni; o ancora per prevedere a cosa conducono 
operazioni fra razionali e irrazionali \cite[p. 55]{niven}.
È istruttivo per gli studenti venire a conoscenza di certi fatti relativi ai 
razionali, come ad esempio che la periodicità non è per un numero un carattere 
assoluto, a differenza della primalità, ma legato alla base in cui è scritto. A 
questo proposito vorrei aggiungere che dimostrare cose ben note dagli anni 
della Scuola Media o Elementare, come ad esempio i criteri di divisibilità o la 
frazione generatrice di un numero periodico, fa parte di una strategia che 
tende a suscitare negli studenti spirito critico e curiosità. 

I greci reagirono all'incommensurabilità rifugiandosi nella geometria, in 
particolare esaltando, forse eccessivamente, le costruzioni con riga e 
compasso. 
Si tratta di un vero e proprio \emph{cambio di paradigma}. 
Si possono presentare agli studenti costruzioni di irrazionali come le radici di 
interi con, ad esempio, 
la Spirale di Teodoro di Cirene (V secolo a. C.), o la costruzione della radice 
quadrata, o quarta, di un segmento di misura generica utilizzando i teoremi di 
Euclide (figura \ref{continuita:f3}) \cite[p. 173]{ghersi}.
\begin{figure}[h] %(figura 3)
 \centering
\includegraphics[width=0.8\textwidth]{chap/01_interventi/04_cacciatore/fig03}
 \caption{\footnotesize A sinistra la Spirale di Teodoro di Cirene. A destra 
la costruzione della radice quadrata di a, non è difficile, a partire da questa 
figura costruire le radici quarte, ottava,\dots di a.}
 \label{continuita:f3} 
\end{figure}
 
E ancora immaginare costruzioni legate alla storia dell'Arte, come il segmento 
di misura \(\sqrt{3}\) ricavato all'interno della ``Mandorla'', 
una figura ottenuta da una opportuna intersezione fra i due cerchi, e 
largamente utilizzata nell'iconografia medievale\footnote{Argomenti, fra 
l'altro, che possono aiutare gli studenti ad affrontare certi tipi di quesiti 
Invalsi, senza che al momento della somministrazione essi percepiscano un senso 
di estraneità. 
A questo proposito, e sia detto senza polemica, non ho mai capito perché certi 
risultati annoverati dagli autori dei libri di divulgazione o dai relatori alle 
conferenze  tra le ``bellezze della matematica'', non possano far parte di un 
normale curricolo liceale.}. 
O ancora le diverse costruzioni della parte aurea di un segmento, come quelle 
che si possono immaginare studiando le straordinarie proprietà del numero 
aureo \cite{posamentier}.
Contestualmente si farà notare agli studenti come, nel frattempo, nel mondo 
greco sia cambiato significativamente lo statuto epistemologico della 
matematica: gli enti matematici non sono più intesi come oggetti concreti, 
addirittura alla base delle cose del Mondo, ma elementi che cominciano a 
diventare sempre più astratti. Per Euclide, ad esempio, reale è tutto ciò che è 
costruibile con riga e compasso.

\section{Continuità e Teorema di Talete}

Fortemente collegato al concetto di continuità è il Teorema di Talete, nel 
caso, spesso trascurato, dell'incommensurabilità.
\begin{figure}[h] %(figura 4)
 \centering
\includegraphics[width=0.5\textwidth]{chap/01_interventi/04_cacciatore/fig04}
 \caption{\footnotesize Dopo aver dimostrato 
che a segmenti uguali sulla trasversale \(r\) corrispondono segmenti uguali su 
\(t\), si mostra, iterando il segmento unitario \(u\) e suoi sottomultipli, che 
si possono ottenere approssimazioni sempre migliori del segmento \(AB\), e 
rispettivamente \(A'B'\), onde poter giungere all'uguaglianza dei rapporti 
\(AB/BC\) e \(A'B'/B'C'\).}
 \label{continuita:f4} 
 \end{figure}
 
Si parte dal lemma: \emph{Dato un fascio di rette parallele tagliate da due 
trasversali, a segmenti uguali su una trasversale corrispondono segmenti uguali 
sull'altra}, 
per costruire, con un sottile ragionamento, l'uguaglianza dei rapporti tra 
grandezze incommensurabili\cite[p. 61]{niven} (figura \ref{continuita:f4}).

\section{Continuità e quadratura del cerchio}

Altro argomento da enfatizzare nell'ambito della comprensione del concetto di 
continuità è la quadratura del cerchio. Prima di tutto è importante far 
comprendere agli studenti cosa vuol dire \emph{quadrare una figura geometrica}. 
Per farlo, con la speranza di qualche probabilità di successo, si dovrà partire 
dalla quadratura con riga e compasso dei poligoni \cite[p. 95]{odifreddi10}, 
argomento dove non mancano occasioni che richiedono una certa ingegnosità. 
Affascinante risulta poi la quadratura delle lunule ad opera di Ippocrate di 
Chio, non solo per l'originalità del percorso, ma anche per dare un'idea del 
fermento intellettuale di un periodo relativamente così arcaico \cite[p. 
77]{boyer} 
\cite[p. 99]{odifreddi10}. 
Si può partire dal teorema: 
\emph{Due cerchi stanno tra loro come i quadrati costruiti sui rispettivi 
diametri}, 
del quale si dà solo un argomento di plausibilità un po' per motivi storici, un 
po' per risparmiare agli studenti una dimostrazione troppo lunga. Probabilmente 
Ippocrate si limitò a fare solo una congettura suggerita dalla constatazione 
che aumentando indefinitamente il numero dei lati del poligono regolare 
iscritto (ad esempio raddoppiandoli successivamente) il poligono ``tende ad 
approssimarsi'' al cerchio. Euclide dimostra rigorosamente il teorema nel Libro 
XIII, usando il metodo di esaustione, del quale, purtroppo, che io sappia, non 
esiste una trasposizione didattica. 
Come fondamentale ricaduta di questo teorema, Ippocrate riesce ad ottenere la 
prima rigorosa quadratura di una superficie curvilinea: la lunula costruita sul 
lato del quadrato, cioè a trovare con riga e compasso un quadrato di area 
uguale a quella di una figura curvilinea. 

Ma è con la quadratura del cerchio che diventa improcrastinabile, dal 
punto di vista didattico, l'introduzione dell'assioma di continuità. Per 
preparare gli studenti all'assioma, che, fra l'altro, dovrebbe essere alla base 
di una corretta definizione del termine \emph{continuo} nel linguaggio naturale, 
è bene preliminarmente fare chiarezza sui termini \emph{discreto} e 
\emph{denso}, marcando le differenze fra gli insiemi \(\N\), \(\Q\), \(\R\) alla 
luce di questi due concetti. 
Risultati facili da ottenere, ma non per questo banali, sono la densità di 
\(\Q\) e la non densità di \(\N\), anzi si può dimostrare che \(\N\) soddisfa la 
definizione di insieme discreto. 
Così come non è difficile mostrare che in \(\Q\) esistono situazioni in cui sono 
presenti dei ``buchi''. 
\[A=\graffa{x~|~2 \leqslant x < 2,5 \quad o \quad
                2,5 < x  \leqslant 3,~x \in \Q}\]
\[B=\graffa{x~|~2 \leqslant x < \sqrt{7} \quad o \quad
               \sqrt{7} < x \leqslant 3,~x \in \Q}\]
In entrambi gli insiemi c'è una lacuna (una interruzione), ma nel primo caso è 
possibile colmarla utilizzando elementi di \(\Q\), nel secondo non si può fare.
% \begin{figure}[h] %(figura 5)
%  \centering
% \includegraphics[width=1\textwidth]{chap/01_interventi/04_cacciatore/fig05}
%  \caption{\footnotesize In entrambi gli insiemi c'è una lacuna (una 
%   interruzione), ma nel primo caso è possibile colmarla   utilizzando elementi 
%   di \(\Q\), nel secondo non si può fare.}
%  \label{f5} 
% \end{figure}
 
Sarà bene far notare agli studenti che le difficoltà che si erano incontrate 
nel caso dell'incommensurabilità parlando dell'impossibilità di una unità di 
misura ``sufficientemente piccola'', si ripresentano per un insieme, geometrico 
o numerico, che sia ``solo'' denso. Anche in questo caso l'intuizione non ci è 
di alcun aiuto: sembra impossibile che in un insieme ordinato dove si possa 
sempre inserire un elemento fra altri due, si debbano avere dei ``buchi'' o 
``lacune'' o ``soluzioni di continuità''. Ma bisogna trovare le parole per 
raccontare agli studenti la continuità, e quale può essere il modo migliore se 
non far parlare i protagonisti di questa vicenda? 

\begin{quoting}
In che cosa consiste dunque propriamente questa continuità? Con vaghe 
osservazioni sulla connessione ininterrotta delle parti più piccole non si 
chiarisce alcunché, si tratta di indicare invece una precisa caratteristica 
della continuità che possa servire da base ad argomentazioni realmente valide. 

Vi ho riflettuto a lungo invano, ma finalmente ho trovato ciò che andavo 
cercando. Ogni punto di una retta produce una separazione della stessa in due 
parti, così che ciascun punto dell'una giace a sinistra di ciascun punto 
dell'altra. 

Io trovo l'essenza della continuità nel principio inverso: dividiamo 
tutti i punti della retta in due classi, di modo che ogni punto della prima si 
trovi a sinistra della seconda: esisterà allora uno e un solo punto che 
determina questa partizione. 

Come già osservai, ritengo indubitabile che tutti riconosceranno la verità di 
questo principio; qualche lettore potrà anzi rimanere stupito che proprio una 
proposizione così banale possa scoprirci il segreto del concetto di continuità.

Io sono però ben contento che tutti lo trovino tanto intuitivo e in così grande 
accordo con la rappresentazione comune di linea, dato che né io né altri 
potremo mai trovarci in grado di dimostrarlo. 
Esso infatti non è altro che un assioma; ed è proprio accettando questo assioma 
che noi attribuiamo alla linea la sua struttura continua.
\cite[p. 217]{dedekind}.
\end{quoting}

Nel leggere le parole di Dedekind si tratta di far rivivere, in qualche modo, 
agli studenti il travaglio intellettuale di quanti cercavano di trovare, ognuno 
con la propria sensibilità, oltre alle parole per esprimere la continuità anche 
un modo efficace per produrre dimostrazioni. Non è ozioso ricordare che persino 
geni assoluti come Aristotele \cite{evans}, Galileo, Newton, Leibniz\dots si 
erano confrontati con questo concetto senza riuscire a trovare soluzioni, 
alcuni addirittura confondendo la continuità con la densità. 
Forse non è superfluo notare che verso la fine di questo brano emerge la 
consapevolezza delle necessità di una visione assiomatica, che da lì a poco 
giocherà un ruolo importante sia in campo geometrico, sia in campo numerico.

% \begin{figure}[h] %(figura 6)
%  \centering
% \includegraphics[width=0.5\textwidth]{chap/01_interventi/04_cacciatore/fig06}
%  \caption{}
%  \label{f6} 
% \end{figure}
\begin{wrapfloat}{figure}{R}{0pt}
%  \centering
\includegraphics[width=0.5\textwidth]{chap/01_interventi/04_cacciatore/fig06}
   \caption{}
   \label{continuita:f6}
 \end{wrapfloat}
Tornando alla ricerca dell'essenza della continuità ci si può esprimere così: 
se ho una retta e la spezzo in due, trovo da una parte un ``buco'' e dall'altra 
un ``tappo''. 
Questa immagine, che sempre mi rimanda alla prima volta che l'ho 
ascoltata, la devo al Professor Ferro dalle cui lezioni ho tratto molto della 
mia ri-formazione matematica, avvenuta perlopiù in anni molto posteriori alla 
laurea. \\
Immediatamente si può ora dimostrare che né \(\N\) né \(\Q\) sono continui. 
Si può anche ritornare a teoremi di cui non si era potuta, a suo tempo, dare 
dimostrazione, ad esempio l'intersezione di un segmento con un circonferenza. 
%\vspace{-2mm}
Osservando la figura \ref{continuita:f6} si può notare che le intersezioni del segmento 
\(AB\) con le regioni \(\alpha\) e \(\beta\) rispettivamente dividono il 
segmento stesso in due parti complementari e separate. 
Risultano così soddisfatte condizione dell'assioma di continuità ed è dunque 
assicurata l'esistenza del punto \(P\) che sarà o ultimo punto del segmento 
\(AP\) o primo punto del segmento \(PB\).

\begin{wrapfloat}{figure}{R}{0pt}
%  \centering
\includegraphics[width=0.5\textwidth]{chap/01_interventi/04_cacciatore/fig07}
   \caption{}
   \label{continuita:f7}
 \end{wrapfloat}
Così come per Dedekind anche per Cantor la genesi della formulazione di un 
assioma che cogliesse lo spirito della continuità deve essere stata 
profondamente travagliata. Le obiezioni di Reymond qui riportate (si segua 
sulla figura \ref{continuita:f7}) bene testimoniano dello straordinario momento vissuto 
dalla Matematica degli anni '70 e '80 dell'Ottocento. 
% \vspace{-5mm}
% \begin{figure}[h] %(figura 7)
%  \centering
% \includegraphics[width=0.5\textwidth]{chap/01_interventi/04_cacciatore/fig07}
%  \caption{}
%  \label{f7} 
% \end{figure}

% \vspace{-5mm}
\begin{quoting}
\noindent Se da un segmento ne stacchiamo un altro, da questo un terzo, ecc., 
otteniamo sempre un segmento: come potrà dunque da una serie siffatta venire 
originato un punto? 
L'abisso tra segmento e punto sembra rimanere sempre il medesimo. Grande 
o piccolo che sia  il segmento rimane sempre un intervallo compreso tra due 
estremi. Se noi tutt'a un tratto sostituiamo, senza ragione logica, il punto al 
segmento, compiamo con ciò un atto arbitrario\dots O gli estremi si trovano 
separati da un segmento, oppure coincidono formando un unico punto; uno stato 
intermedio non esiste.
\end{quoting}

Esse compaio con grande risalto in due preziosi libri di divulgazione dovuti a 
Geymonat \cite[p. 269]{geymonat} e Weismann rispettivamente \cite[p. 
201]{waismann}. Naturalmente le considerazioni di  
Reymond conducono in modo naturale, col senno di poi, alla formulazione 
dell'assioma nel senso di Cantor, con le parole di Geymonat: 
\begin{quoting}
In questo caso infatti tali successioni danno luogo a segmenti sempre più 
piccoli che vanno gradualmente riducendosi ad un solo ed unico punto. ``Ecco 
allora trovata la risposta! – esclamerà qualcuno. – Proprio quest'ultimo punto 
rappresenta l'elemento di separazione''[ \dots ] Reymond aveva perfettamente 
ragione; 
per giungere alla esistenza dell'elemento di separazione, bisogna far ricorso, 
non ad una dimostrazione, ma ad un assioma: l'assioma di continuità.
\end{quoting}

Anche in questo caso sarà importante far comprendere agli studenti la natura 
assiomatica di certe affermazioni. 

Si può dimostrare che le due formulazioni dell'assioma di continuità sono 
logicamente equivalenti, anche se per la verità l'implicazione da Cantor a 
Dedekind richiede l'Assioma di Archimede\footnote{Per una trasposizione 
didattica vedi: \cite[p. 107]{agazzi}}. 
Ci troviamo così in presenza di un 
caso di scoperta simultanea, che conduce a una interessante riflessione 
epistemologica: se programmi di ricerca diversi giungono alla stessa 
conclusione, allora ci dev'essere una certa intersoggettività nella conoscenza 
scientifica (o dovremmo parlare di oggettività?). 

Quando si introduce un assioma, scatta sempre la domanda: chi ci assicura che 
debba essere introdotto proprio in quel momento? 
Nello specifico chi ci assicura che, ad esempio, per l'intersezione fra retta e 
circonferenza non basti la densità (ovvero l'assioma di separazione del piano) 
e occorra proprio la continuità? 
La risposta si trova nella teoria dei modelli: basta costruire due modelli uno 
fondato sui numeri razionali Mod(\(\Q\)) ed uno sui reali Mod(\(\R\)). 
E considerare il caso delle intersezioni delle \emph{rette}  
\(y = x + 1\) e \(y = x\) 
con la \emph{circonferenza} \(x^2 + y^2 = 1\)\footnote{Non sarebbe 
completamente corretto dire: ``la retta di equazione \(y = x + 1\)'', poiché è 
l'equazione stessa che è la retta nei due modelli!}. 
In Mod(\(\Q\)) la prima retta interseca la circonferenza 
in \(\punto{-1}{0}\) e \(\punto{0}{1}\), essendo le soluzioni razionali le 
due coppie di numeri risultano \emph{punti} del modello, mentre la retta 
\(y = x\) attraversa la circonferenza senza intersecarla poiché le soluzioni del 
sistema hanno almeno una coordinata irrazionale, e dunque quelle coppie di 
numeri non sono punti di Mod(\(\Q\)). 

Quasi a voler sottolineare la straordinaria importanza di questi fatti, si 
scopre che addirittura il primo teorema del Primo libro degli \emph{Elementi} 
(costruzione del triangolo equilatero) necessita di revisione: nella 
dimostrazione di Euclide il passo 4 non ha giustificazione (si veda la figura 
\ref{continuita:f8}). 

\begin{figure}[h] %(figura 8)
 \centering
\includegraphics[width=1\textwidth]{chap/01_interventi/04_cacciatore/fig08}
 \caption{}
 \label{continuita:f8} 
\end{figure}

Questa circostanza si inserisce nel contesto più ampio della questione 
dell'incompletezza del sistema assiomatico di Euclide. 

È ora possibile, finalmente, quadrare il cerchio. Costruiti i due insiemi dei 
poligoni inscritti e circoscritti la circonferenza, basta dimostrare che essi 
formano due classi contigue. Sono così verificate le condizioni dell'assioma di 
Cantor. Per sottolineare l'importanza del risultato è bene proporre alcune 
attività. Intanto si può cominciare col trovare una grossolana approssimazione 
di \(\pi\) partendo da un cerchio di raggio unitario e dai quadrati iscritti e 
circoscritti, certo dal punto di vista dell'approssimazione di \(\pi\) non è 
molto, ma da quello della comprensione di questo difficile e intricato 
argomento è un bel passo avanti!\footnote{Ho sempre trovato questa sorta di 
provocazione un modo per far notare, agli studenti, che si tratta sempre e 
comunque di approssimazioni.} 
Ma poi si può fare di meglio, ottenendo approssimazioni sempre migliori di 
\(\pi\) a partire dalle formule dei lati dei poligoni regolari iscritti e 
circoscritti o da formule più generali; insomma, mettendosi sulle orme di 
Archimede. Va notato che ricavare formule come queste non solo è un ottimo 
esercizio di geometria piana e sui radicali,  ma ha anche il pregio di essere 
inserito in un contesto significativo. 

Come ho già detto l'insegnamento della matematica può contribuire a 
perfezionare e arricchire il vocabolario degli studenti. 
L'espressione ``questo problema è impossibile come quadrare il cerchio'', ad 
esempio, spesso fonte di fraintendimenti, può essere ora chiarita utilizzando 
una nobile citazione: 

\begin{verse}
Dentro da sé del suo colore stesso,\\
mi parve pinta della nostra effige;\\
per che il mio viso in Lei tutto era messo.

Qual è il geomètra che tutto s'affige\\
per misurar lo cerchio, e non ritrova,\\
pensando, quel principio ond'elli indige,

tal era io a quella vista nova: \\
(Par, XXXIII, 130- 136)
\end{verse}

È piuttosto evidente che Dante non si riferisce all'impossibilità senza 
specificazioni,  ma alla imposizione di farlo con riga e 
compasso\footnote{Forse non è superfluo notare che Dante sottolinea spesso i 
momenti drammatici del Poema con similitudini ricavate dalle sue conoscenze 
matematiche o astronomiche.}.

\section{Continuità e infinito}

Una più approfondita comprensione del concetto di continuità emerge dal 
rapporto 
col concetto di infinito che si può affrontare già al terzo anno. Il carattere 
profondamente diverso fra \(\Q\) ed \(\R\) risalta in modo lampante se si pensa 
alla 
cardinalità dei due insiemi, e già da questo risultato appare quanto sia 
straordinario il ruolo della continuità. Ma se si fa una breve incursione fra i 
numeri algebrici e trascendenti \cite[p. 72]{niven}, si può fare di più. Con 
pochi sforzi  si riesce, senza dover approfondire una materia molto 
ostica\footnote{Per restare nella similitudine dell'albero è sicuramente questo 
un argomento da annoverare fra le radici, ma a volte parti di radici affiorano 
ed è possibile intravedere qualcosa.}, a mostrare che i numeri algebrici hanno 
la cardinalità del numerabile\footnote{È sufficiente associare ai coefficienti 
delle equazioni di grado n le \(n-uple\) in \(\Z\) e mostrare che per ogni 
\(n\) è possibile ordinare queste ultime.}, e dunque i numeri trascendenti hanno 
la cardinalità del continuo\footnote{La reale successione degli eventi storici 
la si può trovare in \cite[p. 969]{bottazzini}. 
In questo articolo si può anche trovare descritto il ruolo avuto dall'Assioma 
di continuità di Dedekind nel risolvere il secolare problema della ricerca di 
una definizione di funzione continua.}. 
Parlando in modo un po' approssimativo e certo molto impreciso, possiamo dire 
che gli studenti capiscono ora che l'operazione di estrazione di radice, che 
tanto aveva sconvolto la ragionevole e intuitiva visione pitagorica, era solo 
la fessura attraverso la quale penetrare in mondi straordinari e ricchi di 
fatti stupefacenti. 
Non sono le radici che fanno salire la cardinalità di \(\R\) ma i trascendenti! 
       
\section{Assiomatizzazione dei reali}

Al quinto anno gli studenti hanno tutti gli strumenti per intraprendere lo 
studio della teoria assiomatica dei numeri reali. Proporre la sistemazione 
assiomatica così avanti nei curricolo non dipende solo dal grado di maturazione 
degli studenti ma è anche un modo per rivivere il percorso storico: per gli 
scienziati del periodo, anni '70 e '80 dell'Ottocento si trattava di rimettere 
a posto proprietà di questi numeri che erano state trovate sin dal Seicento e 
che si continuavano a scoprire anche in quegli anni (come fa notare il 
Professor Lolli: prima si trovano le proprietà degli oggetti matematici e solo 
dopo si costruiscono i sistemi assiomatici \cite[p. 99]{lolli}.) Lo si può fare 
ad esempio utilizzando l'assiomatizzazione di Hilbert 
\cite[v. 2, p. 1155]{kline}.

Come è consuetudine ogni volta che introduciamo una teoria ci affrettiamo a 
ricercarne un modello. 
Esso è in parte già suggerito dallo studio del teorema di Talete nel caso 
dell'incommensurabilità e dal lavoro sulla quadratura del cerchio, ma, 
soprattutto, dalle riflessioni fatte al momento dell'introduzione dell'assioma 
di continuità. 
Sto parlando del modello fondato sulle sezioni di Dedekind \cite[p. 
220]{waismann}. 
Non è molto difficile verificare che tutti gli assiomi sono veri nel 
modello, dopo tutto le proprietà richieste sono tutte conseguenze delle 
proprietà dei razionali. Diverso è per l'assioma di continuità, si tratta di 
dimostrare che le sezioni in \(\R\) (sezioni i cui singoli numeri sono a loro 
volta 
sezioni!) non hanno ``buchi''. Anche se si tratta di una dimostrazione sottile, 
esistono libri di divulgazione che ne fanno una trasposizione didattica alla 
portata degli studenti, ad esempio lo stesso Waismann \cite[p. 227]{waismann}. 

\section{Altri tre problemi}

Passiamo infine ad altre tre questioni collegate al concetto di continuità, 
affrontate tra quarto e quinto anno. 
Euclide nel V libro degli \emph{Elementi} dà la famosa definizione di 
uguaglianza di rapporti fra grandezze incommensurabili, che quasi certamente 
prende da Eudosso. 
\begin{quoting}
(\emph{Elementi}, libro V, definizione V) Si dice che due grandezze \(A\), \(B\) 
sono nello stesso rapporto di altre due grandezze \(C\), \(D\) quando in 
qualunque modo si prendano due equimultipli \(nA\), \(nC\) della prima e della 
terza, e in qualunque modo si prendano due equimultipli \(mB\), \(mD\) delle 
altre due, a seconda che si abbia: 
\[nA>mB \quad \text{o} \quad nA<mB\]
si ha rispettivamente: 
\[nC>mD \quad \text{o} \quad nC<mD\]
nel caso sussista l'uguaglianza le grandezze sono commensurabili.
\end{quoting}

Frajese, nel suo magistrale commento a quest'opera \cite[p. 301]{euclide} 
afferma che questa definizione, 
``pur non considerando [Euclide] esplicitamente i rapporti come numeri'', 
coincide con quella che ne dà Dedekind. Infatti assumendo i rapporti 
come numeri si può affermare che: \(A\), \(B\) e \(C\), \(D\) hanno lo stesso 
rapporto se per 
\(A/B\) e per \(C/D\), per ogni \(m\) e per ogni \(n\), sussistono 
contemporaneamente le disuguaglianze che determinano le sezioni. 
Ma questo significa che le sezioni che individuano i due rapporti sono le stesse 
e dunque che i due rapporti sono lo stesso numero nel senso di Dedekind. 
Insomma, Euclide sembra aver scoperto per primo le sezioni di Dedekind. 
Naturalmente non è così. 
Eudosso ed Euclide circoscrivevano il rapporto fra grandezze incommensurabili 
alla sola proporzionalità, per loro non era definito il numero che esprime il 
rapporto fra due grandezze, ma solo l'uguaglianza dei rapporti fra quattro 
grandezze. 
E queste grandezze sono solo grandezze geometriche poiché il rapporto fra 
numeri è definito solo nel caso della commensurabilità, e non potrebbe essere 
altrimenti dal momento che per i greci, dopo il dramma dell'incommensurabilità, 
gli unici enti che potevano dirsi numeri erano i naturali. Pare proprio di 
trovarsi in presenza di un caso di \emph{metafisica influente}. 
Non è superfluo notare, poi, che Eudosso-Euclide facciano uso, quasi senza 
darlo a vedere, dell'infinito in atto.

\begin{wrapfloat}{figure}{R}{0pt}
% \begin{center}
\includegraphics[width=0.45\textwidth]{chap/01_interventi/04_cacciatore/fig09}
% \end{center}
\caption{\footnotesize Libro III, Proposizione 16: Tra le rette perpendicolari 
  al diametro in un suo estremo e il cerchio non è interposta alcuna retta.\\
  Libro III, Proposizione 16, Corollario: L'angolo formato dalla tangente e il 
  cerchio [angolo di contingenza] è minore di qualsiasi angolo rettilineo.}
 \label{continuita:f9} 
\end{wrapfloat}
La seconda questione da puntualizzare ci riporta alla dimostrazione del Teorema 
di Talete nel caso dell'incommensurabilità, in quell'occasione non si era detta 
tutta la verità: affinché la figura \ref{continuita:f4} abbia senso è necessario che 
partendo dal 
punto \(B\) e iterando il segmentino \(u\) prima o poi si possa andare oltre il 
punto \(A\), ma questa condizione è proprio l'assioma di Archimede. 
Ed è ancora l'assioma di Archimede che compare nella terza ed ultima questione 
da precisare. Le discussioni sull'esistenza degli infinitesimi hanno origini 
assai remote e se ne può trovar traccia addirittura negli \emph{Elementi}. 
Euclide distingue fra angoli i cui lati siano rette e non. La Proposizione 16 
del Libro III coinvolge in particolare l'angolo fra la circonferenza e una sua 
tangente in un punto (angolo di contingenza).
% \vspace{-1em}
% \begin{figure}[h] %(figura 9)
% \begin{minipage}{.48\textwidth} 
Euclide mostra che fra circonferenza e tangente non può interposti alcuna retta 
(figura \ref{continuita:f9}), 
e dunque che l'angolo di contingenza è sempre minore di un qualunque angolo 
rettilineo, insomma è un infinitesimo. 
Vale la pena dimostrare agli studenti il teorema per far loro toccare con mano 
un esempio concreto di infinitesimo in atto. 
E, anche, arricchire il loro vocabolario, soprattutto visto l'uso impreciso che 
si fa di questo termine nel linguaggio naturale. 
È possibile poi introdurre una misura per l'angolo di contingenza legata 
all'inverso del raggio del cerchio, e stabilire una relazione d'ordine \cite[p. 
98]{agazzi}: insomma, mostrare che questi infinitesimi si comportano come dei 
veri e propri numeri, che tuttavia non rispettano l'assioma di Archimede. 
Un'ultima riflessione epistemologica, e chiudo: 
la consueta definizione di angolo fra due curve, come angolo fra le tangenti, 
blocca l'apparire di infinitesimi in atto. 
È un modo per la teoria, nata dalla sistemazione di Weierstrass, di provvedere 
alla costruzione di una \emph{cintura protettiva} di lakatosiana memoria.
% \end{minipage}
% \hfill
% \begin{minipage}{.48\textwidth}
% \begin{center}
% \includegraphics[width=\textwidth]{chap/01_interventi/04_cacciatore/fig09}
% \end{center}
% \caption{\footnotesize Libro III, Proposizione 16: Tra le rette perpendicolari 
%   al diametro in un suo estremo e il cerchio non è interposta alcuna retta.\\
%   Libro III, Proposizione 16, Corollario: L'angolo formato dalla tangente e il 
%   cerchio [angolo di contingenza] è minore di qualsiasi angolo rettilineo.}
%  \label{f9} 
% \end{minipage}
% \end{figure}

\vspace{-1em}

\section{Conclusione}

Sia dopo il mio intervento al Convegno sia dopo la scrittura di questo articolo 
sono stato assalito da un senso di disagio: proporre percorsi all'apparenza così 
sofisticati può sembrare una provocazione in un periodo in cui l'insegnamento 
sembra diventare sempre di più un percorso ad ostacoli fatto di compatibilità di 
ogni sorta, che interferiscono continuamente col lavoro del docente; oltre che 
un labirinto burocratico che toglie il fiato e paralizza. Spero che i pochi 
colleghi che hanno avuto la pazienza di leggere fino alla fine interpretino 
queste pagine come un invito alla resistenza in attesa di tempi migliori, o 
possano trarre spunto da qualcuna delle idee esposte per cercare di anticipare 
in qualche modo e maniera l'avvento di questi tempi.     

 \begin{thebibliography}{10}
\phantomsection
\addcontentsline{toc}{section}{Bibliografia}

\bibitem[Agazzi]{agazzi} 
\textsc{E. Agazzi, D. Palladino}, 
{\em Le geometrie non euclidee}, 
Mondadori,

\bibitem[Bottazzini]{bottazzini} 
\textsc{U. Bottazzini}, 
{\em L'aritmetizzazione 
dell'Analisi, in Storia della scienza moderna e contemporanea}, Vol. II, 
a cura di P. Rossi, 
UTET

\bibitem[Boyer]{boyer} 
\textsc{C. Boyer}, 
{\em Storia della matematica}, 
Mondadori, 1980

\bibitem[Dedekind]{dedekind} 
\textsc{I.W.R. Dedekind}, 
{\em Continuità e numeri irrazionali}, 
citato in \cite{waismann}

\bibitem[Euclide]{euclide} 
\textsc{Euclide}, 
{\em Elementi}, % Vol. II, 
a cura di A. Fraiese, 
Mondadori, 2008

\bibitem[Evans]{evans} 
\textsc{M.G. Evans}, 
{\em Aristotele, Newton e la teoria del continuo}, 
in \textsc{P.P. Wiener, A. Nolan} (a cura di), 
{\em Le radici del pensiero scientifico},
Feltrinelli, 1971

\bibitem[Geymonat]{geymonat} 
\textsc{L. Geymonat}, 
{\em Storia e filosofia dell'analisi infinitesimale}, 
Boringhieri, 2008

\bibitem[Ghersi]{ghersi} 
\textsc{I. Ghersi}, 
{\em Matematica dilettevole e curiosa}, 
Hoepli, 1986

\bibitem[Kline]{kline}
\textsc{M. Kline}, 
\emph{Storia del pensiero matematico}, 
vol. 1 - 2, Einaudi, 1996
 
\bibitem[Lolli]{lolli}
\textsc{G. Lolli}, 
\emph{Filosofia della matematica}, 
Il Mulino, 2002
 
\bibitem[Niven]{niven}
\textsc{I. Niven}, 
{\em Numeri razionali e irrazionali}, 
Zanichelli, 1965

\bibitem[Odi2005]{odifreddi05}
\textsc{P. Odifreddi}, 
{\em Penna, pennello e bacchetta}, 
Laterza, 2005

\bibitem[Odi2010]{odifreddi10}
\textsc{P. Odifreddi}, 
{\em C'è spazio per tutti}, 
Mondadori, 2010

\bibitem[Posamentier]{posamentier}
\textsc{A.S. Posamentier, I. Lehmann},
{\em I favolosi numeri di Fibonacci}, 
Muzio, 2010

\bibitem[Waismann]{waismann} 
\textsc{F.Waismann}, 
{\em Introduzione al pensiero matematico}
Boringhieri, 1971

 \end{thebibliography}

\chapter{Presentazione}
\begin{center} Parte I \end{center}
Il Dipartimento di Informatica dell'Università di Verona, nel quadro del Piano 
Nazionale Lauree Scientifiche - Progetto Nazionale di Matematica e in 
collaborazione con Mathesis Verona, ha avuto anche quest'anno il grande piacere 
e l'onore di ospitare la Giornata Nazionale di Analisi Non Standard  per i 
docenti della Scuola Secondaria di Secondo Grado.

È un'occasione che, da docente di Analisi Matematica, mi riempie di entusiasmo: 
le costruzioni non standard di Robinson e dei suoi successori permettono infatti 
di recuperare, in tutto il loro fascino, gli infinitesimi di Leibniz e dei 
pionieri del Calcolo del XVIII sec., senza per questo  rinunciare al rigore e 
alla precisione. 

La definizione formale di limite con \(\epsilon\) e \(\delta\), con tutto il suo
labirinto di 
quantificatori che tanto spesso viene frainteso dagli studenti,  viene 
sostituita da un allargamento della retta reale,  che viene arricchita di 
infiniti e infinitesimi. Un'idea suggestiva, che si presta ad essere chiarita 
con metafore evocative  come i microscopi e i telescopi a ingrandimento 
infinito\dots e  costituisce  un'occasione straordinaria per innovare la 
didattica dell'Analisi Matematica, sia in alternativa che a fianco di una 
trattazione più tradizionale della materia.

La giornata è giunta ormai alla sua nona edizione e si affianca ad un 
allargamento delle esperienze didattiche di insegnamento dell'Analisi 
Nonstandard nella Scuola  italiana: se è vero che si tratta ancora di 
sperimentazioni di nicchia, il loro numero è andato progressivamente 
aumentando! 

Il convegno è dunque un'occasione, per i colleghi insegnanti, di arricchire le 
loro conoscenze disciplinari con conferenze sugli aspetti tecnici e sulla storia 
del calcolo infinitesimale tenute da alcuni dei massimi cultori italiani del 
settore, ma anche di un momento importante di confronto con i colleghi e gli 
esperti, di divulgazione e di scambio di esperienze e buone pratiche 
didattiche. 

Come novità di questa edizione, sono stati previsti due tutorial: uno per i 
``principianti'' dell'analisi nonstandard, il secondo per ``utenti avanzati'' 
che volessero approfondire aspetti più specifici. 

\vspace{1em}
%\mesepubblicazione~\annopubblicazione\\
%\vspace{6pt}
\begin{flushright}
%\hspace*{2cm}
Sisto Baldo - Università di Verona,  Referente PLS di Matematica
\end{flushright}

\newpage
\begin{center} Parte II \end{center}
L'Analisi non Standard trova qualche ostacolo ad affermarsi come metodo 
analitico e matematico a uso dei licei e delle scuole medie superiori. 

Forse 
questa difficoltà è dovuta prevalentemente a una sorta d'inerzia dei docenti di 
Matematica ad aprirsi al nuovo, a sperimentare metodi diversi d'insegnamento di 
questa disciplina o, forse, è la paura che agli esami di Stato studenti 
preparati a trattare i vari argomenti di matematica con il metodo non standard 
si dimostrino meno preparati o meno adeguati a rispondere con un linguaggio 
standard alle richieste o ai quesiti che professori con conoscenze tradizionali 
possono fare loro durante la prova d'esame.

Se le ragioni fossero queste, ogni 
sperimentazione applicata alla formazione culturale in campo matematico di 
studenti alla fine del loro corso di studi non si dovrebbe fare perché troppo 
rischiosa. Ma ciò contrasta con la funzione stessa dell'educare. Se, infatti, 
educare (etimologicamente) significa <<condurre fuori>>, allora il vero 
educatore è colui che porta novità nell'insegnamento accompagnando così su 
percorsi nuovi, anche se irti di ostacoli, i giovani studenti. 
Non è possibile produrre risultati migliori senza alcun rischio associato alla 
nostra azione docente e ogni docente deve pur liberarsi dalle sue abitudini.

Le novità dell'insegnamento dell'analisi matematica impostato su basi non 
standard possono essere espresse considerando due aspetti che ne 
contraddistinguono le funzioni: il primo è il confronto diretto con l'idea 
fondamentale dell'infinito matematico (e dell'infinitesimo), che domina da 
sempre ogni speculazione matematica nel corso della sua storia, malgrado il 
tentativo di addomesticarne i contorni fatto dagli analisti standard nel corso 
del XIX secolo; il secondo è che il campo ordinato e completo dei numeri reali, 
fondamento di tutta l'analisi classica, non è l'unica struttura numerica 
possibile e che si può fare a meno del postulato di Archimede (e dell'assioma 
della completezza) senza che ciò comporti malanni interpretativi dei fenomeni 
matematici da cui poi non si potrà più guarire.

Se si accetta l'idea che anche 
gli infinitesimi e gli infiniti sono numeri, si può procedere senza intoppi 
nello studio dell'analisi matematica arrivando a ottenere gli stessi risultati 
che si ottengono con l'uso dell'analisi classica. 

D'altra parte, grazie ai 
lavori di A. Robinson, di J. Keisler e di altri logici del XX secolo sappiamo 
ora che l'Analisi non Standard è logicamente coerente come l'analisi classica 
rispetto al linguaggio formale del primo ordine e, perciò, la scelta di un 
metodo o dell'altro è solo questione di utilità.

Gli oggetti matematici esistono in quanto dimostrano di essere funzionali al 
miglioramento delle risposte ai problemi che la matematica stessa pone (Benci 
V.). Alla domanda ``esistono i punti?'' Se dovessimo giustificare in sé una 
risposta possibile troveremmo una molteplicità d'interpretazioni e forse ci 
porremmo il problema di dare un senso alla risposta. Ma se, invece, ci 
ponessimo la domanda <<È utile disporre di questo oggetto matematico?>>. Senza 
dubbio la risposta sarebbe affermativa. Così, l'Analisi non Standard risponde a 
requisiti di utilità analitica perché migliora la conoscenza dell'analisi 
matematica ed è funzionale alla sua didattica.

Queste sono le ragioni per le quali un gruppo di docenti di matematica della 
Mathesis di Verona sta operando in collaborazione con altri appassionati sparsi 
in Italia perché l'Analisi non Standard si affermi come argomento di matematica 
per le scuole medie superiori. Sono ormai nove anni che a diversi livelli sono 
stati organizzati convegni di Analisi non Standard in varie città italiane e il 
gruppo di Verona è trainante e fermamente convinto che sia opportuno continuare 
per rendere familiare a tutti i docenti questo argomento della matematica. A 
Verona, è la seconda volta che si organizza un convegno su questo tema e anche 
quest'anno è andata bene sia per la qualità delle relazioni presentate, sia per 
la buona presenza di pubblico motivato.

\vspace{1em}
%\mesepubblicazione~\annopubblicazione\\
%\vspace{6pt}
\begin{flushright}
%\hspace*{2cm}
Luciano Corso - Presidente della Mathesis di Verona
\end{flushright}


# makefile per nsa_atti_5
#
# (c) 2014 Daniele Zambelli - daniele.zambelli@gmail.com
#
# tratto da un lavoro di:
# (c) 2013 Dimitrios Vrettos - d.vrettos@gmail.com
# v1.0
#
NAME = nsa_atti_9_r01

SHORT_NAME = nsa_atti_9_r01

TEX = $(NAME).tex

PDF = $(NAME).pdf

PDFLATEX = pdflatex --shell-escape

PDFGRAY = gs -sDEVICE=pdfwrite -dProcessColorModel=/DeviceGray -dColorConversionStrategy=/Gray -dPDFUseOldCMS=false -o $(NAME)_mono.pdf -f $(NAME).pdf

FILE_CLEAN = *.aux *.gnuplot *.table *.toc *.log *~ *backup *.old *.veia *.out

ROOT = $(shell basename $$(pwd))

DIRS = chap* lbr

CLEAN_DIRS= $(DIRS:%=clean-%)

DATE = $(shell date "+%Y%m%d")

TAR= $(SHORT_NAME)-$(DATE).tar.gz

ZIP= $(SHORT_NAME)-$(DATE).zip

help:
	@echo ''
	@echo 'nsa_atti    - Makefile targets'
	@echo ''
	@echo ' help       - Questo messaggio'
	@echo ' pdf        - Crea il file pdf'
	@echo ' clean      - Rimuove tutti i file ausiliari'
	@echo ' clean-dist - Rimuove tutti i file prodotti'
	@echo ' dist-tar   - Crea una distribuzione (tar.gz) del codice sorgente, escludendo il file pdf'
	@echo ' dist-zip   - Crea una distribuzione (zip) del codice sorgente, escludendo il file pdf'
	@echo ''

pdf: $(TEX)
	$(PDFLATEX) $<
	$(PDFLATEX) $<

gray: $(PDFGRAY)

clean: $(CLEAN_DIRS)
$(CLEAN_DIRS):
	@for i in $(FILE_CLEAN); \
	 do \
		find ./ -type f -name "$$i" -delete ; \
	done

clean-dist: clean
	rm -f $(PDF)
	rm -f *.tar.gz
	rm -f *.zip
 
dist-zip: clean
	rm -f  $(ZIP)
	zip -r $(ZIP) . -x '$(PDF)' -x '*.zip' -x '*.tar.gz'

dist-tar: clean
	rm -f $(TAR)
	tar -czvf $(TAR) --exclude $(PDF) --exclude *.tar.gz --exclude *.zip *

# END OF MAKEFILE

